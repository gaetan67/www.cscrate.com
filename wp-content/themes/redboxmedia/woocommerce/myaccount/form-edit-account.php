<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_edit_account_form' ); ?>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post">
	<h2><?php echo __('Informations','redbox'); ?></h2>

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>

	<div class="woocommerce-FormRow col_1_3 form-row ">
		<!--<label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>-->
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" placeholder="<?php _e( 'First name', 'woocommerce' ); ?>" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
	</div>
	<div class="woocommerce-FormRow  col_1_3 form-row ">
		<!--<label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>-->
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" placeholder="<?php _e( 'Last name', 'woocommerce' ); ?>" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
	</div>
	

	<div class="woocommerce-FormRow  col_1_3 last form-row ">
		<!--<label for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>-->
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" placeholder="<?php _e( 'Email address', 'woocommerce' ); ?>" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
	</div>
	<div style="clear: both;"></div>
	
	<h2><?php echo __('Password Change','redbox'); ?></h2>
	<fieldset>
		

		<div class="woocommerce-FormRow  col_1_3 form-row ">
			<!--<label for="password_current"><?php _e( 'Current Password', 'woocommerce' ); ?></label>-->
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" placeholder="<?php _e( 'Current Password', 'woocommerce' ); ?>" name="password_current" id="password_current" />
		</div>
		<div class="woocommerce-FormRow  col_1_3 form-row ">
			<!--<label for="password_1"><?php _e( 'New Password', 'woocommerce' ); ?></label>-->
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" placeholder="<?php _e( 'New Password', 'woocommerce' ); ?>" name="password_1" id="password_1" />
		</div>
		<div class="woocommerce-FormRow  col_1_3 last form-row ">
			<!--<label for="password_2"><?php _e( 'Confirm Password', 'woocommerce' ); ?></label>-->
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" placeholder="<?php _e( 'Confirm Password', 'woocommerce' ); ?>" name="password_2" id="password_2" />
		</div>
		<div style="clear: both;"></div>
	</fieldset>
	<!--<h2><?php echo __('Billing Address','redbox'); ?></h2>-->
	<?php if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	$page_title = __( 'Billing Address', 'woocommerce' );
	//$load_address = 'billing';
	do_action( 'woocommerce_before_edit_account_address_form' ); ?>

	<?php if ( ! $load_address ) : ?>
		<?php wc_get_template( 'myaccount/my-address.php' ); ?>
	<?php else : ?>

		<form method="post">


			<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

			<?php foreach ( $address as $key => $field ) : ?>

				<?php woocommerce_form_field( $key, $field, ! empty( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : $field['value'] ); ?>

			<?php endforeach; ?>

			<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

			<p>
				<input type="submit" class="button" name="save_address" value="<?php esc_attr_e( 'Save Address', 'woocommerce' ); ?>" />
				<?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
				<input type="hidden" name="action" value="edit_address" />
			</p>

		</form>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_edit_account_address_form' ); ?>
	





	<!--<h2><?php echo __('Steam ID','redbox'); ?></h2>
	<div class="woocommerce-FormRow  col_1_3 last form-row form-row-wide">
			
			<input type="text" class="input-text steamid" placeholder="<?php _e( 'Your Steam ID', 'woocommerce' ); ?>" name="steamid" id="steamid"  value="<?php echo esc_attr( $user->steam_id ); ?>"/>
		</div>
	<div class="clear"></div>-->

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<div>
		<?php wp_nonce_field( 'save_account_details' ); ?>
		<input type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>" />
		<input type="hidden" name="action" value="save_account_details" />
	</div>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
