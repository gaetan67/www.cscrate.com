<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
<div id="login-popup2" >
<div class="wrap-form">
        <div class="form-login">
            <form id="login" class="ajax-auth" action="login" method="post" class="login">
                <h2>Sign In</h2>
                <div class="sep"></div>
                <p class="status"></p>  
                <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>  
               <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                <input id="username" type="email" class="required" name="username" placeholder="<?php echo __('Email','redbox') ?>">
                </p>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                <input id="password" type="password" class="required" name="password" placeholder="<?php echo __('Password','redbox') ?>">
                </p>
                
                <p class="">
                    <input class="submit_button button" type="submit" value="<?php echo __('Sign In','redbox') ?>" >
                </p>
                <a class="" href="<?php echo wp_lostpassword_url(); ?>">Lost password?</a>
            	<a class="close" href="">X</a>   
                
            </form>
            <div class="user-steam">
                <span class="or-choice">OR</span>
               <!-- <p class="form-row  smaller-text">
                    <input type="text" class="required " name="idSteam" id="reg_email" placeholder="<?php _e( 'Steam user ID', 'woocommerce' ); ?>"  />
                </p>
                <p class="form-row ">
                    <input class="required" type="password" name="password-steam" placeholder="<?php _e( 'Password', 'redbox' ); ?>" id="password" />
                </p>-->
                <p class="form-row ">
                    <?php
                        if ( !is_user_logged_in() ) {
                        echo '<a href="'.wpsap_button_login_url().'" title="Steam"><img src="' . get_stylesheet_directory_uri() . '/images/steam-logo.png" /></a>';
                        }
                    ?>
                    

                </p>
            </div>
        </div>
        <div class="form-register">
            <form id="register_my_account" class="ajax-auth"  action=http://www.cscrate.com/wp-login.php?action=register" method="post" class="register">
                <h2>Register</h2>
                <div class="sep"></div>
                <p class="status"></p>
                <?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?>  
                
                 <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row half smaller-text">     
                    <input id="signonname" type="text" name="signonname" class="required" placeholder="<?php echo __('First Name','redbox') ?>">
                </p>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row half smaller-text last">
                    <input id="signonlastname" type="text" name="signonlastname" class="required" placeholder="<?php _e( 'Last Name', 'redbox' ); ?>"  />
                </p>


               
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <input id="email" type="email" class="required email" name="email" placeholder="<?php echo __('Email','redbox') ?>">
                </p>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <input id="signonpassword" type="password" class="required" name="signonpassword"  placeholder="<?php echo __('Password','redbox') ?>">
                </p>
               
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide smaller-text">
                    <input type="password" id="password_signonpassword" class="required " name="password_signonpassword"  placeholder="<?php echo __('Confirm Password','redbox') ?>">
                </p>
                 <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide smaller-text">
                    <input id="steam_id" type="text" class="required" name="steam_id"  placeholder="<?php echo __('STEAM TRADE URL','redbox') ?>">
                </p>
                <p class="form-row">
                    <input class="submit_button button" type="submit" value="<?php echo __('Register','redbox') ?>">
                </p>
               
                   
            </form>
            <div class="user-steam">
                <span class="or-choice">OR</span>
                <!--<p class="form-row ">
                   
                    <input type="text" class="required smaller-text" name="idSteam" id="reg_email" placeholder="<?php _e( 'Steam user ID', 'woocommerce' ); ?>"  />
                </p>
                <p class="form-row ">
                    
                    <input class="required" type="password" name="password-steam" placeholder="<?php _e( 'Password', 'redbox' ); ?>" id="password" />
                </p>-->
                <p class="form-row ">
                    <?php
                        if ( !is_user_logged_in() ) {
                        echo '<a href="'.wpsap_button_login_url().'" title="Steam"><img src="' . get_stylesheet_directory_uri() . '/images/steam-logo.png" /></a>';
                        }
                    ?>
                    
                </p>
            </div>
        </div>
        <div style="clear: both;"></div>
</div>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
<div style="clear: both;"></div>
