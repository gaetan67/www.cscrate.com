<?php

$id_user = get_current_user_id();

$args_items = array(
            'post_type' => 'crate',
            
            'meta_query' => array(
                array(
                    'key' => 'winner_item',
                    'value' => $id_user,
                    'compare' => '=',
                  ),
               )
            );
$items_wins_this_user = new WP_Query( $args_items );
$totalWin = count($items_wins_this_user -> posts);
$i = 0;

$PostByMonth = array();
$theMonth = array();

$leMois = '';
$lannee = '';
$step = 0;

?>
<?php $theDate = '2016-11-01';   ?>

<?php if ( $items_wins_this_user->have_posts() ) : ?>
		
		<?php while ( $items_wins_this_user->have_posts() ) : $items_wins_this_user->the_post(); ?>

			<?php 
			$month = date("F", strtotime($theDate));
			$year = date("Y", strtotime($theDate));

			//echo $month . ' ' . $year . ', ';

			$postMonth = get_the_date('F');
			$postYear = get_the_date('Y');
			$momentPost = get_the_date('Y-m-d');
			$getThePost = array();
			//echo $postMonth . ' ' . $postYear;

			foreach (get_post() as  $key => $post) {
				$getThePost[$key] = $post;
			}
			//print_r($getThePost);

			

			//print_r($getThePost);

			if($step == 0 || ($leMois == $postMonth && $lannee == $postYear) ) {

				$theMonth[] = $getThePost;
				$leMois = $postMonth;
				$lannee = $postYear;


				$step++;
				//print_r($theMonth);
				if($i == ($totalWin - 1)){
					$PostByMonth[] = $theMonth;
					$$momentPost = date("Y-m-d", strtotime($momentPost ." -1 months"));
					$theMonth = array();
					$step = 0;
					$leMois = '';
					$lannee = '';
				}
				


				
			} else {
				
				$step = 0;
				$PostByMonth[] = $theMonth;
				$$momentPost = date("Y-m-d", strtotime($$momentPost ." -1 months"));
				$theMonth = array();
				$leMois == '';
				$lannee == '';
			}
			
			

			$i++;

			?>

		<?php endwhile; //print_r($PostByMonth);?>
		<!-- pagination here -->
		<div class="my_crates">
		<!-- the loop -->
		<?php 
			$totalWinCrate = count($PostByMonth);
			
		?>
			<?php for ($winCrate = 0; $winCrate < $totalWinCrate; $winCrate++ ){ 
				
					if(count($PostByMonth[$winCrate]) > 0) {
				?>

				<div class="crate">
					<?php 
						$valueDate = $PostByMonth[$winCrate][0];
						$open = false;
						

						$openDate  =  date('F d Y');

						$winDate  =  get_the_date("F d Y",$valueDate['ID']);

						$winMonth = get_the_date("F",$valueDate['ID']);
						$winYear = get_the_date("Y",$valueDate['ID']);

						if (get_user_meta( $id_user, 'crate_'.$winMonth.'_'.$winYear, true )) {
							$open = true;
							// $open = false;
						}
						if (get_user_meta( $id_user, 'crate_'.$winMonth.'_'.$winYear.'_open', true )) {
							$openDate = get_user_meta( $id_user, 'crate_'.$winMonth.'_'.$winYear.'_open', true );
						}

						$plan = 'gold';
						if($plan == 'gold') {
							$image = 'gold-plan-crate_win_close.png';
							$imageOpen = 'Crate_gold.gif';
						} elseif ($plan == 'silver' ) {
							$image = 'silver-plan-crate_win_close.png';
						} else {
							$image = 'bronze-plan-crate_win_close.png';
						}
						
						$date = get_the_date("F Y",$valueDate['ID']);//get_date('F Y');
						

					?>
					<h2><?php echo $date; ?></h2>
					<div class="crate-image">
						<?php ?>
							<img class="close" src="<?php echo get_stylesheet_directory_uri(); ?>/images/<?php echo $image; ?>" alt="<?php echo $plan; ?>-plan">
					</div>
					<div class="crate-info <?php if($open) echo ' open' ?>">
						
							<p class="received">RECEIVED ON <?php echo $winDate; ?></p>
							<p class="opened">Opened ON <?php echo $openDate; ?></p>
						

						<p class="subscription">CRATE FROM YOUR <?php echo $plan; ?> SUBSCRIPTION</p>
						
					</div>

					<div class="crate-btn <?php if($open) echo ' opened' ?>">
						
						<a rel="prettyPhoto" href="#crate<?php echo $winMonth . $winYear;  ?>"   class="btn open-crate">
							Open crate
							<?php
								if (!$open) { ?>
									<form action="" method="post" accept-charset="utf-8" >
										<input type="hidden" name="open_crate" value="crate_<?php echo $winMonth.'_'.$winYear;  ?>">
									</form>
							<?php	} ?>

						</a>
					
						<a  rel="prettyPhoto" class="btn view-crate" href="#crate<?php echo $winMonth . $winYear;  ?>">View crate</a>
					</div>
					<div style="clear: both;"></div>
					<div id="crate<?php echo $winMonth . $winYear;  ?>" class="wrap_producs ">
						<div class="win_producs">
							<h2><?php echo $date; ?></h2>
							<div class="wrap_win <?php if($open) echo ' opened' ?>">
								<?php if(!$open) { ?>
									<img class="opening" src="<?php echo get_stylesheet_directory_uri(); ?>/images/<?php echo $imageOpen ; ?>" alt="<?php echo $plan; ?>-plan">
								<?php } ?>
								<?php 
									for($j = 0; $j < count($PostByMonth[$winCrate]); $j++){

								?>
									<div class="win_produc">
										<div class="produc_img">
											<?php 
											echo get_the_post_thumbnail($PostByMonth[$winCrate][$j]['ID'],'win_product');?>
										</div>
										<?php $cat = get_the_terms($PostByMonth[$winCrate][$j]['ID'],'category_crate' ); ?>

										<p class="cat"><?php echo $cat[1]->name; ?></p>
										<h3><?php echo get_the_title($PostByMonth[$winCrate][$j]['ID']); ?></h3>
										
									</div>
								<?php
																 };  ?>
								<div style="clear: both;"></div>
						</div>
						</div>
					</div>

				</div>
			
		
		<!-- end of the loop -->
			<?php } 

			} ?>
		<!-- pagination here -->
		</div>
	<?php else : ?>
		<p><?php _e( 'Sorry, no crates.' ); ?></p>

	<?php endif; ?>
	<?php wp_reset_postdata(); ?>




