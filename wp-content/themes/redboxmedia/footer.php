</
<?php 

$options = get_nectar_theme_options(); 
global $post;
$cta_link = ( !empty($options['cta-btn-link']) ) ? $options['cta-btn-link'] : '#';
$using_footer_widget_area = (!empty($options['enable-main-footer-area']) && $options['enable-main-footer-area'] == 1) ? 'true' : 'false';
$disable_footer_copyright = (!empty($options['disable-copyright-footer-area']) && $options['disable-copyright-footer-area'] == 1) ? 'true' : 'false';
$footer_reveal = (!empty($options['footer-reveal'])) ? $options['footer-reveal'] : 'false'; 
$midnight_non_reveal = ($footer_reveal != 'false') ? null : 'data-midnight="light"';

  
$exclude_pages = (!empty($options['exclude_cta_pages'])) ? $options['exclude_cta_pages'] : array(); 

/*?>

<div id="footer-outer" <?php echo $midnight_non_reveal; ?> data-using-widget-area="<?php echo $using_footer_widget_area; ?>">
	
	<?php if(!empty($options['cta-text']) && current_page_url() != $cta_link && !in_array($post->ID, $exclude_pages)) {  
		$cta_btn_color = (!empty($options['cta-btn-color'])) ? $options['cta-btn-color'] : 'accent-color'; ?>

		<div id="call-to-action">
			<div class="container">
				<div class="triangle"></div>
				<span> <?php echo $options['cta-text']; ?> </span>
				<a class="nectar-button <?php if($cta_btn_color != 'see-through') echo 'regular-button '; ?> <?php echo $cta_btn_color;?>" data-color-override="false" href="<?php echo $cta_link ?>"><?php if(!empty($options['cta-btn'])) echo $options['cta-btn']; ?> </a>
			</div>
		</div>

	<?php } ?>

	<?php if( $using_footer_widget_area == 'true') { ?>
		
	<div id="footer-widgets">
		
		<div class="container">
			
			<div class="row">
				
				<?php 
				
				$footerColumns = (!empty($options['footer_columns'])) ? $options['footer_columns'] : '4'; 
				
				if($footerColumns == '2'){
					$footerColumnClass = 'span_6';
				} else if($footerColumns == '3'){
					$footerColumnClass = 'span_4';
				} else {
					$footerColumnClass = 'span_3';
				}
				?>
				
				<div class="col <?php echo $footerColumnClass;?>">
				      <!-- Footer widget area 1 -->
		              <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Footer Area 1') ) : else : ?>	
		              	  <div class="widget">		
						  	 <h4 class="widgettitle">Widget Area 1</h4>
						 	 <p class="no-widget-added"><a href="<?php echo admin_url('widgets.php'); ?>">Click here to assign a widget to this area.</a></p>
				     	  </div>
				     <?php endif; ?>
				</div><!--/span_3-->
				
				<div class="col <?php echo $footerColumnClass;?>">
					 <!-- Footer widget area 2 -->
		             <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Footer Area 2') ) : else : ?>	
		                  <div class="widget">			
						 	 <h4 class="widgettitle">Widget Area 2</h4>
						 	 <p class="no-widget-added"><a href="<?php echo admin_url('widgets.php'); ?>">Click here to assign a widget to this area.</a></p>
				     	  </div>
				     <?php endif; ?>
				     
				</div><!--/span_3-->
				
				<?php if($footerColumns == '3' || $footerColumns == '4') { ?>
					<div class="col <?php echo $footerColumnClass;?>">
						 <!-- Footer widget area 3 -->
			              <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Footer Area 3') ) : else : ?>		
			              	  <div class="widget">			
							  	<h4 class="widgettitle">Widget Area 3</h4>
							  	<p class="no-widget-added"><a href="<?php echo admin_url('widgets.php'); ?>">Click here to assign a widget to this area.</a></p>
							  </div>		   
					     <?php endif; ?>
					     
					</div><!--/span_3-->
				<?php } ?>
				
				<?php if($footerColumns == '4') { ?>
					<div class="col <?php echo $footerColumnClass;?>">
						 <!-- Footer widget area 4 -->
			              <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Footer Area 4') ) : else : ?>	
			              	<div class="widget">		
							    <h4>Widget Area 4</h4>
							    <p class="no-widget-added"><a href="<?php echo admin_url('widgets.php'); ?>">Click here to assign a widget to this area.</a></p>
							 </div><!--/widget-->	
					     <?php endif; ?>
					     
					</div><!--/span_3-->
				<?php } ?>
				
			</div><!--/row-->
			
		</div><!--/container-->
	
	</div><!--/footer-widgets-->
	
	<?php } //endif for enable main footer area


	   if( $disable_footer_copyright == 'false') { ?>

	
		<div class="row" id="copyright">
			
			<div class="container">
				
				<div class="col span_5">
					
					<?php if(!empty($options['disable-auto-copyright']) && $options['disable-auto-copyright'] == 1) { ?>
						<p><?php if(!empty($options['footer-copyright-text'])) echo $options['footer-copyright-text']; ?> </p>	
					<?php } else { ?>
						<p>&copy; <?php echo date('Y') . ' ' . get_bloginfo('name'); ?>. <?php if(!empty($options['footer-copyright-text'])) echo $options['footer-copyright-text']; ?> </p>
					<?php } ?>
					
				</div><!--/span_5-->
				
				<div class="col span_7 col_last">
					<ul id="social">
						<?php  if(!empty($options['use-twitter-icon']) && $options['use-twitter-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['twitter-url']; ?>"><i class="icon-twitter"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-facebook-icon']) && $options['use-facebook-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['facebook-url']; ?>"><i class="icon-facebook"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-vimeo-icon']) && $options['use-vimeo-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['vimeo-url']; ?>"> <i class="icon-vimeo"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-pinterest-icon']) && $options['use-pinterest-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['pinterest-url']; ?>"><i class="icon-pinterest"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-linkedin-icon']) && $options['use-linkedin-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['linkedin-url']; ?>"><i class="icon-linkedin"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-youtube-icon']) && $options['use-youtube-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['youtube-url']; ?>"><i class="icon-youtube"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-tumblr-icon']) && $options['use-tumblr-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['tumblr-url']; ?>"><i class="icon-tumblr"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-dribbble-icon']) && $options['use-dribbble-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['dribbble-url']; ?>"><i class="icon-dribbble"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-rss-icon']) && $options['use-rss-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo (!empty($options['rss-url'])) ? $options['rss-url'] : get_bloginfo('rss_url'); ?>"><i class="icon-rss"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-github-icon']) && $options['use-github-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['github-url']; ?>"><i class="icon-github-alt"></i></a></li> <?php } ?>
						<?php  if(!empty($options['use-behance-icon']) && $options['use-behance-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['behance-url']; ?>"> <i class="icon-be"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-google-plus-icon']) && $options['use-google-plus-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['google-plus-url']; ?>"><i class="icon-google-plus"></i> </a></li> <?php } ?>
						<?php  if(!empty($options['use-instagram-icon']) && $options['use-instagram-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['instagram-url']; ?>"><i class="icon-instagram"></i></a></li> <?php } ?>
						<?php  if(!empty($options['use-stackexchange-icon']) && $options['use-stackexchange-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['stackexchange-url']; ?>"><i class="icon-stackexchange"></i></a></li> <?php } ?>
						<?php  if(!empty($options['use-soundcloud-icon']) && $options['use-soundcloud-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['soundcloud-url']; ?>"><i class="icon-soundcloud"></i></a></li> <?php } ?>
						<?php  if(!empty($options['use-flickr-icon']) && $options['use-flickr-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['flickr-url']; ?>"><i class="icon-flickr"></i></a></li> <?php } ?>
						<?php  if(!empty($options['use-spotify-icon']) && $options['use-spotify-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['spotify-url']; ?>"><i class="icon-salient-spotify"></i></a></li> <?php } ?>
						<?php  if(!empty($options['use-vk-icon']) && $options['use-vk-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['vk-url']; ?>"><i class="icon-vk"></i></a></li> <?php } ?>
						<?php  if(!empty($options['use-vine-icon']) && $options['use-vine-icon'] == 1) { ?> <li><a target="_blank" href="<?php echo $options['vine-url']; ?>"><i class="fa-vine"></i></a></li> <?php } ?>
					</ul>
				</div><!--/span_7-->
			
			</div><!--/container-->
			
		</div><!--/row-->
		
		<?php } //endif for enable main footer copyright ?>

</div><!--/footer-outer-->


<?php 

$mobile_fixed = (!empty($options['header-mobile-fixed'])) ? $options['header-mobile-fixed'] : 'false';
$has_main_menu = (has_nav_menu('top_nav')) ? 'true' : 'false';

$sideWidgetArea = (!empty($options['header-slide-out-widget-area'])) ? $options['header-slide-out-widget-area'] : 'off';
$userSetSideWidgetArea = $sideWidgetArea;
if($has_main_menu == 'true' && $mobile_fixed == '1') $sideWidgetArea = '1';

$fullWidthHeader = (!empty($options['header-fullwidth']) && $options['header-fullwidth'] == '1') ? true : false;
$sideWidgetClass = (!empty($options['header-slide-out-widget-area-style'])) ? $options['header-slide-out-widget-area-style'] : 'slide-out-from-right';
$sideWidgetOverlayOpacity = (!empty($options['header-slide-out-widget-area-overlay-opacity'])) ? $options['header-slide-out-widget-area-overlay-opacity'] : 'dark';
$prependTopNavMobile = (!empty($options['header-slide-out-widget-area-top-nav-in-mobile'])) ? $options['header-slide-out-widget-area-top-nav-in-mobile'] : 'false';

if($sideWidgetArea == '1') { 

	if($sideWidgetClass == 'fullscreen') echo '</div><!--blurred-wrap-->'; ?>

	<div id="slide-out-widget-area-bg" class="<?php echo $sideWidgetClass . ' '. $sideWidgetOverlayOpacity; ?>"></div>
	<div id="slide-out-widget-area" class="<?php echo $sideWidgetClass; ?>" data-back-txt="<?php echo __('Back', NECTAR_THEME_NAME); ?>">

		<?php if($sideWidgetClass == 'fullscreen') echo '<div class="inner-wrap">'; ?>

		<div class="inner">

		  <a class="slide_out_area_close" href="#"><span class="icon-salient-x icon-default-style"></span></a>


		   <?php  

		   if($userSetSideWidgetArea == 'off' || $prependTopNavMobile == '1' && $has_main_menu == 'true') { ?>
			   <div class="off-canvas-menu-container mobile-only">
			  		<ul class="menu">
					   <?php 
					  		////use default top nav menu if ocm is not activated
					  	     ////but is needed for mobile when the mobile fixed nav is on
					   		wp_nav_menu( array('theme_location' => 'top_nav', 'container' => '', 'items_wrap' => '%3$s')); 
					   ?>
		
					</ul>
				</div>
			<?php } 
		 
		  if(has_nav_menu('off_canvas_nav') && $userSetSideWidgetArea != 'off') { ?>
		 	 <div class="off-canvas-menu-container">
		  		<ul class="menu">
					    <?php wp_nav_menu( array('theme_location' => 'off_canvas_nav', 'container' => '', 'items_wrap' => '%3$s'));	?>		  
				</ul>
		    </div>
		    
		  <?php } 
		  
		   //widget area
		   if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Off Canvas Menu') ) : elseif(!has_nav_menu('off_canvas_nav') && $userSetSideWidgetArea != 'off') : ?>	
		      <div class="widget">			
			 	 <h4 class="widgettitle">Side Widget Area</h4>
			 	 <p class="no-widget-added"><a href="<?php echo admin_url('widgets.php'); ?>">Click here to assign widgets to this area.</a></p>
		 	  </div>
		 <?php endif; ?>

		</div>

		<?php

			$usingSocialOrBottomText = (!empty($options['header-slide-out-widget-area-social']) && $options['header-slide-out-widget-area-social'] == '1' || !empty($options['header-slide-out-widget-area-bottom-text'])) ? true : false;
			
			if($usingSocialOrBottomText == true) echo '<div class="bottom-meta-wrap">';
			
		 	/*social icons*/
			/* if(!empty($options['header-slide-out-widget-area-social']) && $options['header-slide-out-widget-area-social'] == '1') {
			 	$social_link_arr = array('twitter-url','facebook-url','vimeo-url','pinterest-url','linkedin-url','youtube-url','tumblr-url','dribbble-url','rss-url','github-url','behance-url','google-plus-url','instagram-url','stackexchange-url','soundcloud-url','flickr-url','spotify-url','vk-url','vine-url');
			 	$social_icon_arr = array('icon-twitter','icon-facebook','icon-vimeo','icon-pinterest','icon-linkedin','icon-youtube','icon-tumblr','icon-dribbble','icon-rss','icon-github-alt','icon-be','icon-google-plus','icon-instagram','icon-stackexchange','icon-soundcloud','icon-flickr','icon-salient-spotify','icon-vk','fa-vine');
			 	
			 	echo '<ul class="off-canvas-social-links">';

			 	for($i=0; $i<sizeof($social_link_arr); $i++) {
			 		
			 		if(!empty($options[$social_link_arr[$i]]) && strlen($options[$social_link_arr[$i]]) > 1) echo '<li><a target="_blank" href="'.$options[$social_link_arr[$i]].'"><i class="'.$social_icon_arr[$i].'"></i></a></li>';
			 	}

			 	echo '</ul>';
			 }

			 /*bottom text*/
			 /*if(!empty($options['header-slide-out-widget-area-bottom-text'])) {
			 	echo '<p class="bottom-text">'.$options['header-slide-out-widget-area-bottom-text'].'</p>';
			 }

			if($usingSocialOrBottomText == true) echo '</div><!--/bottom-meta-wrap-->';

			if($sideWidgetClass == 'fullscreen') echo '</div> <!--/inner-wrap-->'; ?>

	</div>
<?php } */?>


</div> <!--/ajax-content-wrap-->


<?php if(!empty($options['boxed_layout']) && $options['boxed_layout'] == '1') { echo '</div>'; } ?>
</div>
<div style="clear: both;"></div>
<?php if(is_front_page()){ ?>
	<div class="store-slider">
		<h2><?php echo get_field('store_title', 43); ?></h2>
		<div class='wrap-slider'>
			<div class="wrap-strict">
				<div class="flexslider">
					<ul class="products slides">
						<?php
							$args = array(
								'post_type' => 'product',
								'posts_per_page' => -1,
								'tax_query'             => array(
							        array(
							            'taxonomy'      => 'product_cat',
							            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
							            'terms'         => 17,
							            'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
							        )
							    )
							);
							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) {
								while ( $loop->have_posts() ) : $loop->the_post();
									wc_get_template_part( 'content', 'product' );
								endwhile;
							} else {
								echo __( 'No products found' );
							}
							wp_reset_postdata();
						?>
					</ul><!--/.products-->
				</div>
				
			</div>
		</div>
		
	</div>
<?php } ?>
</div><!---- /.wrap-all -->
<div style="clear: both;"></div>
<?php echo get_template_part('ajax', 'auth'); ?>

<?php 

	if(is_shop()){
		$popup = load_template_part('nonmember', 'popup');
		// echo do_shortcode('[groups_non_member group="Gold,Silver,Bronze"]'.  $popup.'[/groups_non_member]') ;
		if (check_if_user_in_group('Gold') && check_if_user_in_group('Silver') && check_if_user_in_group('Bronze') ) {
			echo $popup;
		}
		
	}
?>
	
	
<div class="footer-menu">
	<?php 
	
		wp_nav_menu(array(
			'theme_location' => 'expanded_footer', // menu slug from step 1
			'container' => false, // 'div' container will not be added
			'menu_class' => 'nav', // <ul class="nav"> 
			//'fallback_cb' => 'default_header_nav', // name of default function from step 2
		));
	

	?>
	<div class="credits">
		© <?php echo date('Y'); ?> CS Crate | Website by <a target="_blank" href='http://redboxmedia.ca/' title=''Redbox Media>Redbox Media</a>, Your creative agency
	</div>
</div>
</div>
<?php if(!empty($options['back-to-top']) && $options['back-to-top'] == 1) { ?>
	<a id="to-top" class="<?php if(!empty($options['back-to-top-mobile']) && $options['back-to-top-mobile'] == 1) echo 'mobile-enabled'; ?>"><i class="icon-angle-up"></i></a>
<?php } ?>

<?php wp_footer(); ?>	

<!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/flexslider/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/flexslider/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/flexslider/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/flexslider/js/jquery.easing.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/flexslider/js/jquery.mousewheel.js"></script>
  <script type="text/javascript">
  	/*jQuery(window).load(function() {
	  jQuery('.flexslider').flexslider({
	    animation: "slide",
	    animationLoop: false,
	    itemWidth: 210,
	    itemMargin: 0,
	    minItems: 2,
	    maxItems: 5
	  });
	});*/


	

	


	function productImageHeight(){
		$imageHeight = jQuery('.single-product-main-image').innerHeight();
		jQuery('.single-product-summary .summary').css('min-height',$imageHeight+'px');
	}
	jQuery(window).load(function() {
	  productImageHeight();
	  
	});
	jQuery(window).resize(function() {
	  productImageHeight();
	});
</script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>


<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-validation/dist/jquery.validate.js"></script>
<script type="text/javascript">

	jQuery(document).ready(function() {
		jQuery(".various").fancybox({
			maxWidth	: 900,
			maxHeight	: 817,
			fitToView	: false,
			width		: '85%',
			height		: '85%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
		
	});
  </script>

  <script type="text/javascript">
  	jQuery('.slide-out-widget-area-toggle a').click(function(){
  		jQuery('#header-outer header#top nav').toggleClass('open');
  		jQuery('#header-outer header#top .bg_menu_open').toggleClass('open');
  	}) ;
  	jQuery('.bg_menu_open, .btn-close-menu').click(function(){
  		jQuery('#header-outer header#top nav').toggleClass('open');
  		jQuery('.bg_menu_open').toggleClass('open');
  	}) ;
  </script>


<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/ajax-login/ajax-auth-style.css" type="text/css" media="screen" />
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/ajax-login/ajax-auth-script.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/ajax-login/jquery.validate.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function(){
			
				jQuery('.open-crate').each(function(){
					/*jQuery('.view-crate').each(function(){*/
					var clickBtn = jQuery(this);
					    

					jQuery(clickBtn).click(function(){
						jQuery.post(
						    "<?php echo admin_url( 'admin-ajax.php' ) ?>",
						    {
						        'action': "crate_open_red",
						        'postdata': clickBtn.children('form').serialize()
						    },
						    function ( response ) {
						    	console.log(response);
						        // jQuery(".thankyou").html("Merci! "+response);
						    }
						);
						//jQuery(clickBtn).parents('.crate').find('.wrap_win').addClass('opened');
						jQuery(clickBtn).parent().addClass('opened');
						jQuery(clickBtn).parent().parent().find('.crate-info').addClass('open');
						/*jQuery(clickBtn).parent().parent().find('.crate-image').addClass('open');*/
						setTimeout(function(){ 

							//jQuery(clickBtn).parent().parent().find('.crate-image').addClass('open_gift');

							
							jQuery('.pp_inline').find('.wrap_win').addClass('opened');

						}, 1500);

					});

				});
				

			
			jQuery('.main-content.contact form .gfield input').keyup(function(){
				$inputVal = jQuery(this).val();
				if($inputVal != '') {
					jQuery(this).parent().parent().addClass('actif');
					$largeur = jQuery(this).parent().parent().find('label').innerWidth();
					jQuery(this).css('padding-right', $largeur + 5 + 'px');
				}else{
					jQuery(this).parent().parent().removeClass('actif');
					jQuery(this).css('padding-right', '0')
				}
			});
			jQuery('.main-content.contact form .gfield textarea').keyup(function(){
				$inputVal = jQuery(this).val();
				if($inputVal != '') {
					jQuery(this).parent().parent().addClass('actif');
					$largeur = jQuery(this).parent().parent().find('label').innerWidth();
					jQuery(this).css('padding-right', $largeur + 5 + 'px');
				}else{
					jQuery(this).parent().parent().removeClass('actif');
					jQuery(this).css('padding-right', '0')
				}
			});

			function relatedProductsHeight() {

				jQuery('.related.products li .btn-product').css('height','auto');
				var prodHeight = 0;
				jQuery('.related.products li .btn-product').each(function(){
					var oneProdHeight = jQuery(this).innerHeight();
					if(oneProdHeight > prodHeight) prodHeight = oneProdHeight;
				});
				jQuery('.related.products li .btn-product').css('height', prodHeight + 'px');
			}
			function homeSliderHeight() {
				
				jQuery('.flexslider .text_on_hover.product').css('height','auto');
				var prodHeight = 0;
				jQuery('.flexslider .text_on_hover.product').each(function(){
					var oneProdHeight = jQuery(this).innerHeight();
					if(oneProdHeight > prodHeight) { prodHeight = oneProdHeight; };
					//alert(prodHeight);
				});
				jQuery('.flexslider .text_on_hover.product .product-wrap').css('height', prodHeight + 'px');
				
			}

			//** bloque accès produit si non logé, difiré vers page des produits ou un popup ouvre ***/

			jQuery(document).on('click','.store-slider .add_to_cart_button',function(){
				
				var data = {
				    action: 'is_user_logged_in'
				};
				alert(date);
				return false;

				jQuery.post(ajaxurl, data, function(response) {
				    if(response == 'no') {
				        // user is logged in, do your stuff here
				        // similar behavior as clicking on a link
				        window.location.href = "<?php echo get_permalink(4); ?>";
				        return false;
				    } else {
				        // user is not logged in, show login form here
				    }
				});
			});

			


			relatedProductsHeight();
			
			jQuery('.flexslider .slides').load(function(){

				    homeSliderHeight();
				});
			jQuery('.flexslider').on('DOMNodeInserted', 'ul.products', function(){

			    homeSliderHeight();
			});

			    
			
		jQuery(document).resize(function(){
			relatedProductsHeight();
			homeSliderHeight();
		});





		
	})
	jQuery(document).ready(function () {
	    // store the slider in a local variable
	    var $window = jQuery(window),
      flexslider = { vars:{} };

	    // tiny helper function to add breakpoints
	    function getGridSize() {
	        return (window.innerWidth < 480) ? 1 : (window.innerWidth < 900) ? 2 : (window.innerWidth < 1100) ? 3 : (window.innerWidth < 1300) ? 4 : 5;
	        
	    }

	    /*jQuery(function () {
	        SyntaxHighlighter.all();
	    });*/
	     $window.load(function() {
		    jQuery('.flexslider').flexslider({
		        animation: "slide",
		        animationLoop: false,
		        itemWidth: 290,
		        itemMargin: 0,
		        smoothHeight: true,
		        prevText: " ",
		        nextText: " ",
		        minItems: getGridSize(),
		        maxItems: getGridSize()/*,
		        start: function (slider) {
		            flexslider = slider;
		        }*/
		    });
		 });
	    // check grid size on resize event
	    /*jQuery(window).resize(function () {
	        var gridSize = getGridSize();
	       
	        flexslider.vars.minItems = getGridSize();
	        flexslider.vars.maxItems = getGridSize();
	    });*/
	    $window.resize(function() {
		    var gridSize = getGridSize();
		 
		    flexslider.vars.minItems = gridSize;
		    flexslider.vars.maxItems = gridSize;
		  });
	    
	    jQuery('.open-crate').click(function(){
	    
	    	var color = function(){
	    		jQuery('.pp_inline .wrap_win').addClass('opened');
	    	};
	    	
	    	setTimeout(color, 2000);
	    });

	    function heightProductShop(){
	    	jQuery('.archive .text_on_hover.product .product-wrap').css('height','auto');
	    	var heightProductShop = 0;
	    	jQuery('.archive .text_on_hover.product .product-wrap').each(function(){
	    		var onHeight = jQuery(this).innerHeight();
	    		if(onHeight > heightProductShop) heightProductShop = onHeight;
	    	});
	    	jQuery('.archive .text_on_hover.product .product-wrap').css('height', heightProductShop + 'px');
	    }
	    heightProductShop(); 
	   
		    jQuery('.woocommerce').on('DOMNodeInserted', 'ul.products', function(){

			    heightProductShop();
			});
		jQuery(window).resize(function(){
			heightProductShop();
		});

		/*jQuery(document).on('click','.edit-account .woocommerce-Button',function(){

				jQuery.validator.addMethod("steamID", function(value, element) {
			        return this.optional(element) || /^STEAM_[01]:[01]:\d+$/i.test(value);
			    }, "Invalid Steam ID (STEAM_x:x:xxxxxxxx)");
			    
			    
				// Client side form validation
			    
					jQuery(".edit-account").validate(
					{ 
			            
						
						
			            rules: {
			                steamid: {
			                	steamID: true
			                },
			                
			            
			    		}
					});
					
		})*/
		


	
		
	});

	/*jQuery('#register').validate({
	    rules: {
	        signonpassword: {
	            required: true,
	            minlength: 5
	        },
	        password_signonpassword: {
	            required: true,
	            minlength: 5,
	            equalTo: "#signonpassword"
	        }
	    }
	}); */
</script>

</body>
</html>