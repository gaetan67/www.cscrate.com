<?php 
/*template name: How it works */
get_header(); ?>
	
<?php nectar_page_header($post->ID); ?>

<div class="container-wrap">
	
	<div class="container main-content how-work">
		
		<div class="row">
			<h1><?php echo get_field('page_title');//__('HOW TO GET STARTED','redbox'); ?></h1>
			<div class="work-steps">
				<div class="work-step">
					<div class="icon">
						<img src="<?php $image =  get_field('icon-step_1'); echo $image['url']; ?>" alt="Register" />
					</div>
					<div class="texte">
						<h2><?php echo get_field('titre_step_1'); ?></h2>
						<div class="col_1_3">
							<?php echo get_field('text_left_section_step_1'); ?>
							

						</div>
						<div class="col_1_3">
							
							<?php echo get_field('text_right_section_step_1'); ?>
						</div>
						<div style="clear: both;"></div>
					</div>
					<div class="step-number">
						step 1
					</div>	
					<div style="clear: both;"></div>
				</div>
				<div class="work-step">
					<div class="icon">
						<img src="<?php $image =  get_field('icon-step_2'); echo $image['url']; ?>" alt="GET YOUR CRATES" />
					</div>
					<div class="texte">
						
						
						<div class="col_1_3">
							<h2><?php echo get_field('titre_step_2'); ?></h2>
							<?php echo get_field('text_left_section_step_2'); ?>
							

						</div>
						<div class="col_2_3">
							<?php if( have_rows('plans') ): ?>

								<?php $i='A'; while( have_rows('plans') ): the_row(); 

									// vars
									$plan_name = get_sub_field('plan_name');
									$cost = get_sub_field('cost');
									$plan_link = get_sub_field('plan_link', false, false);
									$plan_description = get_sub_field('plan_description');

									?>

									<div class="sous_col_1_3">

										
										<div class="plan plan_<?php echo $i; ?>">
											<h3><?php echo $plan_name; ?> <br/><span>Plan</span></h3>
											<?php if(is_user_logged_in()) { ?>
												<a class="select " href="/?add-to-cart=<?php echo  $plan_link;//get_permalink( 21 );//do_shortcode('[add_to_cart_url id="21"]'); ?>" title="Select"><?php echo __('Select','redbox'); ?></a>
												
											<?php } else { ?> 
												<a class="select mobile" href="/my-account" title="Select"><?php echo __('Select','redbox'); ?></a>
												<a class="select show_login non-mobile" href="#" title="Select"><?php echo __('Select','redbox'); ?></a>
											<?php }  ?>
											
											<div class="plan-price">
												<?php echo $cost; ?><span class="space"></span>$<span class="per-month">
													Per
													<div class="month">
													month
													</div>
												</span>
											</div>
										</div>
										<div class="description">

									    	<?php echo $plan_description; ?>
									    </div>

									</div>

								<?php $i++; endwhile; ?>

							<?php endif; ?>
							
							<!--<div class="sous_col_1_3">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bronze-plan.png" alt="Bronze plan" />
								<p>Every month, receive your free skin. Your skin will be transferred directly into your Steam account on the 1st of the month.</p>
							</div>
							<div class="sous_col_1_3">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/silver-plan.png" alt="Silver plan" />
								<p>Every month, receive your free skin. Your skin will be transferred directly into your Steam account on the 1st of the month.</p>
								<p>Every month, receive 5$ credit on CScrate.com. You can use your credits to purchase any item in our online store.</p>
							</div>
							<div class="sous_col_1_3">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gold-plan.png" alt="Gold plan" />
								<p>Every month, receive your free skin. Your skin will be transferred directly into your Steam account on the 1st of the month.</p>
								<p>Every month, receive 10$ credit on CScrate.com. You can use your credits to purchase any item in our online store.</p>
								<p>As a Gold plan member, you will benefit from an extra 5% discount, applicable on all items in our online store.</p>
							</div>-->
							<div style="clear: both;"></div>
						</div>
						<div style="clear: both;"></div>
					</div>
					<div class="step-number">
						step 2
					</div>
					<div style="clear: both;"></div>
				</div>

				<div class="work-step">
					<div class="icon">
						<img src="<?php $image =  get_field('icon-step_3'); echo $image['url']; ?>" alt="GET YOUR CRATES" />
					</div>
					<div class="texte">
						
						<h2><?php echo get_field('titre_step_3'); ?></h2>
						<div class="col_1_3">
							
							<?php echo get_field('text_left_section_step_3'); ?>
							

						</div>
						<div class="col_1_3 accent">
							
							<?php echo get_field('text_right_section_step_3'); ?>
						</div>
						<div style="clear: both;"></div>
					</div>
					<div class="step-number">
						step 3
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<div class="faq-section">
				<div class="icon">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/faq-icon.png" alt="FAQ" />
				</div>
				<div class="faqs">
					<h2>F.A.Q</h2>
					<?php if( have_rows('faq') ): ?>

						<?php  while( have_rows('faq') ): the_row(); 

							// vars
							$question = get_sub_field('question');
							$answer = get_sub_field('answer');
							

							?>

							<div class="faq">

								
								<h3><?php echo $question; ?></h3>
								<?php echo $answer; ?>

							</div>

						<?php endwhile; ?>

					<?php endif; ?>
					
				</div>
			</div>
			
				
	
		</div><!--/row-->
		
	</div><!--/container-->
	
	
</div>
	
<?php get_footer(); ?>