<div id="nonmember-popup">
    <div class="popup">
        <div class="logo-login">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cscrate-login.png" />
        </div>
        <div class="wrap-content">
            <h2>To be able to access the store, you need to have an active plan</h2>

            <p><a class="button" href="<?php echo get_permalink(45); ?>" title="Choose your plan">CHOOSE YOUR PLAN</a></p>
            
        </div>
    </div>
</div>