<?php

require_once( get_template_directory() . '/libs/custom-ajax-auth.php' );

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));
}

/* creating a shortcode */
/*
function foobar_func( $atts ){
	return "foo and bar";
}
add_shortcode( 'foobar', 'foobar_func' );
*/

// Add Your Menu Locations
function register_my_menus() {
  register_nav_menus(
    array(  
    	
    	'expanded_footer' => __( 'Menu Footer' )
    )
  );
} 
add_action( 'init', 'register_my_menus' );

/**
 * Register new endpoint to use inside My Account page.
 *
 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
 */
function my_custom_endpoints() {
    add_rewrite_endpoint( 'my-crates', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'my_custom_endpoints' );


/**
 * Add new query var.
 *
 * @param array $vars
 * @return array
 */
function my_custom_query_vars( $vars ) {
    $vars[] = 'my-crates';

    return $vars;
}

add_filter( 'query_vars', 'my_custom_query_vars', 0 );


/**
 * Flush rewrite rules on theme activation.
 */
function my_custom_flush_rewrite_rules() {
    add_rewrite_endpoint( 'my-crates', EP_ROOT | EP_PAGES );
    flush_rewrite_rules();
}

add_action( 'after_switch_theme', 'my_custom_flush_rewrite_rules' );

function wpb_woo_my_account_order() {
    $myorder = array(
        /*'my-custom-endpoint' => __( 'My Stuff', 'woocommerce' ),*/
        'edit-account'       => __( 'My profile', 'woocommerce' ),
        'my-crates'       => 'My crates',
       /* 'dashboard'          => __( 'Dashboard', 'woocommerce' ),*/
        'orders'             => __( 'My orders', 'woocommerce' ),
       'subscriptions'          => __( 'My Subscription', 'woocommerce' ),
        /*'edit-address'       => __( 'Addresses', 'woocommerce' ),
        'payment-methods'    => __( 'Payment Methods', 'woocommerce' ),
        'customer-logout'    => __( 'Logout', 'woocommerce' ),*/
    );
    return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );

function my_custom_endpoint_content() {
    include 'woocommerce/myaccount/my-crates.php'; 

}

add_action( 'woocommerce_account_my-crates_endpoint', 'my_custom_endpoint_content' );


/**
 * Remove specific category ID's from the WooCommerce shop page
 */
/*add_action( 'pre_get_posts', 'uw_remove_product_cats_shop_page' );
function uw_remove_product_cats_shop_page( $query ) {

  // Comment out the line below to hide products in the admin as well
  if ( is_admin() ) return;
  
  if ( is_shop() && $query->is_main_query() ) {
 
    $query->set( 'tax_query', array(
      array(
        'taxonomy' => 'product_cat',
        'field' => 'ID',
        'terms' => array( 16 ),// 16 is ID of Plan categori
        'operator' => 'NOT IN'
      )
    ) );
  
  }
 
}*/
add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );
function custom_pre_get_posts_query( $q )
{

    if ( ! $q->is_main_query() ) return;
    if ( ! $q->is_post_type_archive() ) return;
    if ( ! is_admin() )
    {
      $q->set( 'tax_query', array(
        array(
          'taxonomy' => 'product_cat',
          'field' => 'id',
          'terms' => array( 16 ),
          'operator' => 'NOT IN'
        )
      ));

    }
}




// Filtre pour woocommerce


// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
    $fields['billing']['billing_first_name']['placeholder'] = _x('First name*', 'placeholder', 'woocommerce');
    $fields['billing']['billing_last_name']['placeholder'] = _x('Last name*', 'placeholder', 'woocommerce');
    $fields['billing']['billing_company']['placeholder'] = _x('Company*', 'placeholder', 'woocommerce');
    $fields['billing']['billing_email']['placeholder'] = _x('Email*', 'placeholder', 'woocommerce');
    $fields['billing']['billing_phone']['placeholder'] = _x('Telephone*', 'placeholder', 'woocommerce');
    $fields['billing']['billing_phone']['required'] = true;
    //$fields['billing']['billing_phone']['custom_attributes'] = array( "pattern" => "(?([0-9]{3})\)?([\s .-]?)([0-9]{3})?([\s .-]?)([0-9]{4})" );
    $fields['billing']['billing_phone']['type'] = 'tel';
    $fields['billing']['billing_phone']['custom_attributes'] = array( "minlength" => "10" );
    $fields['billing']['billing_city']['placeholder'] = _x('City*', 'placeholder', 'woocommerce');
    $fields['billing']['billing_postcode']['placeholder'] = _x('Postcode*', 'placeholder', 'woocommerce');
    $fields['billing']['billing_address_1']['placeholder'] = _x('Street address*', 'placeholder', 'woocommerce');
     /*$fields['billing']['billing_phone'] = array(
        'placeholder'   => _x('Phone', 'placeholder', 'woocommerce')
     );

     $fields['billing']['billing_phone'] = array(
        'placeholder'   => _x('Phone', 'placeholder', 'woocommerce')     );

     $fields['billing']['billing_phone'] = array(
        'placeholder'   => _x('Phone', 'placeholder', 'woocommerce')     );*/

     return $fields;
}


// replace paypal logo in checkout

function replacePayPalIcon($iconUrl) {
  return get_bloginfo('stylesheet_directory') . '/images/paypal-icon.png';
}
 
add_filter('woocommerce_paypal_icon', 'replacePayPalIcon');



function load_template_part($template_name, $part_name=null) {
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}


/** ajout steam id in user admin */
function new_contact_methods( $contactmethods ) {
    $contactmethods['steam_id'] = 'STEAM TRADE URL';
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );


function new_modify_user_table( $column ) {
    $column['steam_id'] = 'STEAM TRADE URL';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'steam_id' :
            return get_the_author_meta( 'steam_id', $user_id );
            break;
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );



/*** add steam ID in My Profile ***/
add_action( 'woocommerce_edit_account_form', 'my_woocommerce_edit_address_form' );
add_action( 'woocommerce_edit_account_form', 'my_woocommerce_edit_account_form' );
add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );
 
function my_woocommerce_edit_account_form() {
 
  $user_id = get_current_user_id();
  $steam_id = get_user_meta( $user_id, 'steam_id', true );
 
  ?>
 
 
  <h2><?php echo __('Steam TRADE URL','redbox'); ?></h2>
  <div class="woocommerce-FormRow  col_2_3 last form-row form-row-wide">
      
      <input type="text" class="input-text steamid " placeholder="<?php _e( 'Your Steam trade URL', 'woocommerce' ); ?>" required name="steamid" id="steamid"  value="<?php echo esc_attr( $steam_id ); ?>"/>
    </div>
    <div class="clear"></div>
  <?php
 
}
 
function my_woocommerce_save_account_details( $user_id ) {
  update_user_meta( $user_id, 'steam_id', htmlentities( $_POST[ 'steamid' ] ) );
}


// add_action('init','get_number_user_by_groups' );
add_image_size( 'win_product', 181, 101, true );


//* Redirect WordPress Logout to Home Page
add_action('wp_logout',create_function('','wp_redirect(home_url());exit();'));

// Ajout de la fonction call_action
add_action( 'wp_ajax_crate_open_red', 'save_crate_open' );
add_action( 'wp_ajax_nopriv_crate_open_red', 'save_crate_open');

function save_crate_open() {
  $query_tags = urldecode($_POST['postdata']);
  foreach (explode('&', $query_tags) as $resp) {
    $param = explode("=", $resp);
    $reponse = $param[1];
    $opendate_field = $param[1].'_open';
    // $current_date = ;
    $current_date = 'bien';
    $current_user = wp_get_current_user();
    update_user_meta( $current_user->ID, $opendate_field ,date('F d Y'));
    update_user_meta( $current_user->ID, $param[1],TRUE);
  }
  echo $current_date;
  die();
}


/**
 * Auto Complete all WooCommerce orders.
 */
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}

/**** detect if user loged in, utilisé pour javascript ***/

function ajax_check_user_logged_in() {
    echo is_user_logged_in()?'yes':'no';
    die();
}
add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');


function wpse_131562_redirect() {
    if ( ! is_user_logged_in() && is_product()) {
        // feel free to customize the following line to suit your needs
      $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
        wp_redirect($shop_page_url);
        exit;
    } else if (! is_user_logged_in() && is_page(310)) {
      wp_redirect(home_url());
      exit;
    }
}
add_action('template_redirect', 'wpse_131562_redirect');




/**remove the required username field form registration ***/
add_action( 'bp_core_validate_user_signup', 'custom_validate_user_signup' );

function custom_validate_user_signup($result)
{
  unset($result['errors']->errors['user_name']);

  if(!empty($result['user_email']) && empty($result['errors']->errors['user_email']))
  {
    $result['user_name'] = md5($result['user_email']);
    $_POST['signup_username'] = $result['user_name'];
  }

  return $result;
}

// Change default WordPress email address
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');
 
function new_mail_from($old) {
return 'info@cscrate.com';
}
function new_mail_from_name($old) {
return 'SC CRATE';
}

/*add_action( 'user_register', 'myplugin_registration_save', 10, 1 );

function myplugin_registration_save( $user_id ) {

   if ( isset( $_POST['username'] ) )

      update_user_meta( $user_id ,'billing_first_name',$_POST['username']) ;
    if ( isset( $_POST['userlastname'] ) )
      update_user_meta( $user_id ,'billing_last_name',$_POST['userlastname']) ;

}*/


/***** force using email for username ***/
//remove wordpress authentication
/*remove_filter('authenticate', 'wp_authenticate_username_password', 20);
add_filter('authenticate', function($user, $email, $password){
 
    //Check for empty fields
        if(empty($email) || empty ($password)){        
            //create new error object and add errors to it.
            $error = new WP_Error();
 
            if(empty($email)){ //No email
                $error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.'));
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
                $error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.'));
            }
 
            if(empty($password)){ //No password
                $error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.'));
            }
 
            return $error;
        }
 
        //Check if user exists in WordPress database
        $user = get_user_by('email', $email);
 
        //bad email
        if(!$user){
            $error = new WP_Error();
            $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
            return $error;
        }
        else{ //check password
            if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
                $error = new WP_Error();
                $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
                return $error;
            }else{
                return $user; //passed
            }
        }
}, 20, 3);*/

/**
 * Only allow one payment plan purchase (Subscription) at a time
 */
 
/*add_filter( 'woocommerce_add_to_cart_validation', 'woo_block_sub', 10, 2 );
function woo_block_sub( $valid, $product_id ) {
    // Get the current product
    $current_product = wc_get_product( $product_id );

    // See if the current product user's are viewing is a subscription product
    if ( $current_product instanceof WC_Product_Subscription || $current_product instanceof WC_Product_Variable_Subscription ) {
        foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
            // Loop through all products in the cart
            $_product = $values['data'];
            // Check if current item is a subscription type
            if( $_product instanceof WC_Product_Subscription || $_product instanceof WC_Product_Subscription_Variation ) {
                // Display a notice and cancel the addition of item to cart
                wc_add_notice( "Only one payment plan can be purchased." );
                return false;
            }
        }
    }
    return $valid;
}*/

function check_if_user_in_group($group_name){
  $user_id = get_current_user_id();
    $group = Groups_Group::read_by_name( $group_name);
    if ( Groups_User_Group::read( $user_id, $group->group_id ) ) { 
        return true;
    } else {
          return false;
    }
}

add_action('init','check_if_user_in_group');

add_action( 'wp_print_scripts', 'DisableStrongPW', 100 );

function DisableStrongPW() {
    if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
        wp_dequeue_script( 'wc-password-strength-meter' );
    }
}

function wooc_extra_register_fields() {
    ?>

    <p class="form-row form-row-first">
    <!-- <label for="reg_billing_first_name"><?php // _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label> -->
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" placeholder="FIRST NAME" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>

    <p class="form-row form-row-last">
    <!-- <label for="reg_billing_last_name"><?php // _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label> -->
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" placeholder="LAST NAME" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>

    <!-- <div class="clear"></div> -->

    <?php
}
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );
// Add the code below to your theme's functions.php file to add a confirm password field on the register form under My Accounts.
add_filter('woocommerce_registration_errors', 'registration_errors_validation', 10,3);
function registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
  global $woocommerce;
  extract( $_POST );
  if ( strcmp( $password, $password2 ) !== 0 ) {
    return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
  }
  return $reg_errors;
}
add_action( 'woocommerce_register_form', 'wc_register_form_password_repeat' );
function wc_register_form_password_repeat() {
  ?>
  <p class="form-row form-row-wide">
    <!-- <label for="reg_password2"><?php // _e( 'Password Repeat', 'woocommerce' ); ?> <span class="required">*</span></label> -->
    <input type="password" class="input-text" name="password2" id="reg_password2" placeholder="PASSWORD" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
  </p>
  <p class="form-row form-row-wide">
  <!-- <label for="reg_billing_last_name"><?php // _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label> -->
  <input type="text" class="url" name="steam_id" id="reg_steam_id" placeholder="STEAM TRADE URL" value="<?php if ( ! empty( $_POST['steam_id'] ) ) esc_attr_e( $_POST['steam_id'] ); ?>" />
  </p>
  <?php
}

add_action( 'woocommerce_login_form_end', 'wc_login_steam' );
add_action( 'woocommerce_register_form_end', 'wc_login_steam' );
function wc_login_steam() {
  ?>
  <p  class="form-row form-row-wide">OR</p>
  <p class="form-row form-row-wide">
  <?php
    if ( !is_user_logged_in() ) {
    echo '<a href="'.wpsap_button_login_url().'" title="Steam"><img src="' . get_stylesheet_directory_uri() . '/images/steam-logo.png" /></a>';
    }
  ?>
  </p>
  <?php
}

/**
 * Validate the extra register fields.
 *
 * @param WP_Error $validation_errors Errors.
 * @param string   $username          Current username.
 * @param string   $email             Current email.
 *
 * @return WP_Error
 */
function wooc_validate_extra_register_fields( $errors, $username, $email ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['steam_id'] ) && empty( $_POST['steam_id'] ) ) {
        $errors->add( 'steam_id_error', __( '<strong>Error</strong>: Steam trade url is required!.', 'woocommerce' ) );
    }
    return $errors;
}
add_filter( 'woocommerce_registration_errors', 'wooc_validate_extra_register_fields', 10, 3 );

/**
 * Save the extra register fields.
 *
 * @param int $customer_id Current customer ID.
 */
function wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        // WordPress default first name field.
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
        // WooCommerce billing first name.
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    }
    if ( isset( $_POST['billing_last_name'] ) ) {
        // WordPress default last name field.
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
        // WooCommerce billing last name.
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    }
    if ( isset( $_POST['steam_id'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $customer_id, 'steam_id', sanitize_text_field( $_POST['steam_id'] ) );
    }
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

/**
 * Redirect non-admins to the homepage after logging into the site.
 *
 * @since   1.0
 */
function acme_login_redirect( $redirect_to, $request, $user  ) {
  return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url('/my-account/');
}
add_filter( 'login_redirect', 'acme_login_redirect', 10, 3 );

?>