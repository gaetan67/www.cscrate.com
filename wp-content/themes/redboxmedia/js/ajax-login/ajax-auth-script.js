jQuery(document).ready(function ($) {

    if ($('#login-popup div.woocommerce > ul').hasClass('woocommerce-error')) {
        // $('#login-popup').css("display","block");
         // formtoFadeIn = $('#login-popup');
         // formtoFadeIn.fadeIn();
         // 
         $('body').prepend('<div class="login_overlay"></div>');
        $('#login-popup').fadeIn(500);
    }



    // Display form from link inside a popup
	$('#pop_login, #pop_signup').live('click', function (e) {
        //formToFadeOut = $('form#register');
        //formtoFadeIn = $('form#login');
        formtoFadeIn = $('#login-popup');
        /*if ($(this).attr('id') == 'pop_signup') {
            formToFadeOut = $('form#login');
            formtoFadeIn = $('form#register');
        }
        formToFadeOut.fadeOut(500, function () {
            formtoFadeIn.fadeIn();
        })*/
        formtoFadeIn.fadeIn();
        return false;
    });
	// Close popup
    $(document).on('click', '.login_overlay, .close', function () {
		/*$('form#login, form#register').fadeOut(500, function () {
            $('.login_overlay').remove();
        });*/
        $('#login-popup').fadeOut(500, function () {
            $('.login_overlay').remove();
        });
        return false;
    });

    // Show the login/signup popup on click
    $('#show_login, #show_signup, .show_login').on('click', function (e) {
        $('body').prepend('<div class="login_overlay"></div>');
        /*if ($(this).attr('id') == 'show_login') 
			$('form#login').fadeIn(500);
        else 
			$('form#register').fadeIn(500);*/
        $('#login-popup').fadeIn(500);
        e.preventDefault();
    });

	// Perform AJAX login/register on form submit
	$('form#login').on('submit', function (e) {
        if (!$(this).valid()) return false;
        $('p.status', this).show().text(ajax_auth_object.loadingmessage);
		action = 'ajaxlogin';
		username = 	$('form#login #username').val();
		password = $('form#login #password').val();
		email = '';
		security = $('form#login #security').val();
		
		ctrl = $(this);
		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                
                'password': password,
                
                'security': security
            },
            success: function (data) {
				$('p.status', ctrl).text(data.message);
				if (data.loggedin == true) {
                    document.location.href = ajax_auth_object.redirecturl;
                }
            }
        });
        e.preventDefault();
    });
    $('form#register').on('submit', function (e) {
        if (!$(this).valid()) return false;
        $('p.status', this).show().text(ajax_auth_object.loadingmessage);
        
       
            action = 'ajaxregister';
            username = $('#signonname').val();
            userlastname = $('#signonlastname').val();
            password = $('#signonpassword').val();
            steam_id = $('#steam_id').val();
            email = $('#email').val();
            security = $('#signonsecurity').val();  

        
        ctrl = $(this);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                'userlastname': userlastname,
                'password': password,
                'steam_id': steam_id,
                'email': email,
                'security': security
            },
            success: function (data) {
                $('p.status', ctrl).text(data.message);
                if (data.loggedin == true) {
                    document.location.href = ajax_auth_object.redirecturl;
                }
            }
        });
        e.preventDefault();
    });
    /*$('form#login, form#register').on('submit', function (e) {
        if (!$(this).valid()) return false;
        $('p.status', this).show().text(ajax_auth_object.loadingmessage);
        action = 'ajaxlogin';
        username =  $('form#login #username').val();
        password = $('form#login #password').val();
        email = '';
        security = $('form#login #security').val();
        if ($(this).attr('id') == 'register') {
            action = 'ajaxregister';
            username = $('#signonname').val();
            userlastname = $('#signonlastname').val();
            password = $('#signonpassword').val();
            steam_id = $('#steam_id').val();
            email = $('#email').val();
            security = $('#signonsecurity').val();  
        }  
        ctrl = $(this);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                'userlastname': userlastname,
                'password': password,
                'steam_id': steam_id,
                'email': email,
                'security': security
            },
            success: function (data) {
                $('p.status', ctrl).text(data.message);
                if (data.loggedin == true) {
                    document.location.href = ajax_auth_object.redirecturl;
                }
            }
        });
        e.preventDefault();
    });*/
	$.validator.addMethod("steamID", function(value, element) {
        return this.optional(element) || /^STEAM_[01]:[01]:\d+$/i.test(value);
    }, "Invalid Steam ID (STEAM_x:x:xxxxxxxx)");


	// Client side form validation
    if (jQuery("#register").length) 
		jQuery("#register").validate(
		{ 
            

			/*rules:{
			password2:{ equalTo:'#signonpassword' 
			}*/
            rules: {
                //steam_id: "required steamID",
                signonpassword: {
                    required: true,
                    minlength: 5
                },
                password_signonpassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#signonpassword"
                }
            
    		}
		});
    else if (jQuery("#login").length) 
		jQuery("#login").validate();
});