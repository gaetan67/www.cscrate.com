<?php 
/*template name: Home */
get_header(); 
$date_array = array('2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026','2027','2028');
$mois_array = array('January', 'February', 'March','April','May','June','July','August','September','October','November','December');
?>
	
<?php nectar_page_header($post->ID); ?>

<div class="container-wrap">
	
	<div class="container main-content">
		
		<div class="row">
			<div class="wrap-started-plans">
				<div class="started">
					<h2><?php echo get_field('get_started_title'); ?></h2>
					<div class="steps">
						<?php if( have_rows('step') ): ?>
							<?php $i = 1; while( have_rows('step') ): the_row(); 

								// vars
								$title = get_sub_field('title');
								$texte = get_sub_field('texte');


								?>

								<div class="step">


								    <h3><?php echo $title; ?></h3>
									<?php echo $texte; ?>
									<div class="step-number">
										step <?php echo $i; ?>
									</div>

								</div>

							<?php $i++; endwhile; ?>

							

						<?php endif; ?>
						
						<div style="clear: both;"></div>
					</div>
				</div>
				<div class="rows-down">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/rows-down.png">
				</div>
				<div class="theplans">
					<h2><?php echo get_field('plans_title'); ?></h2>
					<div class="plans">
						<?php if( have_rows('plan') ): ?>
							<?php $i = 'A'; while( have_rows('plan') ): the_row(); 

								// vars
								$title = get_sub_field('title');
								$texte = get_sub_field('texte');
								$plan_cost = get_sub_field('plan_cost');
								$plan_link = get_sub_field('plan_link', false, false);


								?>

								<div class="plan plan_<?php echo $i; ?>">
									<h3><?php echo $title; ?> <br/><span>Plan</span></h3>
									<?php if(is_user_logged_in()) { ?>
										<a class="select " href="/?add-to-cart=<?php echo  $plan_link;//get_permalink( 21 );//do_shortcode('[add_to_cart_url id="21"]'); ?>" title="Select"><?php echo __('Select','redbox'); ?></a>
										
									<?php } else { ?> 
										<a class="select mobile" href="/my-account" title="Select"><?php echo __('Select','redbox'); ?></a>
										<a class="select show_login non-mobile" href="#" title="Select"><?php echo __('Select','redbox'); ?></a>
									<?php }  ?>
									<?php echo $texte; ?>
									<div class="plan-price">
										<?php echo $plan_cost; ?><span class="space"></span>$<span class="per-month">
											Per
											<div class="month">
											month
											</div>
										</span>
									</div>
								</div>

							<?php $i++; endwhile; ?>

							

						<?php endif; ?>
						
						<div style="clear: both;"></div>
					</div>
				</div>
			</div>
			<!--- articles to winn -->
			<div class="top-item">
				<div class="wrap-h2">
					<h2><?php echo get_field('top_items_title'); ?></h2>
				</div>
				<div class="items">
					<?php 
						$args = array( 
							'post_type' => 'crate',
							'tax_query' => array(
								array(
									'taxonomy' => 'category_crate',
									'field'    => 'name',
									'terms'    => date('F'),
								),
							),
            				'meta_query' => array(
                                array('key' => 'position',
							        'value'   => array(''),
							        'compare' => 'NOT IN'
                                )
                            ), 
							'orderby' => array(
								'meta_value' => 'ASC',
								'post_date'=> 'DESC',
								 //or 'meta_value_num'
                            )
						);
						$loop = new WP_Query( $args );
						$i = 1; 

						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php //print_r($terms);
							 $laPosition = get_post_meta($post->ID,'position');
							if( $laPosition[0] == $i){ ?>
								<?php $terms = get_the_terms( $post->ID , 'category_crate' ); 
									// echo "<pre>";
									// var_dump($terms);
									// echo "</pre>";
								?>
								<div class="item">
									<div class="image" style="background-image: url(<?php  the_post_thumbnail_url();//echo $photo['url']; ?>"></div>
									<div class="price"><?php echo get_field('crate_price') ; ?> $<span class="value-note">value</span></div>
									<?php 
										foreach ($terms as $term) {
											if (!in_array($term->name,$date_array) && !in_array($term->name,$mois_array)) { ?>
												<h3><?php echo $term->name; //$categoy; ?><span class="name"><?php echo get_the_title(); ?></span></h3>
									<?php break;	}
										}
									?>
									
									<div class="number">
										<?php echo $i; ?>
									</div>
									
								</div>
							<?php $i++; } ?>
						  
						<?php  endwhile;
					?>
					
					
					<div style="clear: both;"></div>
				</div>

				
			</div>
			<div style="clear: both;"></div>

			
			<?php ?>
				
	
		</div><!--/row-->
		
	</div><!--/container-->
	
	
</div>
	
<?php get_footer(); ?>