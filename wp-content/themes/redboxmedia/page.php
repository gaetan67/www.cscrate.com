<?php get_header(); ?>

<?php nectar_page_header($post->ID); ?>

<div class="container-wrap">
	
	<div class="container main-content page">
		
		<div class="row">
			<?php if(is_account_page()){ ?>
				

				<?php

				//$points_balance = WC_Points_Rewards_Manager::get_users_points( get_current_user_id() );
				$points_balance = 2;
    			
				?>

				<?php if(is_user_logged_in()) { ?>
			
				<div class="woocommerce">
					<div class="wrap_btn_credit">
						<div class="mycredits">
							<span class="title"><?php echo __('My credits','redbox'); ?> :</span>
							<span id="mycredits"><?php echo $points_balance; ?></span>$
						</div>
						<a href="<?php echo get_permalink(310); ?>" title="Refer a friend" class="refer_btn">
							<div class="hover">Refer a friend</div>
							Earn 5$
							
						</a>
					</div>
					<div style="clear: both;"></div>
				</div>

				<?php } ?>
			<?php } ?>
			<h1><?php the_title(); ?></h1>
			<div class="wrap">
				<?php 

				//breadcrumbs
				//if ( function_exists( 'yoast_breadcrumb' ) && !is_home() && !is_front_page() ){ yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } 

				 //buddypress
				 global $bp; 
				 if($bp && !bp_is_blog_page()) echo '<h1>' . get_the_title() . '</h1>'; ?>
				
				<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
					
					<?php the_content(); ?>
		
				<?php endwhile; endif; ?>
			</div>
				
	
		</div><!--/row-->
		
	</div><!--/container-->
	
</div>
<?php get_footer(); ?>