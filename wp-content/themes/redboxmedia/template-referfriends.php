<?php 
/*template name: Refer a friend */
get_header(); ?>
	
<?php nectar_page_header($post->ID); ?>

<div class="container-wrap">
	
	<div class="container main-content refer_friend">
		
		<div class="row">
			<h1><?php echo get_the_title(); ?></h1>
			<div class="wrap">
				<?php
				//buddypress
				 /*global $bp; 
				 if($bp && !bp_is_blog_page()) echo '<h1>' . get_the_title() . '</h1>'; ?>*/
				
				 if(have_posts()) : while(have_posts()) : the_post(); ?>
					
					<?php the_content(); ?>
					<div class="refer_friend_form">
						<?php echo do_shortcode( '[woocommerce_refferal_form]'); ?>
					</div>
		
				<?php endwhile; endif; ?>
			</div>
			
				
	
		</div><!--/row-->
		
	</div><!--/container-->
	
	
</div>
	
<?php get_footer(); ?>