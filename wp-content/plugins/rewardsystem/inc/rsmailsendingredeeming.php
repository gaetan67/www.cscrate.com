<?php

foreach (get_users() as $myuser) {
    $user = get_userdata($myuser->ID);
    $to = $user->user_email;
    $subject = $emails->subject;
    $firstname = $user->user_firstname;
    $lastname = $user->user_lastname;
    $url_to_click = "<a href=" . site_url() . ">" . site_url() . "</a>";
    $userpoint = get_user_meta($myuser->ID, '_my_reward_points', true);
    $minimumuserpoints = $emails->minimum_userpoints;
    if ($minimumuserpoints == '') {
        $minimumuserpoints = 0;
    } else {
        $minimumuserpoints = $emails->minimum_userpoints;
    }
    if ($minimumuserpoints < $userpoint) {

        $getredeemedpoints = get_user_meta($myuser->ID, '_redeemed_points', true);
        if ($getredeemedpoints >= $emails->redeemingpoints) {
            $message = str_replace('{rssitelink}', $url_to_click, $emails->message);
            $message = str_replace('{rsfirstname}', $firstname, $message);
            $message = str_replace('{rslastname}', $lastname, $message);
            $message = str_replace('{rspoints}', $userpoint, $message);
            $message = do_shortcode($message); //shortcode feature
            ob_start();
            wc_get_template('emails/email-header.php', array('email_heading' => $subject));
            echo $message;
            wc_get_template('emails/email-footer.php');
            $woo_temp_msg = ob_get_clean();
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            if ($emails->sender_opt == 'local') {
                // $headers .= "From: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                $headers .= 'From: ' . $emails->from_name . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                //$headers .= "Reply-To: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
            } else {
                // $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                $headers .= 'From: ' . get_option('woocommerce_email_from_name') . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                //$headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
            }
            //wp_mail($to, $subject, $woo_temp_msg, $headers='');
            if ('2' == get_option('rs_select_mail_function')) {
                if (wp_mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                }
            } elseif ('1' == get_option('rs_select_mail_function')) {
                if (mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                }
            } else {
                if (wp_mail($to, $subject, $woo_temp_msg, $headers = '')) {

                }
            }
        }
    }
}
?>
