<?php

class FPRewardSystemPointsVoucher {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_point_vouchers'] = __('Gift Voucher', 'rewardsystem');
        return $settings_tabs;
    }

// Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_point_vouchers_settings', array(
            array(
                'name' => __('Gift Voucher', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_point_vouchers'
            ),
            array(
                'name' => 'test',
                'type' => 'point_vouchers',
            ),
            array('type' => 'sectionend', 'id' => '_rs_point_vouchers'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemPointsVoucher::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemPointsVoucher::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemPointsVoucher::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function reward_system_my_account_voucher_redeem() {
        //delete_option('rsvoucherlists');
        //delete_user_meta(get_current_user_id(), '_my_points_log');
        ?>
        <h3><?php _e('Redeem your Gift Voucher', 'rewardsystem'); ?></h3>
        <input type="text" size="50" name="rs_redeem_voucher" id="rs_redeem_voucher_code" value=""><input type="submit" style="margin-left:10px;" class="button <?php echo get_option('rs_extra_class_name_redeem_gift_voucher_button'); ?>" name="rs_submit_redeem_voucher" id="rs_submit_redeem_voucher" value="<?php _e('Redeem Gift Voucher', 'rewardsystem'); ?>"/>
        <div class="rs_redeem_voucher_error" style="color:red;"></div>
        <div class="rs_redeem_voucher_success" style="color:green"></div>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#rs_submit_redeem_voucher').click(function () {

                    jQuery(this).prop("disabled", true);
                    var redeemvouchercode = jQuery('#rs_redeem_voucher_code').val();
                    if (redeemvouchercode === '') {
                        jQuery('.rs_redeem_voucher_error').html('<?php _e("Please Enter your Voucher Code", 'rewardsystem'); ?>');
                        return false;
                    } else {
                        jQuery('.rs_redeem_voucher_error').html('');
                        var dataparam = ({
                            action: 'rewardsystem_redeem_voucher_codes',
                            redeemvouchercode: redeemvouchercode,
                        });
                        jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                function (response) {
                                    console.log(jQuery.parseHTML(response));
                                    jQuery('.rs_redeem_voucher_success').html(jQuery.parseHTML(response));
                                    //jQuery(".voucher_rs_list_table").load(window.location + " .voucher_rs_list_table");
                                    jQuery('#rs_submit_redeem_voucher').prop("disabled", false);
                                });
                        return false;
                    }
                });
            });
        </script>
        <?php
    }

    public static function rewardsystem_myaccount_voucher_redeem_shortcode($content) {
        ob_start();
        ?>
        <h3><?php _e('Redeem your Gift Voucher', 'rewardsystem'); ?></h3>
        <input type="text" size="50" name="rs_redeem_voucher" id="rs_redeem_voucher_code" value=""><input type="submit" style="margin-left:10px;" class="button <?php echo get_option('rs_extra_class_name_redeem_gift_voucher_button'); ?>" name="rs_submit_redeem_voucher" id="rs_submit_redeem_voucher" value="<?php _e('Redeem Gift Voucher', 'rewardsystem'); ?>"/>
        <div class="rs_redeem_voucher_error" style="color:red;"></div>
        <div class="rs_redeem_voucher_success" style="color:green"></div>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#rs_submit_redeem_voucher').click(function () {

                    jQuery(this).prop("disabled", true);
                    var redeemvouchercode = jQuery('#rs_redeem_voucher_code').val();
                    if (redeemvouchercode === '') {
                        jQuery('.rs_redeem_voucher_error').html('<?php _e("Please Enter your Voucher Code", 'rewardsystem'); ?>');
                        return false;
                    } else {
                        jQuery('.rs_redeem_voucher_error').html('');
                        var dataparam = ({
                            action: 'rewardsystem_redeem_voucher_codes',
                            redeemvouchercode: redeemvouchercode,
                        });
                        jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                function (response) {
                                    console.log(jQuery.parseHTML(response));
                                    jQuery('.rs_redeem_voucher_success').html(jQuery.parseHTML(response));
                                    //jQuery(".voucher_rs_list_table").load(window.location + " .voucher_rs_list_table");
                                    jQuery('#rs_submit_redeem_voucher').prop("disabled", false);
                                });
                        return false;
                    }
                });
            });
        </script>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public static function search($array, $key, $value) {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, FPRewardSystemPointsVoucher::search($subarray, $key, $value));
            }
        }

        return $results;
    }

    public static function process_ajax_request_to_redeem_voucher_reward_system() {
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
        $banned_user_list = get_option('rs_banned-users_list');
        if (!in_array(get_current_user_id(), $banned_user_list)) {
               $getarrayofuserdata = get_userdata(get_current_user_id());
                $banninguserrole = get_option('rs_banning_user_role');
                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
            if (isset($_POST['redeemvouchercode'])) {

                if (is_array(get_option('rsvoucherlists'))) {
                    foreach (get_option('rsvoucherlists')as $newones) {
                        if (!array_key_exists($_POST['redeemvouchercode'], $newones)) {
                            $newone[] = $newones;
                        }
                    }
                }
                // var_dump($newone);
                //exit();
                // echo "<pre>";
                //  var_dump(array_filter(array_intersect_key($newone, array_flip(array($_POST['redeemvouchercode'])))));
                // $findedarray = array_intersect_key($newone, array_flip(array('final')));

                $findedarray = FPRewardSystemPointsVoucher::search(get_option('rsvoucherlists'), 'vouchercode', $_POST['redeemvouchercode']);


                // echo "</pre>";
                if (($findedarray == NULL) || ($findedarray == '')) {
                    echo "Sorry, Voucher not found in a list";
                    exit();
                } else {

                    $todays_date = date("Y-m-d");
                    $today = strtotime($todays_date);
                    $exp_date = $findedarray[0]['voucherexpiry'];
                    $vouchercreated = $findedarray[0]['vouchercreated'];
                    $voucherused = $findedarray[0]['voucherused'];
                    $voucherpoints = $findedarray[0]['points'];
                    if ($voucherused == '') {
                        if ($exp_date != '') {
                            $expiration_date = strtotime($exp_date);
                            if ($expiration_date > $today) {
                                $voucherpoints = $findedarray[0]['points'];
                                $previouspoints = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                                update_user_meta(get_current_user_id(), '_my_reward_points', $previouspoints + $voucherpoints);

                                $myrewards = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                                $pointslogs[] = array('orderid' => '', 'userid' => get_current_user_id(), 'points_earned_order' => $voucherpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $myrewards, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => 'Redeem Voucher Code ' . $_POST['redeemvouchercode'], 'rewarder_for_frontend' => 'Redeem Voucher Code ' . $_POST['redeemvouchercode']);
                                $overalllogs[] = array('userid' => get_current_user_id(), 'totalvalue' => $myrewards, 'eventname' => 'Redeem Voucher Code ' . $_POST['redeemvouchercode'], 'date' => date('Y-m-d H:i:s'));
                                $getoveralllogs = get_option('rsoveralllog');
                                $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                                update_option('rsoveralllog', $logmerges);
                                $getmypointss = get_user_meta(get_current_user_id(), '_my_points_log', true);
                                $mergeds = array_merge((array) $getmypointss, $pointslogs);
                                update_user_meta(get_current_user_id(), '_my_points_log', $mergeds);
                                echo "<strong>" . $voucherpoints . "</strong> added to your Total Reward Points";
                            } else {
                                echo "Voucher has been Expired";
                            }
                        } else {
                            // Coupon Never Expired
                            $voucherpoints = $findedarray[0]['points'];
                            $previouspoints = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                            update_user_meta(get_current_user_id(), '_my_reward_points', $previouspoints + $voucherpoints);
                            $myrewards = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                            $pointslogs[] = array('orderid' => '', 'userid' => get_current_user_id(), 'points_earned_order' => $voucherpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $myrewards, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => 'Redeem Voucher Code ' . $_POST['redeemvouchercode'], 'rewarder_for_frontend' => 'Redeem Voucher Code ' . $_POST['redeemvouchercode']);
                            $overalllogs[] = array('userid' => get_current_user_id(), 'totalvalue' => $myrewards, 'eventname' => 'Redeem Voucher Code ' . $_POST['redeemvouchercode'], 'date' => date('Y-m-d H:i:s'));
                            $getoveralllogs = get_option('rsoveralllog');
                            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                            update_option('rsoveralllog', $logmerges);
                            $getmypointss = get_user_meta(get_current_user_id(), '_my_points_log', true);
                            $mergeds = array_merge((array) $getmypointss, $pointslogs);
                            update_user_meta(get_current_user_id(), '_my_points_log', $mergeds);
                            echo "<strong>" . $voucherpoints . "</strong> added to your Total Reward Points";
                        }

                        //var_dump($newone);

                        $updates = array(
                            array(
                                $_POST['redeemvouchercode'] => array('points' => $voucherpoints, 'vouchercode' => $_POST['redeemvouchercode'], 'vouchercreated' => $vouchercreated, 'voucherexpiry' => $exp_date, 'memberused' => get_current_user_id(), 'voucherused' => '1')
                            ),
                        );

                        $array1 = $newone;
                        $array2 = $updates;
                        $array3 = array_merge((array) $array1, (array) $array2);
                        update_option('rsvoucherlists', array_filter($array3));
                        // $newupdatedarray = array_unique(array_merge($newone, $updates));
                        //update_option('rsvoucherlists', $newupdatedarray);
                    } else {
                        echo "Voucher has been used";
                    }
                }
            }
        }
        } else {
            echo "You have Earned 0 Points";
        }
        exit();
    }

}

add_action('wp_ajax_nopriv_rewardsystem_redeem_voucher_codes', array('FPRewardSystemPointsVoucher', 'process_ajax_request_to_redeem_voucher_reward_system'));
add_action('wp_ajax_rewardsystem_redeem_voucher_codes', array('FPRewardSystemPointsVoucher', 'process_ajax_request_to_redeem_voucher_reward_system'));

function rs_point_voucher_field() {
    ?>
    <style type="text/css">
        p.submit {
            display:none;
        }
        #mainforms {
            display:none;
        }
    </style>
    <tr valign="top">
        <th class="titledesc" scope="row">
            <label for="rs_point_voucher_create_option"><?php _e('Voucher Creation', 'rewardsystem'); ?></label>
        </th>
        <td class="forminp forminp-select">
            <input type="radio" name="rs_point_voucher_create_option" class="rs_point_voucher_create_option" id="rs_point_voucher_create_option" value="1" checked="checked"/> Individual Voucher Code<br>
            <input type="radio" name="rs_point_voucher_create_option" class="rs_point_voucher_create_option" id="rs_point_voucher_create_option" value="2"/> Bulk Voucher Code<br/>
        </td>
    </tr>
    <tbody class="rs_bulk_vouchers">
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_bulk_voucher_count">
                    <?php _e('Enter Number of Vouchers to Generate', 'rewardsystem'); ?>
                </label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" id="rs_point_bulk_voucher_count" name="rs_point_bulk_voucher_count" value=""/><em>For Example: 10</em>
            </td>
        </tr>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_prefix_for_gift_voucher"><?php _e('Prefix for Gift Voucher Code', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" name="rs_prefix_for_gift_voucher" id="rs_prefix_for_gift_voucher" value="SRP"/>
            </td>
        </tr>
    <!--        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_bulk_voucher_type"><?php _e('Bulk Voucher Type', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <select name="rs_point_bulk_voucher_type" id="rs_point_bulk_voucher_type">
                    <option value="1" selected="selected">Numeric Voucher Code</option>
                    <option value="2">Alpha Numeric Voucher Code</option>
                </select>
            </td>
        </tr>-->
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_bulk_voucher_points">
                    <?php _e('Enter Bulk Gift Voucher Points', 'rewardsystem'); ?>
                </label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" id="rs_point_bulk_voucher_points" name="rs_point_bulk_voucher_points" value=""/>
            </td>
        </tr>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_bulk_voucher_expiry"><?php _e('Bulk Gift Voucher Expiry', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" class="rs_point_bulk_voucher_expiry" value="" name="rs_point_bulk_voucher_expiry" id="rs_point_bulk_voucher_expiry" />
            </td>
        </tr>
    </tbody>

    <tbody class="rs_individual_vouchers">
        <tr valign ="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_voucher_field"><?php _e('Enter Gift Voucher Code', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" id="rs_point_voucher_field" name="rs_point_voucher_field" value=""/><em>For Example: giftvoucher</em>
            </td>
        </tr>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_voucher_reward_points"><?php _e('Enter Gift Voucher Points'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" id="rs_point_voucher_reward_points" name="rs_point_voucher_reward_points" value=""/>
            </td>
        </tr>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_voucher_expiry"><?php _e('Gift Voucher Expiry', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" class="rs_point_voucher_expiry" value="" name="rs_point_voucher_expiry" id="rs_point_voucher_expiry" />
            </td>
        </tr>
    </tbody>
    <tr valign="top">
        <td>


        </td>
        <td>
            <input type='submit' name='rs_submit_point_vouchers' id='rs_submit_point_vouchers' class='button-primary' value='Create Voucher'/>
            <div class="vouchererror"></div>
        </td>
    </tr>
    <?php if (isset($_GET['tab'])) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#rs_point_voucher_expiry').datepicker({dateFormat: 'yy-mm-dd'});
                jQuery('#rs_point_bulk_voucher_expiry').datepicker({dateFormat: 'yy-mm-dd'});
            });
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#rs_voucher_lists').footable().bind('footable_filtering', function (e) {
                    var selected = jQuery('.filter-status').find(':selected').text();
                    if (selected && selected.length > 0) {
                        e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                        e.clear = !e.filter;
                    }
                });
                jQuery('#changepagesizers').change(function (e) {
                    e.preventDefault();
                    var pageSize = jQuery(this).val();
                    jQuery('.footable').data('page-size', pageSize);
                    jQuery('.footable').trigger('footable_initialized');
                });
            });
        </script>
    <?php }
    ?>

    <script type="text/javascript">
        jQuery(function () {
            var checkvouchervalue = jQuery('.rs_point_voucher_create_option').filter(':checked').val();
            if (checkvouchervalue === '1') {
                jQuery('.rs_bulk_vouchers').css('display', 'none');
                jQuery('.rs_individual_vouchers').css('display', 'table-row-group');
            } else {
                jQuery('.rs_individual_vouchers').css('display', 'none');
                jQuery('.rs_bulk_vouchers').css('display', 'table-row-group');
            }
            jQuery('.rs_point_voucher_create_option').change(function () {
                //alert(jQuery(this).val());
                if (jQuery(this).val() === '1') {
                    jQuery('.rs_bulk_vouchers').css('display', 'none');
                    jQuery('.rs_individual_vouchers').css('display', 'table-row-group');
                } else {
                    jQuery('.rs_individual_vouchers').css('display', 'none');
                    jQuery('.rs_bulk_vouchers').css('display', 'table-row-group');
                }
                //return false;
            });

            jQuery('#rs_submit_point_vouchers').click(function () {
                // alert('you clicked create vouchers');
                var vouchercreateoption = jQuery('.rs_point_voucher_create_option').filter(':checked').val();
                //alert(vouchercreateoption);
                // return false;
                jQuery(this).prop("disabled", true);
                if (vouchercreateoption === '1') {
                    var vouchercode = jQuery('#rs_point_voucher_field').val();
                    var voucherpoints = jQuery('#rs_point_voucher_reward_points').val();
                    var voucherexpiry = jQuery('#rs_point_voucher_expiry').val();
                    var dataparam = ({
                        action: 'rewardsystem_point_vouchers',
                        vouchercode: vouchercode,
                        voucherpoints: voucherpoints,
                        vouchercreated: '<?php echo date('Y-m-d'); ?>',
                        voucherexpiry: voucherexpiry,
                    });
                }
                if (vouchercreateoption === '2') {
                    var bulkvouchercount = jQuery('#rs_point_bulk_voucher_count').val();
                    var bulkvoucherprefix = jQuery('#rs_prefix_for_gift_voucher').val();
                    var bulkvoucherpoints = jQuery('#rs_point_bulk_voucher_points').val();
                    var bulkvoucherexpiry = jQuery('#rs_point_bulk_voucher_expiry').val();
                    var dataparam = ({
                        action: 'rewardsystem_point_bulk_vouchers',
                        vouchercount: bulkvouchercount,
                        voucherprefix: bulkvoucherprefix,
                        bulkvoucherpoints: bulkvoucherpoints,
                        bulkvouchercreated: '<?php echo date('Y-m-d'); ?>',
                        bulkvoucherexpiry: bulkvoucherexpiry,
                    });
                }
                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                        function (response) {
                            // alert(response);
                            // alert(response);
                            if (response === '1') {
                                jQuery('.vouchererror').html('Unique Code Already Exists');
                            }
                            jQuery(".voucher_rs_list_table").load(window.location + " .voucher_rs_list_table");
                            jQuery('#rs_submit_point_vouchers').prop("disabled", false);
                            return false;
                        });
                return false;
            });


            //            jQuery('.rs_vouchers_click').click(function() {
            //                jQuery(this).parent().parent().hide();
            //                var uniquecode = jQuery(this).attr('data-code');
            //                var dataparameter = ({
            //                    action: 'rewardsystem_delete_array',
            //                    deletecode: uniquecode,
            //                });
            //                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparameter,
            //                        function(response) {
            //                            //    alert(response);
            //                            jQuery(".voucher_rs_list_table").load(window.location + " .voucher_rs_list_table");
            //                        });
            //                return false;
            //            });
        });
    </script>
    <style type="text/css">
        .rs_vouchers_click {

            border: 2px solid #a1a1a1;
            padding: 3px 9px;
            background: #dddddd;
            width: 5px;
            border-radius: 25px;
        }
        .rs_vouchers_click:hover {
            cursor: pointer;
            background:red;
            color:#fff;
            border: 2px solid #fff;
        }
    </style>
    <table>
        <tr valign="top">
            <td>
                <?php
                echo '<p> ' . __('Search:', 'rewardsystem') . '<input id="filterings_vouchers" type="text"/>  ' . __('Page Size:', 'rewardsystem') . '
                <select id="changepagesizers">
									<option value="5">5</option>
									<option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
                ?>

        </tr>
    </table>
    <tr>

    <table class="wp-list-table widefat fixed posts voucher_rs_list_table " data-filter = "#filterings_vouchers" data-page-size="10" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" id="rs_voucher_lists" >
        <script type="text/javascript">
            jQuery(function () {
                jQuery(document).on('click', '.rs_vouchers_click', function () {
                    var uniquecode = jQuery(this).attr('data-code');
                    var dataparameter = ({
                        action: 'rewardsystem_delete_array',
                        deletecode: uniquecode,
                    });
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparameter,
                            function (response) {
                                //    alert(response);
                                jQuery(".voucher_rs_list_table").load(window.location + " .voucher_rs_list_table");

                            });
                    return false;
                });
            });
        </script>

        <thead>
            <tr>
                <th scope='col' data-toggle="true"  class='manage-column column-serial_number'  style="">
                    <a href="#"><span><?php _e('S.No', 'rewardsystem'); ?></span>
                </th>
                <th scope='col' id='rs_voucher_codes' class='manage-column column-rs_voucher_codes'  style=""><?php _e('Voucher Code', 'rewardsystem'); ?></th>
                <th scope='col' id='rs_points_assigned' class='manage-column column-rs_points_assigned'  style=""><?php _e('Points Assigned', 'rewardsystem'); ?></th>
                <th scope="col" id="rs_voucher_created" class="manage-column column-rs_voucher_created" style=""><?php _e('Voucher Created', 'rewardsystem'); ?></th>
                <th scope="col" id="rs_voucher_expiry" class="manage-column column-rs_voucher_expiry" style=""><?php _e('Voucher Expiry', 'rewardsystem'); ?></th>
                <th scope="col" id="rs_voucher_used" class="manage-column column-rs_voucher_used" style=""><?php _e('Voucher used by', 'rewardsystem'); ?></th>
                <th scope="col" id="rs_delete_vouchers" class="manage-column column-rs_delete_vouchers" style=""><?php _e('Delete', 'rewardsystem'); ?></th>
            </tr>
        </thead>
        <tbody id="the-list">
            <?php
            // var_dump($blogusers);
            $i = 1;
            $checkvalues = get_option('rsvoucherlists');
            if (!empty($checkvalues)) {
                foreach ($checkvalues as $voucher) {
                    foreach ($voucher as $value) {
                        // echo $eachuser->ID;
                        if ($i % 2 != 0) {
                            $name = 'alternate';
                        } else {
                            $name = '';
                        }
                        ?>
                        <tr id="post-141"  class="type-shop_order status-publish post-password-required hentry <?php echo $name; ?> iedit author-self level-0" valign="top">
                            <td data-value="<?php echo $i; ?>">
                                <?php echo $i; ?>
                            </td>
                            <td class="rs_user_name">
                                <?php echo $value['vouchercode']; ?>
                            </td>
                            <td>
                                <?php echo $value['points']; ?>
                            </td>
                            <td>
                                <?php echo $value['vouchercreated']; ?>
                            </td>
                            <td>
                                <?php
                                if ($value['voucherexpiry'] != '') {
                                    echo $value['voucherexpiry'];
                                } else {
                                    echo "Never";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value['memberused'] != '') {
                                    $userinfo = get_userdata($value['memberused']);
                                    echo $userinfo->user_login;
                                } else {
                                    echo "Not Yet";
                                }
                                ?>
                            </td>
                            <td>
                                <div data-code="<?php echo $value['vouchercode']; ?>" class="rs_vouchers_click">x</div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
            }
            ?>
        </tbody>

    </table>
    <div style="clear:both;">
        <div class="pagination pagination-centered"></div>
    </div>

    </tr>

    <?php
}

function process_ajax_request_rs_point_vouchers() {
    if (isset($_POST['vouchercode']) && ($_POST['voucherpoints'])) {

        $checkifexists = get_option('rsvoucherlists');
        if (!empty($checkifexists)) {
            foreach (get_option('rsvoucherlists') as $updates) {
// var_dump($updates);
                if (!array_key_exists($_POST['vouchercode'], $updates)) {
                    $newupdatess[] = $updates;
                } else {
                    echo "1";
                    exit();
                }
            }

            $newupdates = array(
                array(
                    $_POST['vouchercode'] => array('points' => $_POST['voucherpoints'], 'vouchercode' => $_POST['vouchercode'], 'vouchercreated' => $_POST['vouchercreated'], 'voucherexpiry' => $_POST['voucherexpiry'], 'memberused' => '', 'voucherused' => '')
                ),
            );
            $array1 = (array) $newupdatess;
// var_dump($array1);
            $array2 = $newupdates;
            $array3 = array_merge($array1, $array2);
            update_option('rsvoucherlists', array_filter($array3));
        } else {

            $newupdates = array(
                array(
                    $_POST['vouchercode'] => array('points' => $_POST['voucherpoints'], 'vouchercode' => $_POST['vouchercode'], 'vouchercreated' => $_POST['vouchercreated'], 'voucherexpiry' => $_POST['voucherexpiry'], 'memberused' => '', 'voucherused', '')
                ),
            );
            update_option('rsvoucherlists', $newupdates);
        }
    }
    exit();
}

function process_ajax_request_for_rs_bulk_point_vouchers() {
    if (isset($_POST['vouchercount']) && ($_POST['bulkvoucherpoints'])) {





        $checkifexists = get_option('rsvoucherlists');
        if (!empty($checkifexists)) {

            for ($i = 1; $i <= $_POST['vouchercount']; $i++) {
                $num = mt_rand(1000001, 9999998);
                $output = sprintf('%07x', $num);
                $newvouchercode[] = $_POST['voucherprefix'] . $output;
            }
            // var_dump($newvouchercode);
            //exit();
            foreach (get_option('rsvoucherlists') as $updates) {
// var_dump($updates);
                //var_dump($updates);
                //var_dump($updates);
                foreach ($newvouchercode as $codess) {
                    if (!array_key_exists($codess, $updates)) {
                        $newupdatess[] = array_filter($updates);
                    } else {
                        echo "1";
                    }
                }
            }
            foreach ($newvouchercode as $newcodess) {
                //var_dump($newcodess);
                // var_dump($newupdatess);
                //var_dump(get_option('rsvoucherlists'));
                var_dump($newcodess);
                $newupdates = array(
                    array(
                        $newcodess => array('points' => $_POST['bulkvoucherpoints'], 'vouchercode' => $newcodess, 'vouchercreated' => $_POST['bulkvouchercreated'], 'voucherexpiry' => $_POST['bulkvoucherexpiry'], 'memberused' => '', 'voucherused' => '')
                    ),
                );
                $array1 = (array) $newupdatess;
// var_dump($array1);
                $array2 = $newupdates;
                $array3 = array_merge((array) get_option('rsvoucherlists'), $array2);

                $array3 = array_map("unserialize", array_unique(array_map("serialize", $array3)));
                //var_dump($array3);
                update_option('rsvoucherlists', array_filter($array3));
            }
        } else {
            for ($i = 1; $i <= $_POST['vouchercount']; $i++) {
                $num = mt_rand(1000001, 9999998);
                $output = sprintf('%07x', $num);
                $newvouchercode[] = $_POST['voucherprefix'] . $output;
            }
            foreach ($newvouchercode as $newcde) {
                $newupdates = array(
                    array(
                        $newcde => array('points' => $_POST['bulkvoucherpoints'], 'vouchercode' => $newcde, 'vouchercreated' => $_POST['bulkvouchercreated'], 'voucherexpiry' => $_POST['bulkvoucherexpiry'], 'memberused' => '', 'voucherused', '')
                    ),
                );
                $array2 = $newupdates;
                $array3 = array_merge((array) get_option('rsvoucherlists'), $array2);

                $array3 = array_map("unserialize", array_unique(array_map("serialize", $array3)));
                //var_dump($array3);
                update_option('rsvoucherlists', array_filter($array3));
            }
        }
    }
    exit();
}

function delete_array_keys_rs_point_vouchers() {
    if (isset($_POST['deletecode'])) {
        $checkifexists = get_option('rsvoucherlists');
        if (!empty($checkifexists)) {
            foreach (get_option('rsvoucherlists') as $updates) {
// var_dump($updates);
                if (array_key_exists($_POST['deletecode'], $updates)) {
                    unset($updates);
                }
                $newupdates[] = $updates;
            }
//var_dump($newupdates);
            $new_array_without_nulls = array_filter($newupdates);
            update_option('rsvoucherlists', $new_array_without_nulls);
        }
    }
    exit();
}

//delete_option('rsvoucherlists');
new FPRewardSystemPointsVoucher();
if (get_option('rs_show_hide_redeem_voucher') == '1') {
    if (get_option('rs_redeem_voucher_position') == '1') {
        add_action('woocommerce_before_my_account', array('FPRewardSystemPointsVoucher', 'reward_system_my_account_voucher_redeem'));
    } else {
        add_action('woocommerce_after_my_account', array('FPRewardSystemPointsVoucher', 'reward_system_my_account_voucher_redeem'));
    }
}

add_action('wp_ajax_nopriv_rewardsystem_point_vouchers', 'process_ajax_request_rs_point_vouchers');
add_action('wp_ajax_rewardsystem_point_vouchers', 'process_ajax_request_rs_point_vouchers');

add_action('wp_ajax_nopriv_rewardsystem_point_bulk_vouchers', 'process_ajax_request_for_rs_bulk_point_vouchers');
add_action('wp_ajax_rewardsystem_point_bulk_vouchers', 'process_ajax_request_for_rs_bulk_point_vouchers');

add_action('wp_ajax_nopriv_rewardsystem_delete_array', 'delete_array_keys_rs_point_vouchers');
add_action('wp_ajax_rewardsystem_delete_array', 'delete_array_keys_rs_point_vouchers');

/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

add_action('woocommerce_admin_field_point_vouchers', 'rs_point_voucher_field');

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemPointsVoucher', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_point_vouchers', array('FPRewardSystemPointsVoucher', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemPointsVoucher', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_point_vouchers', array('FPRewardSystemPointsVoucher', 'reward_system_register_admin_settings'));


add_shortcode('rs_redeem_vouchercode', array('FPRewardSystemPointsVoucher', 'rewardsystem_myaccount_voucher_redeem_shortcode'));
