<?php

class FPRewardSystemEmailTemplatesTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_emailtemplates'] = __('Email Templates', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_email_templates_settings', array(
            array(
                'name' => __('Email Template', 'rewardsystem'),
                'type' => 'title',
                'id' => 'rs_email_template_settings',
            ),
            array(
                'name' => __('Testing', 'rewardsystem'),
                'type' => 'list_table',
            ),
            array('type' => 'sectionend', 'id' => 'rs_email_template_settings'),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'type' => 'title',
//                'desc' => __('Reward Points/Referral Reward Points awarded for the following Status', 'rewardsystem'),
//                'id' => '_rs_reward_point_order_status',
//            ),
//            array(
//                'name' => __('Award Reward Points on Order Status', 'rewardsystem'),
//                'desc' => __('Completed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_completed',
//                'css' => 'min-width:150px;',
//                'std' => 'yes',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_completed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Pending', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_pending',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_pending',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Failed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_failed',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_failed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('On Hold', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_on-hold',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_on-hold',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Processing', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_processing',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_processing',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Refunded', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_refunded',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_refunded',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Cancelled', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_cancelled',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_cancelled',
//                'desc_tip' => false,
//            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_order_status'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemEmailTemplatesTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemEmailTemplatesTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemEmailTemplatesTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function create_template() {
        global $wpdb;
        if (isset($_POST['rs_template_name'])) {
            $table_name_email = $wpdb->prefix . 'rs_templates_email';
            $wpdb->insert($table_name_email, array('template_name' => stripslashes($_POST['rs_template_name']),
                'sender_opt' => stripslashes($_POST['rs_sender_option']),
                'earningpoints' => stripslashes($_POST['earningpoints']),
                'redeemingpoints' => stripslashes($_POST['redeemingpoints']),
                'mailsendingoptions' => stripslashes($_POST['mailsendingoptions']),
                'rsmailsendingoptions' => stripslashes($_POST['rsmailsendingoptions']),
                'minimum_userpoints' => stripslashes($_POST['rs_minimum_userpoints']),
                'from_name' => stripslashes($_POST['rs_from_name']),
                'from_email' => stripslashes($_POST['rs_from_email']),
                'subject' => stripslashes($_POST['rs_subject']),
                'sendmail_options' => stripslashes($_POST['rs_sendmail_options']),
                'sendmail_to' => stripslashes($_POST['rs_sendmail_selected']),
                'message' => stripslashes($_POST['rs_message']),
            ));
        }
        echo $wpdb->insert_id;
        exit();
    }

    public static function edit_template() {
        if (isset($_POST['rs_template_id'])) {
            $template_id = $_POST['rs_template_id'];
            global $wpdb;
            $table_name_email = $wpdb->prefix . 'rs_templates_email';

            if ($_POST['mailsendingoptions'] == '1') {
                if ($_POST['rsmailsendingoptions'] == '1') {
                    //For earning
                    if ($_POST['earningpoints'] != $_POST['hiddenearningpoints']) {
                        delete_option('rsearningtemplates' . $template_id);
                    }
                }
                if ($_POST['rsmailsendingoptions'] == '2') {
                    if ($_POST['redeemingpoints'] != $_POST['hiddenredeemingpoints']) {
                        delete_option('rsredeemingtemplates' . $template_id);
                    }
                }
            }

            $wpdb->update($table_name_email, array('template_name' => stripslashes($_POST['rs_template_name']),
                'sender_opt' => stripslashes($_POST['rs_sender_option']),
                'from_name' => stripslashes($_POST['rs_from_name']),
                'from_email' => stripslashes($_POST['rs_from_email']),
                'earningpoints' => stripslashes($_POST['earningpoints']),
                'redeemingpoints' => stripslashes($_POST['redeemingpoints']),
                'mailsendingoptions' => stripslashes($_POST['mailsendingoptions']),
                'rsmailsendingoptions' => stripslashes($_POST['rsmailsendingoptions']),
                'minimum_userpoints' => stripslashes($_POST['rs_minimum_userpoints']),
                'sendmail_options' => stripslashes($_POST['rs_sendmail_options']),
                'sendmail_to' => serialize($_POST['rs_sendmail_selected']),
                'subject' => stripslashes($_POST['rs_subject']),
                'message' => stripslashes($_POST['rs_message']),
                    ), array('id' => $template_id));
        }
        echo "1";
        exit();
    }

    public static function delete_template() {
        if (isset($_POST['row_id'])) {
            global $wpdb;
            $row_id = $_POST['row_id'];
            $table_name_email = $wpdb->prefix . 'rs_templates_email';
            $wpdb->delete($table_name_email, array('id' => $row_id));
        }
        exit();
    }

}

new FPRewardSystemStatusTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemEmailTemplatesTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_emailtemplates', array('FPRewardSystemEmailTemplatesTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemEmailTemplatesTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_emailtemplates', array('FPRewardSystemEmailTemplatesTab', 'reward_system_register_admin_settings'));


add_action('wp_ajax_rs_new_template', array('FPRewardSystemEmailTemplatesTab', 'create_template'));
add_action('wp_ajax_nopriv_rs_new_template', array('FPRewardSystemEmailTemplatesTab', 'create_template'));
add_action('wp_ajax_rs_edit_template', array('FPRewardSystemEmailTemplatesTab', 'edit_template'));
add_action('wp_ajax_nopriv_rs_edit_template', array('FPRewardSystemEmailTemplatesTab', 'edit_template'));
add_action('wp_ajax_rs_delete_email_template', array('FPRewardSystemEmailTemplatesTab', 'delete_template'));
add_action('wp_ajax_nopriv_rs_delete_email_template', array('FPRewardSystemEmailTemplatesTab', 'delete_template'));

function add_sumo_rewards_table_list_email_templates() {
    ?>
    <p>Email Template Settings</p>

    <style type="text/css">
        p.submit {
            display:none;
        }
        #mainforms {
            display:none;
        }
        .chosen-container .chosen-results {
            clear: both;
        }
        .chosen-container {
            position:absolute !important;
        }
    </style>

    <?php if (isset($_GET['page']) == 'rewardsystem_callback') { ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#rs_multiselect_mail_send').chosen();
            });
        </script>
    <?php } ?>
    <script type='text/javascript'>
        jQuery(document).ready(function() {
            //jQuery('.rs_email_template_table').footable();

            jQuery('#rs_pagination').change(function(e) {
                e.preventDefault();
                var pageSize = jQuery(this).val();
                jQuery('.rs_email_template_table').data('page-size', pageSize);
                jQuery('.rs_email_template_table').trigger('footable_initialized');
            });



            jQuery('#rs_email_templates_table').footable().on('click', '.rs_delete', function(e) {
                e.preventDefault();
                var row_id = jQuery(this).data('id');
                console.log(row_id);
                var footable = jQuery('#rs_email_templates_table').data('footable');
                var row = jQuery(this).parents('tr:first');
                footable.removeRow(row);
                var data = {
                    row_id: row_id,
                    action: "rs_delete_email_template"
                }
                jQuery.ajax({type: "POST",
                    url: ajaxurl,
                    data: data}).done(function(res) {

                });
            });
        });
    </script>

    <?php
    //  var_dump(get_users()) ;
//delete_user_meta('1', '_my_points_log');
//delete_user_meta('1', '_my_reward_points');

    global $wpdb;
    $table_name = $wpdb->prefix . 'rs_templates_email';
    $templates = $wpdb->get_results("SELECT * FROM $table_name", OBJECT);

    if (isset($_GET['rs_new_email'])) {
        $editor_id = "rs_email_template_new";
        //$content = get_option('rs_email_template');
        $settings = array('textarea_name' => 'rs_email_template_new');
        $admin_url = admin_url('admin.php');
        $template_list_url = add_query_arg(array('page' => 'rewardsystem_callback', 'tab' => 'rewardsystem_emailtemplates'), $admin_url);
        $content = "Hi {rsfirstname} {rslastname}, <br><br> You have Earned Reward Points: {rspoints} on {rssitelink}  <br><br> You can use this Reward Points to make discounted purchases on {rssitelink} <br><br> Thanks";
        ?>

        <table class="widefat"><tr><td>

            <tr><td><span><strong>Use {rssitelink} to insert the Cart Link in the mail</strong></span></td></tr>
            <tr><td><span><strong>Use {rsfirstname} to insert Reciever First Name in the mail</strong></span></td></tr>
            <tr><td><span><strong>Use {rslastname} to insert Reciever Last Name in the mail</strong></span></td></tr>
            <tr><td><span><strong>{rspoints} to insert User Points in the Mail</strong></span></td></tr>
            <tr><td><?php _e('Template Name', 'rewardsystem') ?>: </td><td><input type="text" name="rs_template_name" id="rs_template_name"></td></tr>
            <tr><td><?php _e('Send Mail', 'rewardsystem'); ?>:</td><td><input type="radio" name="mailsendingoptions" id="mailsendingoptions1" value="1"/>Only Once <br>
                    <input type="radio" name="mailsendingoptions" id="mailsendingoptions2" class="mailsendingoptions" value="2"/>Always<br>
                </td>
            </tr>
            <tr><td><?php _e('Send Mail Type', 'rewardsystem'); ?>:</td><td><input type="radio" name="rsmailsendingoptions" id="rsmailsendingoptions1" class="rsmailsendingoptions" value="1">Based on Earning Point<br>
                    <input type="radio" name="rsmailsendingoptions" id="rsmailsendingoptions2" class="rsmailsendingoptions" value="2">Based on Redeeming Point<br>
                    <input type="radio" name="rsmailsendingoptions" id="rsmailsendingoptions3" class="rsmailsendingoptions" value="3">Based on Cron Job<br>
                </td></tr>
            <tr><td><?php _e('Send Mail on Earning Points of', 'rewardsystem'); ?></td>
                <td><input type="text" name="earningpoints" class="earningpoints" id="earningpoints" value=""/></td>
            </tr>
            <tr><td><?php _e('Send Mail on Redeeming Points of', 'rewardsystem'); ?></td><td>
                    <input type="text" name="redeemingpoints" class="redeemingpoints" id="redeemingpoints" value=""/>
                </td></tr>
            <tr><td><?php _e('Email Sender Option', 'rewardsystem') ?>: </td><td><input type="radio" name="rs_sender_opt" id="rs_sender_woo" value="woo" class="rs_sender_opt">woocommerce <input type="radio" name="rs_sender_opt" id="rs_sender_local" value="local" class="rs_sender_opt">local</td></tr>
            <tr class="rs_local_senders"><td><?php _e('From Name', 'rewardsystem') ?>: </td><td><input type="text" name="rs_from_name"  id="rs_from_name"></td></tr>
            <tr class="rs_local_senders"><td><?php _e('From Email', 'rewardsystem') ?>: </td><td><input type="text" name="rs_from_email"  id="rs_from_email"></td></tr>
            <tr class="rs_minimum_userpoints_field"><td><?php _e('Minimum User Points to send this Mail', 'rewardsystem'); ?>: </td><td><input type="text" class="rs_minimum_userpoints" name="rs_minimum_userpoints" id="rs_minimum_userpoints" value=""/></td></tr>
            <tr class="rs_sendmail_options"><td><?php _e('Send Mail To', 'rewardsystem'); ?>:</td><td><input type="radio" name="rs_sendmail_options" value="1" class="rs_sendmail_options">All Users <input type="radio" name="rs_sendmail_options" id="rs_sendmail_options" value="2" class="rs_sendmail_options"/>Selected Users</td></tr>
            <tr class="rs_sendmail_to"><td><?php _e('Send Mail to Selected Users', 'rewardsystem'); ?></td><td><select style="width:550px;" id="rs_multiselect_mail_send" name="rs_multiselect_mail_send" multiple="multiple">
                        <?php
                        foreach (get_users() as $eachuser) {
                            ?>
                            <option value="<?php echo $eachuser->ID; ?>"><?php echo $eachuser->nickname; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td></tr>
            <tr><td><?php _e('Subject', 'rewardsystem') ?>:</td><td> <input type="text" name="rs_subject" id="rs_subject"></td></tr>
            <tr><td> <?php _e('Message', 'rewardsystem') ?>:</td>
                <td>
                    <?php
                    wp_editor($content, $editor_id, $settings);
                    ?>
                </td></tr>
            <tr><td>
                    <input type="button" name="rs_save_new_template" class="button button-primary button-large" id="rs_save_new_template" value="Save">&nbsp;

                    <a href="<?php echo $template_list_url ?>"><input type="button" class="button" name="returntolist" value="Return to Mail Templates"></a>&nbsp;
                </td></tr>
        </table>
        <script>jQuery(document).ready(function() {
                jQuery("#rs_template_name").val("Default");
                jQuery("#rs_from_name").val("Admin");
                jQuery('#rs_minimum_userpoints').val("0");
                jQuery("#rs_sender_woo").attr("checked", "checked");
                jQuery(".rs_sender_opt").change(function() {
                    if (jQuery("#rs_sender_woo").is(":checked")) {
                        jQuery(".rs_local_senders").css("display", "none");
                    } else {
                        jQuery(".rs_local_senders").css("display", "table-row");
                    }
                });

                jQuery('#mailsendingoptions2').attr('checked', 'checked');
                jQuery('#rsmailsendingoptions3').attr('checked', 'checked');
                var mailsendoptions = jQuery('.rsmailsendingoptions').filter(':checked').val();
                if (mailsendoptions === '3') {
                    jQuery('#earningpoints').parent().parent().hide();
                    jQuery('#redeemingpoints').parent().parent().hide();
                } else if (mailsendoptions === '2') {
                    jQuery('#earningpoints').parent().parent().hide();
                    jQuery('#redeemingpoints').parent().parent().show();
                } else {
                    jQuery('#earningpoints').parent().parent().show();
                    jQuery('#redeemingpoints').parent().parent().hide();
                }

                jQuery('.rsmailsendingoptions').change(function() {
                    if (jQuery(this).val() === '3') {
                        jQuery('#earningpoints').parent().parent().hide();
                        jQuery('#redeemingpoints').parent().parent().hide();
                    } else if (jQuery(this).val() === '2') {
                        jQuery('#earningpoints').parent().parent().hide();
                        jQuery('#redeemingpoints').parent().parent().show();
                    } else {
                        jQuery('#earningpoints').parent().parent().show();
                        jQuery('#redeemingpoints').parent().parent().hide();
                    }
                });

                jQuery("#rs_subject").val("SUMO Reward Points");
                jQuery("#rs_from_email").val("<?php echo get_option('admin_email') ?>");
                jQuery("#rs_duration_type").val("days");
                jQuery("#rs_duration").val("1");

                jQuery("#rs_email_template_new").val("Hi {rsfirstname} {rslastname}, <br><br> You have Earned Reward Points: {rspoints} on {rssitelink}  <br><br> You can use this Reward Points to make discounted purchases on {rssitelink} <br><br> Thanks");
                jQuery("#rs_duration_type").change(function() {
                    jQuery("span#rs_duration").html(jQuery("#rs_duration_type").val());
                });
                jQuery("#rs_save_new_template").click(function() {

                    var multivalue = jQuery('#rs_multiselect_mail_send').val();

                    jQuery(this).prop("disabled", true);
                    var rs_template_name = jQuery("#rs_template_name").val();
                    var sendmail = jQuery('input:radio[name=mailsendingoptions]:checked').val();
                    var sendmailtypes = jQuery('input:radio[name=rsmailsendingoptions]:checked').val();
                    var earningpoints = jQuery('#earningpoints').val();
                    var redeemingpoints = jQuery('#redeemingpoints').val();
                    var rs_sender_option = jQuery("input:radio[name=rs_sender_opt]:checked").val();
                    var rs_sendmail_options = jQuery("input:radio[name=rs_sendmail_options]:checked").val();
                    var rs_sendmail_selected = multivalue;
                    var rs_minimum_userpoints = jQuery('#rs_minimum_userpoints').val();
                    var rs_from_name = jQuery("#rs_from_name").val();
                    var rs_from_email = jQuery("#rs_from_email").val();
                    var rs_subject = jQuery("#rs_subject").val();
                    var rs_message = tinymce.get("rs_email_template_new").getContent();
                    var rs_duration_type = jQuery("#rs_duration_type").val();
                    var rs_mail_duration = jQuery("span #rs_duration").val();
                    console.log(jQuery("#rs_email_template_new").val());

                    var data = {
                        action: "rs_new_template",
                        rs_sender_option: rs_sender_option,
                        rs_template_name: rs_template_name,
                        mailsendingoptions: sendmail,
                        rsmailsendingoptions: sendmailtypes,
                        earningpoints: earningpoints,
                        redeemingpoints: redeemingpoints,
                        rs_minimum_userpoints: rs_minimum_userpoints,
                        rs_from_name: rs_from_name,
                        rs_from_email: rs_from_email,
                        rs_sendmail_options: rs_sendmail_options,
                        rs_sendmail_selected: rs_sendmail_selected,
                        rs_subject: rs_subject,
                        rs_message: rs_message,
                        rs_duration_type: rs_duration_type,
                        rs_mail_duration: rs_mail_duration
                    };

                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: data
                    }).done(function(response) {

                        alert("Settings Saved");
                        jQuery("#rs_save_new_template").prop("disabled", false);
                    });
                    console.log(data);
                });
            });</script>
        <style>
            .rs_local_senders{
                display:none;
            }
        </style>
        <?php
    } else if (isset($_GET['rs_edit_email'])) {
        $template_id = $_GET['rs_edit_email'];
        $edit_templates = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$template_id", OBJECT);
        $edit_templates = $edit_templates[0];
        $admin_url = admin_url('admin.php');
        $template_list_url = add_query_arg(array('page' => 'rewardsystem_callback', 'tab' => 'rewardsystem_emailtemplates'), $admin_url);
        $editor_id = "rs_email_template_edit";
        $content = $edit_templates->message;
        $rs_mailsend_options = $edit_templates->sendmail_options;
        $rs_mailsend_selected_users = unserialize($edit_templates->sendmail_to);
        if (!empty($rs_mailsend_selected_users)) {
            $rs_mailsend_implode = implode(',', $rs_mailsend_selected_users);
        }
        $settings = array('textarea_name' => 'rs_email_template_edit');
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                var data = "<?php echo $rs_mailsend_implode; ?>";
                var values = "<?php echo $rs_mailsend_implode; ?>";
                var splitted_data = values.split(',');
                // alert(splitted_data[0]);
                jQuery('#rs_multiselect_mail_send ').val(splitted_data);
                jQuery.each(splitted_data, function(i, e) {
                    jQuery("select#rs_multiselect_mail_send option[value='" + e + "']").attr("selected", true);
                    jQuery('#rs_multiselect_mail_send').trigger("chosen:updated");
                });
                jQuery("#rs_multiselect_mail_send option").each(function() {
                });
            });
        </script>
        <table class="widefat"><tr><td>
            <tr><td><span><strong>Use {rssitelink} to insert the Cart Link in the mail</strong></span></td></tr>
            <tr><td><span><strong>Use {rsfirstname} to insert Reciever First Name in the mail</strong></span></td></tr>
            <tr><td><span><strong>Use {rslastname} to insert Reciever Last Name in the mail</strong></span></td></tr>
            <tr><td><span><strong>{rspoints} to insert User Points in the Mail</strong></span></td></tr>
            <tr>
                <td> <?php _e('Template Name', 'rewardsystem') ?>:</td>
                <td><input type="text" name="rs_template_name" id="rs_template_name" value="<?php echo $edit_templates->template_name ?>"></td></tr>
            <?php
            $woo_selected = checked($edit_templates->sender_opt, 'woo', false);
            $local_selected = checked($edit_templates->sender_opt, 'local', false);

            $sendmail_option_selected = checked($edit_templates->sendmail_options, '1', false);
            $sendmail_particular_selected = checked($edit_templates->sendmail_options, '2', false);

            $sendmailonce = checked($edit_templates->mailsendingoptions, '1', false);
            $sendmailalways = checked($edit_templates->mailsendingoptions, '2', false);

            $sendmailtypebyearning = checked($edit_templates->rsmailsendingoptions, '1', false);
            $sendmailtypebyredeeming = checked($edit_templates->rsmailsendingoptions, '2', false);
            $sendmailtypebycronjob = checked($edit_templates->rsmailsendingoptions, '3', false);
            ?>

            <tr><td><?php _e('Send Mail', 'rewardsystem'); ?>:</td><td><input type="radio" name="mailsendingoptions" id="mailsendingoptions" value="1" <?php echo $sendmailonce; ?>/>Only Once <br>
                    <input type="radio" name="mailsendingoptions" id="mailsendingoptions" class="mailsendingoptions" value="2" <?php echo $sendmailalways; ?>/>Always<br>
                </td>
            </tr>
            <tr><td><?php _e('Send Mail Type', 'rewardsystem'); ?>:</td><td><input type="radio" name="rsmailsendingoptions" id="rsmailsendingoptions" class="rsmailsendingoptions" value="1" <?php echo $sendmailtypebyearning; ?>>Based on Earning Point<br>
                    <input type="radio" name="rsmailsendingoptions" id="rsmailsendingoptions" class="rsmailsendingoptions" value="2" <?php echo $sendmailtypebyredeeming; ?>>Based on Redeeming Point<br>
                    <input type="radio" name="rsmailsendingoptions" id="rsmailsendingoptions" class="rsmailsendingoptions" value="3" <?php echo $sendmailtypebycronjob; ?>>Based on Cron Job<br>
                </td></tr>
            <tr><td><?php _e('Send Mail on Earning Points of', 'rewardsystem'); ?></td>
                <td><input type="text" name="earningpoints" class="earningpoints" id="earningpoints" value="<?php echo $edit_templates->earningpoints; ?>"/></td>
            </tr>
            <tr><td><?php _e('Send Mail on Redeeming Points of', 'rewardsystem'); ?></td><td>
                    <input type="text" name="redeemingpoints" class="redeemingpoints" id="redeemingpoints" value="<?php echo $edit_templates->redeemingpoints; ?>"/>
                </td></tr>


            <tr><td><?php _e('Email Sender Option', 'rewardsystem') ?>: </td><td><input type="radio" name="rs_sender_opt" id="rs_sender_woo" value="woo" <?php echo $woo_selected ?> class="rs_sender_opt">woocommerce
                    <input type="radio" name="rs_sender_opt" id="rs_sender_local" value="local" <?php echo $local_selected ?> class="rs_sender_opt">local</td></tr>
            <tr class="rs_local_senders"><td><?php _e('From Name', 'rewardsystem') ?>:</td>
                <td><input type="text" name="rs_from_name" id="rs_from_name" value="<?php echo $edit_templates->from_name ?>"></td></tr>
            <tr class="rs_local_senders"><td><?php _e('From Email', 'rewardsystem') ?>:</td>
                <td><input type="text" name="rs_from_email" id="rs_from_email" value="<?php echo $edit_templates->from_email ?>"></td></tr>
            <tr class="rs_minimum_userpoints_field">
                <td><?php _e('Minimum User Points to send this Mail', 'rewardsystem'); ?></td> <td><input type="text" class="rs_minimum_userpoints" name="rs_minimum_userpoints" id="rs_minimum_edit_userpoints" value="<?php echo $edit_templates->minimum_userpoints; ?>"/></td>
            </tr>
            <tr class="rs_sendmail_options"><td><?php _e('Send Mail To', 'rewardsystem'); ?>:</td><td><input type="radio" name="rs_sendmail_options" value="1" <?php echo $sendmail_option_selected ?> class="rs_sendmail_options">All Users <input type="radio" name="rs_sendmail_options" id="rs_sendmail_options" value="2" <?php echo $sendmail_particular_selected; ?> class="rs_sendmail_options"/>Selected Users</td></tr>
            <tr class="rs_sendmail_to"><td><?php _e('Send Mail to Selected Users', 'rewardsystem'); ?></td><td><select id="rs_multiselect_mail_send" style="width:550px;" name="rs_multiselect_mail_send" multiple="multiple">
                        <?php
                        foreach (get_users() as $eachuser) {
                            ?>
                            <option value="<?php echo $eachuser->ID; ?>" <?php selected($edit_templates->sendmail_to); ?>><?php echo $eachuser->nickname; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td></tr>

            <tr><td><?php _e('Subject', 'rewardsystem') ?>:</td>
                <td><input type="text" name="rs_subject" id="rs_subject" value="<?php echo $edit_templates->subject ?>"></td></tr>
            <tr><td> <?php _e('Message', 'rewardsystem') ?>:</td>
                <td>
                    <?php
                    wp_editor($content, $editor_id, $settings);
                    ?>
                </td></tr>
            <tr><td>
                    <input type="hidden" name="rsearningpointschanges" value="<?php echo $edit_templates->earningpoints ?>" id="rschangesearningpoint"/><br>
                    <input type="hidden" name="rsredeemingpointchanges" value="<?php echo $edit_templates->redeemingpoints; ?>" id="rschangesredeemingpoint"/><br>
                    <input type="button" class="button button-primary button-large" name="rs_save_new_template" id="rs_save_new_template" value="<?php _e('Save Changes', 'rewardsystem') ?>">&nbsp;
                    <a href="<?php echo $template_list_url ?>"><input type="button" class="button" name="returntolist" value="<?php _e('Return to Mail Templates', 'rewardsystem') ?>"></a>&nbsp;
                </td></tr>
        </table>
        <script>jQuery(document).ready(function() {


                jQuery("#rs_duration_type").change(function() {
                    jQuery("span#rs_duration").html(jQuery("#rs_duration_type").val());
                });


                //normal ready event
                if (jQuery("#rs_sender_woo").is(":checked")) {
                    jQuery(".rs_local_senders").css("display", "none");
                } else {
                    jQuery(".rs_local_senders").css("display", "table-row");
                }

                jQuery(".rs_sender_opt").change(function() {
                    if (jQuery("#rs_sender_woo").is(":checked")) {
                        jQuery(".rs_local_senders").css("display", "none");
                    } else {
                        jQuery(".rs_local_senders").css("display", "table-row");
                    }
                });


                var mailsendoptions = jQuery('.rsmailsendingoptions').filter(':checked').val();
                if (mailsendoptions === '3') {
                    jQuery('#earningpoints').parent().parent().hide();
                    jQuery('#redeemingpoints').parent().parent().hide();
                } else if (mailsendoptions === '2') {
                    jQuery('#earningpoints').parent().parent().hide();
                    jQuery('#redeemingpoints').parent().parent().show();
                } else {
                    jQuery('#earningpoints').parent().parent().show();
                    jQuery('#redeemingpoints').parent().parent().hide();
                }

                jQuery('.rsmailsendingoptions').change(function() {
                    if (jQuery(this).val() === '3') {
                        jQuery('#earningpoints').parent().parent().hide();
                        jQuery('#redeemingpoints').parent().parent().hide();
                    } else if (jQuery(this).val() === '2') {
                        jQuery('#earningpoints').parent().parent().hide();
                        jQuery('#redeemingpoints').parent().parent().show();
                    } else {
                        jQuery('#earningpoints').parent().parent().show();
                        jQuery('#redeemingpoints').parent().parent().hide();
                    }
                });

                jQuery("#rs_save_new_template").click(function() {

                    var multivalue = jQuery('#rs_multiselect_mail_send').val();
                    jQuery(this).prop("disabled", true);
                    var rs_template_name = jQuery("#rs_template_name").val();

                    var sendmail = jQuery('input:radio[name=mailsendingoptions]:checked').val();
                    var sendmailtypes = jQuery('input:radio[name=rsmailsendingoptions]:checked').val();
                    var earningpoints = jQuery('#earningpoints').val();

                    var hiddenearningpoints = jQuery('#rschangesearningpoint').val();
                    var hiddenredeemingpoints = jQuery('#rschangesredeemingpoint').val();
                    var redeemingpoints = jQuery('#redeemingpoints').val();
                    var rs_sender_option = jQuery("input:radio[name=rs_sender_opt]:checked").val();
                    var rs_from_name = jQuery("#rs_from_name").val();
                    var rs_from_email = jQuery("#rs_from_email").val();
                    var rs_minimum_userpoints = jQuery('#rs_minimum_edit_userpoints').val();
                    var rs_sendmail_options = jQuery("input:radio[name=rs_sendmail_options]:checked").val();
                    var rs_sendmail_selected = multivalue;
                    var rs_subject = jQuery("#rs_subject").val();
                    var rs_message = tinymce.get("rs_email_template_edit").getContent();
                    var rs_duration_type = jQuery("#rs_duration_type").val();
                    var rs_mail_duration = jQuery("span #rs_duration").val();
                    var rs_template_id = '<?php echo $template_id ?>';
                    console.log(jQuery("#rs_email_template_edit").val());


                    var data = {
                        action: "rs_edit_template",
                        rs_sender_option: rs_sender_option,
                        rs_template_name: rs_template_name,
                        mailsendingoptions: sendmail,
                        rsmailsendingoptions: sendmailtypes,
                        earningpoints: earningpoints,
                        hiddenearningpoints: hiddenearningpoints,
                        hiddenredeemingpoints: hiddenredeemingpoints,
                        redeemingpoints: redeemingpoints,
                        rs_minimum_userpoints: rs_minimum_userpoints,
                        rs_from_name: rs_from_name,
                        rs_from_email: rs_from_email,
                        rs_sendmail_options: rs_sendmail_options,
                        rs_sendmail_selected: rs_sendmail_selected,
                        rs_subject: rs_subject,
                        rs_message: rs_message,
                        rs_duration_type: rs_duration_type,
                        rs_mail_duration: rs_mail_duration,
                        rs_template_id: rs_template_id
                    };

                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: data
                    }).done(function(response) {
                        var newresponse = response.replace(/\s/g, '');
                        if (newresponse === '1') {
                            alert("Settings Updated");
                        }
                        jQuery("#rs_save_new_template").prop("disabled", false);
                    });
                    console.log(data);
                });
            });</script>
        <?php
    } else {
        $admin_url = admin_url('admin.php');
        $new_template_url = add_query_arg(array('page' => 'rewardsystem_callback', 'tab' => 'rewardsystem_emailtemplates', 'rs_new_email' => 'template'), $admin_url);

        $edit_template_url = add_query_arg(array('page' => 'rewardsystem_callback', 'tab' => 'rewardsystem_emailtemplates', 'rs_edit_email' => 'template'), $admin_url);
        ?>


        <a href='<?php echo $new_template_url ?>'>
            <input type="button" name="rs_new_email_template" id="rs_new_email_template" class="button" value="New Template">
        </a>
        <?php
        echo '<p> ' . __('Search:', 'rewardsystem') . '<input id="rs_email_templates" type="text"/>  ' . __('Page Size:', 'rewardsystem') . '
                <select id="changepagesizertemplates">
                <option value="1">1</option>
		<option value="5">5</option>
		<option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>

                </p>';
        ?>
        <table class="wp-list-table widefat fixed posts" data-filter = "#rs_email_templates" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" id="rs_email_templates_table" cellspacing="0">
            <thead>
                <tr>
                    <th scope='col' data-toggle="true" class='manage-column column-serial_number'  style="">
                        <a href="#"><span><?php _e('S.No', 'rewardsystem'); ?></span>
                    </th>
                    <th scope='col' id='rs_user_names' class='manage-column column-rs_user_name'  style=""><?php _e('Template Name', 'rewardsystem'); ?></th>
                    <th scope='col' id='rs_from_name' class='manage-column column-rs_from_name'  style=""><?php _e('From Name', 'rewardsystem'); ?></th>
                    <th scope='col' id='rs_from_email' class='manage-column column-rs_from_email'  style=""><?php _e('From Email', 'rewardsystem'); ?></th>
                    <th scope="col" id="rs_subject" class="manage-column column-rs_subject" style=""><?php _e('Subject', 'rewardsystem'); ?></th>
                    <th scope='col' id='rs_message' class='manage-column column-rs_message' style=''><?php _e('Message', 'rewardsystem'); ?></th>
                    <th scope="col" id="rs_minimum_userpoints" class="manage-column column-rs_minimum_userpoints" style=""><?php _e('Minimum User Points', 'rewardsystem'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($templates as $each_template) {
                    echo '<tr><td>';
                    echo $each_template->id;
                    $edit_template_url = add_query_arg(array('page' => 'rewardsystem_callback', 'tab' => 'rewardsystem_emailtemplates', 'rs_edit_email' => $each_template->id), $admin_url);
                    echo '&nbsp;<span><a href="' . $edit_template_url . '">' . __('Edit', 'rewardsystem') . ' </a></span>&nbsp; <span><a href="" class="rs_delete" data-id="' . $each_template->id . '">' . __('Delete', 'rewardsystem') . '</a></span>';
                    echo '</td><td>';
                    echo $each_template->template_name;
                    echo '</td><td>';
                    if ("local" == $each_template->sender_opt) {
                        echo $each_template->from_name;
                        echo '</td><td>';
                        echo $each_template->from_email;
                    } else {
                        echo get_option('woocommerce_email_from_name');
                        echo '</td><td>';
                        echo get_option('woocommerce_email_from_address');
                    }
                    echo '</td><td>';
                    echo $each_template->subject;
                    echo '</td><td>';
                    $message = strip_tags($each_template->message);
                    if (strlen($message) > 80) {
                        echo substr($message, 0, 80);
                        echo '...';
                    } else {
                        echo $message;
                    }
                    echo '</td><td>';
                    echo $each_template->minimum_userpoints;
                    echo '</td></tr>';
                }
                ?>
            </tbody>
        </table>
        <div style="clear:both;">
            <div class="pagination pagination-centered"></div>
        </div>
        <?php
    }
}

add_action('woocommerce_admin_field_list_table', 'add_sumo_rewards_table_list_email_templates');
?>
