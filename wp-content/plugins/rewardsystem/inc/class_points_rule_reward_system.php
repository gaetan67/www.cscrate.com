<?php

class FPRewardSystemPointsRule {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_pointsrule'] = __('Reward Points for Action', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;



        return apply_filters('woocommerce_rewardsystem_poinstrule_settings', array(
            array(
                'name' => __('', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Reward your customers for the following actions', 'rewardsystem'),
                'id' => '_rs_reward_point_action'
            ),
            array(
                'name' => __('Reward Points for Account Signup', 'rewardsystem'),
                'desc' => __('Please Enter the Reward Points that will be earned for Account Signup', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reward_signup',
                'css' => 'min-width:150px;',
                'std' => '1000',
                'type' => 'text',
                'newids' => 'rs_reward_signup',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward Points for Product Review', 'rewardsystem'),
                'desc' => __('Please Enter the Reward Points that will be earned for Reviewing a Product', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reward_product_review',
                'css' => 'min-width:150px;',
                'std' => '200',
                'type' => 'text',
                'newids' => 'rs_reward_product_review',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Restrict Reward Points for One Review per Product per Member', 'rewardsystem'),
                'desc' => __('Restrict the Reward Points for Review a Product per User', 'rewardsystem'),
                'id' => 'rs_restrict_reward_product_review',
                'css' => 'min-width:150px;',
                'type' => 'checkbox',
                'newids' => 'rs_restrict_reward_product_review',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_action'),
            array(
                'name' => __('', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Referral Reward for the following actions', 'rewardsystem'),
                'id' => '_rs_reward_point_for_referral_action'
            ),
            array(
                'name' => __('Referral Reward Points for Account Signup', 'rewardsystem'),
                'desc' => __('Please Enter the Referral Reward Points that will be earned for Account Signup', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_referral_reward_signup',
                'css' => 'min-width:150px;',
                'std' => '1000',
                'type' => 'text',
                'newids' => 'rs_referral_reward_signup',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_for_referral_action'),
            array(
                'name' => __('Payment Gateway Reward Points', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_reward_point_for_payment_gateway',
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_for_payment_gateway'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemPointsRule::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemPointsRule::rewardsystem_admin_fields());
    }

    public static function reward_system_add_settings_to_action($settings) {
        $updated_settings = array();
        $mainvariable = array();
        global $woocommerce;
        foreach ($settings as $section) {
            if (isset($section['id']) && '_rs_reward_point_for_payment_gateway' == $section['id'] &&
                    isset($section['type']) && 'sectionend' == $section['type']) {
                if(function_exists('WC')){
                foreach (WC()->payment_gateways->payment_gateways() as $gateway) {
                    $updated_settings[] = array(
                        'name' => __('Reward Points for Using ' . $gateway->title, 'rewardsystem'),
                        'desc' => __('Please Enter Reward Points for ' . $gateway->title, 'rewardsystem'),
                        'tip' => '',
                        'id' => 'rs_reward_payment_gateways_' . $gateway->id,
                        'css' => 'min-width:150px;',
                        'std' => '',
                        'type' => 'text',
                        'newids' => 'rs_reward_payment_gateways_' . $gateway->id,
                        'desc_tip' => true,
                    );
                    }
                }else {
                    if(class_exists('WC_Payment_Gateways')){
                    $paymentgateway = new WC_Payment_Gateways();
                    foreach($paymentgateway->payment_gateways()as $gateway) {
                             $updated_settings[] = array(
                        'name' => __('Reward Points for Using ' . $gateway->title, 'rewardsystem'),
                        'desc' => __('Please Enter Reward Points for ' . $gateway->title, 'rewardsystem'),
                        'tip' => '',
                        'id' => 'rs_reward_payment_gateways_' . $gateway->id,
                        'css' => 'min-width:150px;',
                        'std' => '',
                        'type' => 'text',
                        'newids' => 'rs_reward_payment_gateways_' . $gateway->id,
                        'desc_tip' => true,
                        );
                    }
                }
                }
                $updated_settings[] = array(
                    'type' => 'sectionend', 'id' => '_rs_reward_system_payment_gateway',
                );
            }


            $newsettings = array('type' => 'sectionend', 'id' => '_rs_reward_system_pg_end');
            $updated_settings[] = $section;
        }

        return $updated_settings;
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        $setting = array();
        foreach (FPRewardSystemPointsRule::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && isset($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemPointsRule();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemPointsRule', 'reward_system_tab_settings'), 101);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_pointsrule', array('FPRewardSystemPointsRule', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemPointsRule', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_pointsrule', array('FPRewardSystemPointsRule', 'reward_system_register_admin_settings'));


add_filter('woocommerce_rewardsystem_poinstrule_settings', array('FPRewardSystemPointsRule', 'reward_system_add_settings_to_action'));
