<?php

if (get_option('timezone_string') != '') {
    date_default_timezone_set(get_option('timezone_string'));
} else {
    date_default_timezone_set('UTC');
}
$pointslog = array();



/* Payment Rewards for WooCommerce Order */
$getpaymentgatewayused = get_option('rs_reward_payment_gateways_' . $order->payment_method);

if ($getpaymentgatewayused != '') {
    $checkmypoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);
    $totalpoints_payment = $checkmypoints_payment + $getpaymentgatewayused;
    update_user_meta($order->user_id, '_my_reward_points', $totalpoints_payment);
    $mycurrentpoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);

    $localizemessage = str_replace('{payment_title}', $order->payment_method_title, get_option('_rs_localize_reward_for_payment_gateway_message'));

    $pointslogs_payment[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getpaymentgatewayused, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints_payment, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $localizemessage, 'rewarder_for_frontend' => $localizemessage);
    $overalllogs_payment[] = array('userid' => $order->user_id, 'totalvalue' => $getpaymentgatewayused, 'eventname' => $localizemessage, 'date' => date('Y-m-d H:i:s'));
    $getoveralllogs_payment = get_option('rsoveralllog');
    $logmerges_payment = array_merge((array) $getoveralllogs_payment, $overalllogs_payment);
    update_option('rsoveralllog', $logmerges_payment);
    $getmypointss_payment = get_user_meta($order->user_id, '_my_points_log', true);
    $mergeds_payment = array_merge((array) $getmypointss_payment, $pointslogs_payment);
    update_user_meta($order->user_id, '_my_points_log', $mergeds_payment);
}

$usernickname = get_user_meta($order->user_id, 'nickname', true);
$checkmypoints = get_user_meta($order->user_id, '_my_reward_points', true);
delete_user_meta($order->user_id, '_redeemed_points');
delete_user_meta($order->user_id, '_redeemed_amount');
$global_referral_enable = get_option('rs_global_enable_disable_reward');
$global_referral_reward_type = get_option('rs_global_referral_reward_type');
$global_enable = get_option('rs_global_enable_disable_reward');
$global_reward_type = get_option('rs_global_reward_type');
foreach ($order->get_items() as $item) {
    if (get_option('rs_set_price_percentage_reward_points') == '1') {
        $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
    } else {
        $getregularprice = get_post_meta($item['product_id'], '_price', true);
    }
    $getpercent = get_post_meta($item['product_id'], '_rewardsystempercent', true);

    $item['qty'];
    $order->user_id;
    $item['product_id'];
    $referreduser = get_post_meta($order_id, '_referrer_name', true);
    if ($referreduser != '') {
        $refuser = get_user_by('login', $referreduser);
        $myid = $refuser->ID;
        if (get_post_meta($item['product_id'], '_rewardsystemcheckboxvalue', true) == 'yes') {
            $getreferraltype = get_post_meta($item['product_id'], '_referral_rewardsystem_options', true);
            if ($getreferraltype == '1') {
                $getreferralpoints = get_post_meta($item['product_id'], '_referralrewardsystempoints', true);
                if ($getreferralpoints == '') {
                    $term = get_the_terms($item['product_id'], 'product_cat');
                    if (is_array($term)) {
                        $rewardpointer = array();
                        foreach ($term as $term) {
                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {

                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpointer[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($global_referral_enable == '1') {
                            if ($global_referral_reward_type == '1') {
                                $rewardpointer[] = get_option('rs_global_referral_reward_point');
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                            }
                        }
                    }
                    $getreferralpoints = max($rewardpointer);
                }


                $exactpointer = $getreferralpoints * $item['qty'];

                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                $findarrays = array('{itemproductid}', '{purchasedusername}');
                $replacearrays = array($item['product_id'], get_user_meta($order->user_id, 'nickname', true));

                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);

                $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                $getoveralllogs = get_option('rsoveralllog');
                $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                update_option('rsoveralllog', $logmerges);

                $userpoints = get_user_meta($myid, '_my_reward_points', true);
                $newgetpoints = $exactpointer + $userpoints;
                if ($enabledisablemaxpoints == '1') {
                    if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                        $newgetpoints = $newgetpoints;
                    } else {
                        $newgetpoints = $restrictuserpoints;
                    }
                }
                update_user_meta($myid, '_my_reward_points', $newgetpoints);

                $pointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $exactpointer, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                $getlogger = get_user_meta($myid, '_my_points_log', true);
                $merged = array_merge((array) $getlogger, $pointsfixed);
                update_user_meta($myid, '_my_points_log', $merged);
            } else {



                $points = get_option('rs_earn_point');
                $pointsequalto = get_option('rs_earn_point_value');
                $getreferralpercent = get_post_meta($item['product_id'], '_referralrewardsystempercent', true);
                if (get_option('rs_set_price_percentage_reward_points') == '1') {
                    $getregularprices = get_post_meta($item['product_id'], '_regular_price', true);
                } else {
                    $getregularprices = get_post_meta($item['product_id'], '_price', true);
                }
                $referralpercentageproduct = $getreferralpercent / 100;
                $getreferralpricepercent = $referralpercentageproduct * $getregularprices;
                $getpointconversion = $getreferralpricepercent * $points;
                $getreferpoints = $getpointconversion / $pointsequalto;

                if ($getreferralpercent == '') {
                    $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                    if (is_array($term)) {
//var_dump($term);
                        $rewardpoints = array('0');
                        foreach ($term as $term) {

                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($global_referral_enable == '1') {
                            if ($global_referral_reward_type == '1') {
                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                            }
                        }
                    }
                    $getreferpoints = max($rewardpoints);
                }


                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                $findarrays = array('{itemproductid}', '{purchasedusername}');
                $replacearrays = array($item['product_id'], get_user_meta($order->user_id, 'nickname', true));
                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);



                $exactpointer = $getreferpoints * $item['qty'];
                $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                $getoveralllogs = get_option('rsoveralllog');
                $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                update_option('rsoveralllog', $logmerges);
                $userpoints = get_user_meta($myid, '_my_reward_points', true);
                $updatedpoints = $exactpointer + $userpoints;
                if ($enabledisablemaxpoints == '1') {
                    if (($updatedpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                        $updatedpoints = $updatedpoints;
                    } else {
                        $updatedpoints = $restrictuserpoints;
                    }
                }
                update_user_meta($myid, '_my_reward_points', $updatedpoints);

                $pointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $exactpointer, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                $getlogg = get_user_meta($myid, '_my_points_log', true);
                $mergeds = array_merge((array) $getlogg, $pointspercent);
                update_user_meta($myid, '_my_points_log', $mergeds);
            }
        }


        /* Referral Reward Points for Variable Products */
        if (get_post_meta($item['variation_id'], '_enable_reward_points', true) == '1') {
            $variablereferralrewardpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
            $variationreferralselectrule = get_post_meta($item['variation_id'], '_select_referral_reward_rule', true);
            $variationreferralrewardpercent = get_post_meta($item['variation_id'], '_referral_reward_percent', true);
            $variable_products = new WC_Product_Variation($item['variation_id']);
            if (get_option('rs_set_price_percentage_reward_points') == '1') {
                $variationregularprice = $variable_products->regular_price;
            } else {
                $variationregularprice = $variable_products->price;
            }

            if ($variationreferralselectrule == '1') {
                $getreferralpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
                $parentvariationid = new WC_Product_Variation($item['variation_id']);
                $newparentid = $parentvariationid->parent->id;
                if ($getreferralpoints == '') {
                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                    if (is_array($term)) {
//var_dump($term);
                        $rewardpoints = array('0');
                        foreach ($term as $term) {

                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $variationregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($global_referral_enable == '1') {
                            if ($global_referral_reward_type == '1') {
                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                $getaveragepoints = $getaverage * $variationregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                            }
                        }
                    }
//var_dump($rewardpoints);
                    $getreferralpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                }

                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                $findarrays = array('{itemproductid}', '{purchasedusername}');
                $replacearrays = array($item['variation_id'], get_user_meta($order->user_id, 'nickname', true));
                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);

                $getvarpoints = $getreferralpoints * $item['qty'];
                $overalllogging[] = array('userid' => $myid, 'totalvalue' => $getvarpoints, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                $getoveralllogging = get_option('rsoveralllog');
                $logmerging = array_merge((array) $getoveralllogging, $overalllogging);
                update_option('rsoveralllog', $logmerging);
                $userpoints = get_user_meta($myid, '_my_reward_points', true);
                $newgetpointing = $getvarpoints + $userpoints;
                if ($enabledisablemaxpoints == '1') {
                    if (($newgetpointing <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                        $newgetpointing = $newgetpointing;
                    } else {
                        $newgetpointing = $restrictuserpoints;
                    }
                }
                update_user_meta($myid, '_my_reward_points', $newgetpointing);
                $varpointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $getvarpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                $getlogge = get_user_meta($myid, '_my_points_log', true);
                $mergedse = array_merge((array) $getlogge, $varpointsfixed);
                update_user_meta($myid, '_my_points_log', $mergedse);
            } else {
                $pointconversion = get_option('rs_earn_point');
                $pointconversionvalue = get_option('rs_earn_point_value');
                $getvaraverage = $variationreferralrewardpercent / 100;
                $getvaraveragepoints = $getvaraverage * $variationregularprice;
                $getvarpointsvalue = $getvaraveragepoints * $pointconversion;
                $varpoints = $getvarpointsvalue / $pointconversionvalue;

                $parentvariationid = new WC_Product_Variation($item['variation_id']);
                $newparentid = $parentvariationid->parent->id;
                if ($variationreferralrewardpercent == '') {
                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                    if (is_array($term)) {
//var_dump($term);
                        $rewardpoints = array('0');
                        foreach ($term as $term) {

                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $variationregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($global_referral_enable == '1') {
                            if ($global_referral_reward_type == '1') {
                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                $getaveragepoints = $getaverage * $variationregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                            }
                        }
                    }
//var_dump($rewardpoints);
                    $varpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                }
                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                $findarrays = array('{itemproductid}', '{purchasedusername}');
                $replacearrays = array($item['variation_id'], get_user_meta($order->user_id, 'nickname', true));
                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);

                $getexactvar = $varpoints * $item['qty'];
                $overallloggered[] = array('userid' => $myid, 'totalvalue' => $getexactvar, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                $getoverallloggered = get_option('rsoveralllog');
                $logmergeder = array_merge((array) $getoverallloggered, $overallloggered);
                update_option('rsoveralllog', $logmergeder);
                $userpointser = get_user_meta($myid, '_my_reward_points', true);
                $newgetpointser = $getexactvar + $userpointser;
                if ($enabledisablemaxpoints == '1') {
                    if (($newgetpointser <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                        $newgetpointser = $newgetpointser;
                    } else {
                        $newgetpointser = $restrictuserpoints;
                    }
                }
                update_user_meta($myid, '_my_reward_points', $newgetpointser);

                $varpointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $getexactvar, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                $getlogge = get_user_meta($myid, '_my_points_log', true);
                $mergedse = array_merge((array) $getlogge, $varpointspercent);
                update_user_meta($myid, '_my_points_log', $mergedse);
            }
        }
    }
    if (get_post_meta($item['product_id'], '_rewardsystemcheckboxvalue', true) == 'yes') {

        if (get_post_meta($item['product_id'], '_rewardsystem_options', true) == '1') {
            $getpoints = get_post_meta($item['product_id'], '_rewardsystempoints', true);
            if ($getpoints == '') {
                $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                if (is_array($term)) {
//var_dump($term);
                    $rewardpoints = array('0');
                    foreach ($term as $term) {

                        $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                        $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                        if ($enablevalue == 'yes') {
                            if ($display_type == '1') {
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                }
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
                    }
                } else {
                    if ($global_enable == '1') {
                        if ($global_reward_type == '1') {
                            $rewardpoints[] = get_option('rs_global_reward_points');
                        } else {
                            $pointconversion = get_option('rs_earn_point');
                            $pointconversionvalue = get_option('rs_earn_point_value');
                            $getaverage = get_option('rs_global_reward_percent') / 100;
                            $getaveragepoints = $getaverage * $getregularprice;
                            $pointswithvalue = $getaveragepoints * $pointconversion;
                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                        }
                    }
                }
//var_dump($rewardpoints);
                $getpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
            }

            $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
            $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
            $toreplacearrayslog = array($item['product_id'], $order_id);
            $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);




            $getpointsmulti = $getpoints * $item['qty'];
            $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
            $getoveralllog = get_option('rsoveralllog');
            $logmerge = array_merge((array) $getoveralllog, $overalllog);
            update_option('rsoveralllog', $logmerge);
            $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
            $newgetpoints = $getpointsmulti + $userpoints;
            if ($enabledisablemaxpoints == '1') {
                if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                    $newgetpoints = $newgetpoints;
                } else {
                    $newgetpoints = $restrictuserpoints;
                }
            }
            update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
        } else {
            $points = get_option('rs_earn_point');
            $pointsequalto = get_option('rs_earn_point_value');
            $getpercent = get_post_meta($item['product_id'], '_rewardsystempercent', true);
            if (get_option('rs_set_price_percentage_reward_points') == '1') {
                $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
            } else {
                $getregularprice = get_post_meta($item['product_id'], '_price', true);
            }
            $percentageproduct = $getpercent / 100;
            $getpricepercent = $percentageproduct * $getregularprice;
            $getpointconvert = $getpricepercent * $points;
            $getexactpoint = $getpointconvert / $pointsequalto;

            if ($getpercent == '') {
                $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                if (is_array($term)) {
//var_dump($term);
                    $rewardpoints = array('0');
                    foreach ($term as $term) {

                        $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                        $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                        if ($enablevalue == 'yes') {
                            if ($display_type == '1') {
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                }
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
                    }
                } else {
                    if ($global_enable == '1') {
                        if ($global_reward_type == '1') {
                            $rewardpoints[] = get_option('rs_global_reward_points');
                        } else {
                            $pointconversion = get_option('rs_earn_point');
                            $pointconversionvalue = get_option('rs_earn_point_value');
                            $getaverage = get_option('rs_global_reward_percent') / 100;
                            $getaveragepoints = $getaverage * $getregularprice;
                            $pointswithvalue = $getaveragepoints * $pointconversion;
                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                        }
                    }
                }
//var_dump($rewardpoints);
                $getexactpoint = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
            }

            $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
            $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
            $toreplacearrayslog = array($item['product_id'], $order_id);
            $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);

            $getpointsmulti = $getexactpoint * $item['qty'];
            $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
            $getoveralllog = get_option('rsoveralllog');
            $logmerge = array_merge((array) $getoveralllog, $overalllog);
            update_option('rsoveralllog', $logmerge);
            $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
            $updatedpoints = $getpointsmulti + $userpoints;
            if ($enabledisablemaxpoints == '1') {
                if (($updatedpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                    $updatedpoints = $updatedpoints;
                } else {
                    $updatedpoints = $restrictuserpoints;
                }
            }
            update_user_meta($order->user_id, '_my_reward_points', $updatedpoints);
        }
    }
    if (get_post_meta($item['variation_id'], '_enable_reward_points', true) == '1') {
        $checkenablevariation = get_post_meta($item['variation_id'], '_enable_reward_points', true);
        $variablerewardpoints = get_post_meta($item['variation_id'], '_reward_points', true);
        $variationselectrule = get_post_meta($item['variation_id'], '_select_reward_rule', true);
        $variationrewardpercent = get_post_meta($item['variation_id'], '_reward_percent', true);
        $variable_product1 = new WC_Product_Variation($item['variation_id']);
#Step 4: You have the data. Have fun :)
        if (get_option('rs_set_price_percentage_reward_points') == '1') {
            $variationregularprice = $variable_product1->regular_price;
        } else {
            $variationregularprice = $variable_product1->price;
        }

        if ($variationselectrule == '1') {
            $getpoints = get_post_meta($item['variation_id'], '_reward_points', true);
            $parentvariationid = new WC_Product_Variation($item['variation_id']);
            $newparentid = $parentvariationid->parent->id;
            if ($getpoints == '') {
                $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                if (is_array($term)) {
//var_dump($term);
                    $rewardpoints = array('0');
                    foreach ($term as $term) {

                        $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                        $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                        if ($enablevalue == 'yes') {
                            if ($display_type == '1') {
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                }
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                $getaveragepoints = $getaverage * $variationregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
                    }
                } else {
                    if ($global_enable == '1') {
                        if ($global_reward_type == '1') {
                            $rewardpoints[] = get_option('rs_global_reward_points');
                        } else {
                            $pointconversion = get_option('rs_earn_point');
                            $pointconversionvalue = get_option('rs_earn_point_value');
                            $getaverage = get_option('rs_global_reward_percent') / 100;
                            $getaveragepoints = $getaverage * $variationregularprice;
                            $pointswithvalue = $getaveragepoints * $pointconversion;
                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                        }
                    }
                }
//var_dump($rewardpoints);
                $getpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
            }

            $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
            $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
            $toreplacearrayslog = array($item['variation_id'], $order_id);
            $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);


            $getpointsmulti = $getpoints * $item['qty'];
            $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
            $getoveralllog = get_option('rsoveralllog');
            $logmerge = array_merge((array) $getoveralllog, $overalllog);
            update_option('rsoveralllog', $logmerge);
            $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
            $newgetpoints = $getpointsmulti + $userpoints;
            if ($enabledisablemaxpoints == '1') {
                if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                    $newgetpoints = $newgetpoints;
                } else {
                    $newgetpoints = $restrictuserpoints;
                }
            }
            update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
        } else {
            $pointconversion = get_option('rs_earn_point');
            $pointconversionvalue = get_option('rs_earn_point_value');
            $getaverage = $variationrewardpercent / 100;
            $getaveragepoints = $getaverage * $variationregularprice;
            $getpointsvalue = $getaveragepoints * $pointconversion;
            $points = $getpointsvalue / $pointconversionvalue;

            $parentvariationid = new WC_Product_Variation($item['variation_id']);
            $newparentid = $parentvariationid->parent->id;
            if ($variationrewardpercent == '') {
                $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                if (is_array($term)) {
//var_dump($term);
                    $rewardpoints = array('0');
                    foreach ($term as $term) {

                        $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                        $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                        if ($enablevalue == 'yes') {
                            if ($display_type == '1') {
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                }
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                $getaveragepoints = $getaverage * $variationregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                    $global_enable = get_option('rs_global_enable_disable_reward');
                                    $global_reward_type = get_option('rs_global_reward_type');
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
                    }
                } else {
                    if ($global_enable == '1') {
                        if ($global_reward_type == '1') {
                            $rewardpoints[] = get_option('rs_global_reward_points');
                        } else {
                            $pointconversion = get_option('rs_earn_point');
                            $pointconversionvalue = get_option('rs_earn_point_value');
                            $getaverage = get_option('rs_global_reward_percent') / 100;
                            $getaveragepoints = $getaverage * $variationregularprice;
                            $pointswithvalue = $getaveragepoints * $pointconversion;
                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                        }
                    }
                }
//var_dump($rewardpoints);
                $points = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
            }

            $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
            $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
            $toreplacearrayslog = array($item['variation_id'], $order_id);
            $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);


            $getpointsmulti = $points * $item['qty'];
            $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
            $getoveralllog = get_option('rsoveralllog');
            $logmerge = array_merge((array) $getoveralllog, $overalllog);
            update_option('rsoveralllog', $logmerge);
            $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
            $newgetpoints = $getpointsmulti + $userpoints;
            if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                $newgetpoints = $newgetpoints;
            } else {
                $newgetpoints = $restrictuserpoints;
            }
            update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
        }
    }

    $rewardpointscoupons = $order->get_items(array('coupon'));

    foreach ($rewardpointscoupons as $couponcode => $value) {
        if (str_replace(' ', '', strtolower($usernickname)) == strtolower($value['name'])) {
            if (get_option('rewardsystem_looped_over_coupon' . $order_id) != '1') {
                $getcouponid = get_option($usernickname . 'coupon');
                $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
                //update_option('testings', $order->get_total_discount());
                if ($currentamount >= $value['discount_amount']) {
                    $current_conversion = get_option('rs_redeem_point');
                    $point_amount = get_option('rs_redeem_point_value');
                    $redeemedamount = $value['discount_amount'] * $current_conversion;
                    $redeemedpoints = $redeemedamount / $point_amount;
                    //$overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => 'Points Redeemed Towards Purchase', 'date' => $order->order_date);
                    //$getoveralllog = get_option('rsoveralllog');
                    //$logmerge = array_merge((array) $getoveralllog, $overalllog);
                    //update_option('rsoveralllog', $logmerge);
                    update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
                    update_user_meta($order->user_id, '_redeemed_amount', $value['discount_amount']);
                    $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
                }

                update_option('rewardsystem_looped_over_coupon' . $order_id, '1');
            }
        }
    }


    $totalpoints = get_user_meta($order->user_id, '_my_reward_points', true);

    if (get_option('rewardsystem_looped_over_coupons' . $order_id) == '1') {
        $getredeemedpoints = "0";
    } else {
        $getredeemedpoints = get_user_meta($order->user_id, '_redeemed_points', true);
    }


    $getpointsvalue = get_user_meta($order->user_id, '_redeemed_amount', true);
    $post_url = admin_url('post.php?post=' . $order_id) . '&action=edit';
    $myaccountlink = get_permalink(get_option('woocommerce_myaccount_page_id'));
    $vieworderlink = add_query_arg('view-order', $order_id, $myaccountlink);


    $overallmaingetoption = get_option('_rs_localize_points_earned_for_purchase_main');
    $fromstartarray = array('{currentorderid}');

    $frontendorderlink = '<a href="' . $vieworderlink . '">#' . $order_id . '</a>';
    $backendorderlink = '<a href="' . $post_url . '">#' . $order_id . '</a>';
    $toendarrayfrontend = array($frontendorderlink);
    $toendarraybackend = array($backendorderlink);
    $frontendstringreplace = str_replace($fromstartarray, $toendarrayfrontend, $overallmaingetoption);

    $backendstringreplace = str_replace($fromstartarray, $toendarraybackend, $overallmaingetoption);

    $pointslog[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getpointsmulti, 'points_redeemed' => $getredeemedpoints, 'points_value' => $getpointsvalue, 'before_order_points' => $userpoints, 'totalpoints' => $totalpoints, 'date' => $order->order_date, 'rewarder_for' => $backendstringreplace, 'rewarder_for_frontend' => $frontendstringreplace);
    add_option('rewardsystem_looped_over_coupons' . $order_id, '1');
}
$rewardpointscoupons = $order->get_items(array('coupon'));

foreach ($rewardpointscoupons as $couponcodeser => $value) {
    if (str_replace(' ', '', strtolower($usernickname)) == strtolower($value['name'])) {
        $getcouponid = get_option($usernickname . 'coupon');
        $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
        //update_option('testings', $order->get_total_discount());
        if ($currentamount >= $value['discount_amount']) {
            $current_conversion = get_option('rs_redeem_point');
            $point_amount = get_option('rs_redeem_point_value');
            $redeemedamount = $value['discount_amount'] * $current_conversion;
            $redeemedpoints = $redeemedamount / $point_amount;

            $redeemloginformation = get_option('_rs_localize_points_redeemed_towards_purchase');
            $startarray = array('{currentorderid}');
            $endarray = array($order_id);

            $mainupdatedvaluesinarray = str_replace($startarray, $endarray, $redeemloginformation);
            $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => $mainupdatedvaluesinarray, 'date' => $order->order_date);
            $getoveralllog = get_option('rsoveralllog');
            $logmerge = array_merge((array) $getoveralllog, $overalllog);
            update_option('rsoveralllog', $logmerge);
            update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
            update_user_meta($order->user_id, '_redeemed_amount', $value['discount_amount']);
            $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
            //update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
        }
    }
}

$getmypoints = get_user_meta($order->user_id, '_my_points_log', true);
$merged = array_merge((array) $getmypoints, $pointslog);
update_user_meta($order->user_id, '_my_points_log', $merged);
