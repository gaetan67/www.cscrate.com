<?php

class FPRewardSystemTroubleshootTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_troubleshoottab'] = __('Troubleshoot', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_troubleshoot_settings', array(
            array(
                'name' => __('Troubleshoot Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the Troubleshoot Options', 'rewardsystem'),
                'id' => '_rs_reward_point_troubleshoot_settings'
            ),
            array(
                'name' => __('Troubleshoot Option for Cart Page', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the Options for Different Hooks for your theme', 'rewardsystem'),
                'id' => '_rs_reward_point_troubleshoot_cart_page'
            ),
            array(
                'name' => __('Troubleshoot Before Cart Hook', 'rewardsystem'),
                'desc' => __('Here you can select the different hooks in Cart Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reward_point_troubleshoot_before_cart',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'woocommerce_before_cart', '2' => 'woocommerce_before_cart_table'),
                'newids' => 'rs_reward_point_troubleshoot_before_cart',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Troubleshoot After Cart Hook for Position of Redeem Points Field', 'rewardsystem'),
                'desc' => __('Here you can select the Redeem Point Position Options for Cart Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reward_point_troubleshoot_after_cart',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'woocommerce_after_cart_table', '2' => 'woocommerce_cart_coupon'),
                'newids' => 'rs_reward_point_troubleshoot_after_cart',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Troubleshoot load Tipsy jQuery settings', 'rewardsystem'),
                'desc' => __('Here you can select to change the load load tipsy option if some jQuery conflict occurs', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reward_point_enable_tipsy_social_rewards',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'Enable ', '2' => 'Disable'),
                'newids' => 'rs_reward_point_enable_tipsy_social_rewards',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_troubleshoot_cart_page'),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_troubleshoot_settings'),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'type' => 'title',
//                'desc' => __('Reward Points/Referral Reward Points awarded for the following Status', 'rewardsystem'),
//                'id' => '_rs_reward_point_order_status',
//            ),
//            array(
//                'name' => __('Award Reward Points on Order Status', 'rewardsystem'),
//                'desc' => __('Completed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_completed',
//                'css' => 'min-width:150px;',
//                'std' => 'yes',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_completed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Pending', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_pending',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_pending',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Failed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_failed',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_failed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('On Hold', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_on-hold',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_on-hold',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Processing', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_processing',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_processing',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Refunded', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_refunded',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_refunded',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Cancelled', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_cancelled',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_cancelled',
//                'desc_tip' => false,
//            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_order_status'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemTroubleshootTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemTroubleshootTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemTroubleshootTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemTroubleshootTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemTroubleshootTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_troubleshoottab', array('FPRewardSystemTroubleshootTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemTroubleshootTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_troubleshoottab', array('FPRewardSystemTroubleshootTab', 'reward_system_register_admin_settings'));
?>