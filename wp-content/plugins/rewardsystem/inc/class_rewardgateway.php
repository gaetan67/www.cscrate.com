<?php

add_action( 'plugins_loaded', 'init_reward_gateway_class' );

function init_reward_gateway_class() {
    if ( !class_exists( 'WC_Payment_Gateway' ) ) return;
    class WC_Reward_Gateway extends WC_Payment_Gateway {
        public function __construct(){
            global $woocommerce;
            
            $this->id = 'reward_gateway';
            $this->method_title = __('SUMO Reward Points Gateway', 'woocommerce');
            $this->has_fields = false;//Load Form Fields
            $this->init_form_fields();
            $this->init_settings();
            $this->title = $this->get_option('gateway_titles');
            $this->description = $this->get_option('gateway_descriptions');
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
           
        }
        
        function init_form_fields() {
             $this -> form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Enable Rewards Point Gateway', 'woowcommerce'),
                    'default' => 'no'
                ),
                'gateway_titles' => array(
                    'title' => __('Title', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('This Controls the Title which the user sees during checkout', 'woocommerce'),
                    'default' => __('SUMO Reward Points', 'woocommerce'),
                    'desc_tip' => true,
                    
                ),
                'gateway_descriptions' => array(
                    'title' => __( 'Description', 'woocommerce' ),
                    'type' => 'textarea',
                    'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
                    'default' => __('Pay with your SUMO Reward Points', 'woocommerce'),
                    'desc_tip' => true,
                ),   
                'error_payment_gateway' => array(
                    'title' => 'Error Message',
                    'type' => 'textarea',
                    'description' => __('This Controls the errror message which is displayed during Checkout', 'woocommerce'),
                    'desc_tip' => true,
                    'default' => __('You need [needpoints] Points in your Account .But You have only [userpoints] Points.','rewardsystem'),                                                     
                ), 
            );
        }
       
        function process_payment($order_id){
            global $woocommerce;
            $order = new WC_Order($order_id);
            $ordertotal = $order->get_total();
            $getuserid = $order->user_id;
            $getmyrewardpoints = get_user_meta($getuserid,'_my_reward_points',true);
            $current_conversion = get_option('rs_redeem_point');
            $point_amount = get_option('rs_redeem_point_value');
            $redeemedamount = $ordertotal * $current_conversion;
            $redeemedpoints = $redeemedamount / $point_amount;
                             
            if($redeemedpoints > $getmyrewardpoints) {               
                $error_msg =$this->get_option('error_payment_gateway');
                $find = array('[userpoints]', '[needpoints]');
                $replace = array($getmyrewardpoints, $redeemedpoints );
                $finalreplace = str_replace($find,$replace,$error_msg);
                wc_add_notice(__($finalreplace, 'woocommerce'), 'error');
                return;    
                
            }else {
                $totalrewardpoints = $getmyrewardpoints-$redeemedpoints;
                update_user_meta($order->user_id,'_my_reward_points',$totalrewardpoints);
            }
            //Status of the product purchased
            //$order->update_status('on-hold', __( 'Awaiting cheque payment', 'woocommerce' ));
            $order->payment_complete();
            $order->update_status('completed');
            //Reduce Stock Levels
            $order->reduce_order_stock();
            
            //Remove Cart
            $woocommerce->cart->empty_cart();
            
            //Redirect the User
            return array(
                'result' => 'success',
                'redirect' => $this ->get_return_url($order)
            );
            wc_add_notice( __('Payment error:', 'woothemes') . $error_message, 'error' );
            return;
        }
    }
     function add_your_gateway_class( $methods ) {
         if(is_user_logged_in()) {
	$banned_user_list = get_option('rs_banned-users_list');
        if(!in_array(get_current_user_id(), (array)$banned_user_list)){
               $getarrayofuserdata = get_userdata(get_current_user_id());
                $banninguserrole = get_option('rs_banning_user_role');
                
                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
                 $methods[] = 'WC_Reward_Gateway'; 
                 }
        }
         }
	return $methods;
        }

        add_filter( 'woocommerce_payment_gateways','add_your_gateway_class');        
}
