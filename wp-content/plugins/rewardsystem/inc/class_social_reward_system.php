<?php

class FPRewardSystemSocialRewardsTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_socialrewardstab'] = __('Social Rewards', 'rewardsystem');
        return $settings_tabs;
    }

// Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_socialrewards_settings', array(
            array(
                'name' => __('Social Rewards Facebook Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can set the application ID of Facebook', 'rewardsystem'),
                'id' => '_rs_reward_point_facebook_application_settings',
            ),
            array(
                'name' => __('Facebook Application ID', 'rewardsystem'),
                'desc' => __('Please Enter Application ID of your Facebook', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_facebook_application_id',
                'css' => 'min-width:150px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_facebook_application_id',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_facebook_application_settings'),
            array(
                'name' => __('Social Button Position Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can position the social buttons', 'rewardsystem'),
                'id' => '_rs_reward_point_socialrewards_position_settings'
            ),
            array(
                'name' => __('Social Buttons Position', 'rewardsystem'),
                'desc' => __('This helps to Position SUMO Social Buttons in Different Places', 'rewardsystem'),
                'id' => 'rs_global_position_sumo_social_buttons',
                'css' => 'min-width:150px;',
                'std' => '5',
                'default' => '5',
                'desc_tip' => true,
                'newids' => 'rs_global_position_sumo_social_buttons',
                'type' => 'select',
                'options' => array(
                    '1' => __('Before Single Product', 'rewardsystem'),
                    '2' => __('Before Single Product Summary', 'rewardsystem'),
                    '3' => __('Single Product Summary', 'rewardsystem'),
                    '4' => __('After Single Product', 'rewardsystem'),
                    '5' => __('After Single Product Summary', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Social Button Position Type', 'rewardsystem'),
                'desc' => __('Select the Social Button Position Type ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_social_button_position_troubleshoot',
                'newids' => 'rs_social_button_position_troubleshoot',
                'css' => 'min-width:150px;',
                'std' => 'inline',
                'default' => 'inline',
                'type' => 'select',
                'options' => array(
                    'inline' => __('Inline', 'rewardsystem'),
                    'inline-block' => __('Inline Block', 'rewardsystem'),
                    'inline-flex' => __('Inline Flex', 'rewardsystem'),
                    'inline-table' => __('Inline Table', 'rewardsystem'),
                    'table' => __('Table', 'rewardsystem'),
                    'block' => __('Block', 'rewardsystem'),
                    'flex' => __('Flex', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_socialrewards_position_settings'),
            array(
                'name' => __('Social Button URL Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can Select the type of URL for Social Buttons', 'rewardsystem'),
                'id' => '_rs_reward_point_socialrewards_url_settings'
            ),
            array(
                'name' => __('Facebook URL Selection', 'rewardsystem'),
                'desc' => __('Select the Type of URL that you wish to enable for Facebook', 'rewardsystem'),
                'id' => 'rs_global_social_facebook_url',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_social_facebook_url',
                'type' => 'select',
                'options' => array(
                    '1' => __('Default', 'rewardsystem'),
                    '2' => __('Custom', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Facebook Custom URL', 'rewardsystem'),
                'desc' => __('Enter the Custom URL that you wish to enable for Facebook', 'rewardsystem'),
                'type' => 'text',
                'id' => 'rs_global_social_facebook_url_custom',
                'css' => 'min-width:150px;',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Twitter URL Selection', 'rewardsystem'),
                'desc' => __('Select the Type of URL that you wish to enable for Twitter', 'rewardsystem'),
                'id' => 'rs_global_social_twitter_url',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_social_twitter_url',
                'type' => 'select',
                'options' => array(
                    '1' => __('Default', 'rewardsystem'),
                    '2' => __('Custom', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Twitter Custom URL', 'rewardsystem'),
                'desc' => __('Enter the Custom URL that you wish to enable for Twitter', 'rewardsystem'),
                'type' => 'text',
                'id' => 'rs_global_social_twitter_url_custom',
                'css' => 'min-width:150px;',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Google URL Selection', 'rewardsystem'),
                'desc' => __('Select the Type of URL that you wish to enable for Google', 'rewardsystem'),
                'id' => 'rs_global_social_google_url',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_social_google_url',
                'type' => 'select',
                'options' => array(
                    '1' => __('Default', 'rewardsystem'),
                    '2' => __('Custom', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Google Custom URL', 'rewardsystem'),
                'desc' => __('Enter the Custom URL that you wish to enable for Google', 'rewardsystem'),
                'type' => 'text',
                'id' => 'rs_global_social_google_url_custom',
                'css' => 'min-width:150px;',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_socialrewards_url_settings'),
            array(
                'name' => __('Social Reward Points Global', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_global_social_reward_points'
            ),
            array(
                'name' => __('Enable SUMO Reward Points for Social Promotion', 'rewardsystem'),
                'desc' => __('This helps to Enable Social Reward Points in Global Level', 'rewardsystem'),
                'id' => 'rs_global_social_enable_disable_reward',
                'css' => 'min-width:150px;',
                'std' => '2',
                'default' => '2',
                'desc_tip' => true,
                'newids' => 'rs_global_social_enable_disable_reward',
                'type' => 'select',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Facebook Like', 'multivendor'),
                'desc' => __('Choose Option to Show/Hide Facebook Like Button', 'multivendor'),
                'id' => 'rs_global_show_hide_facebook_like_button',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_show_hide_facebook_like_button',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Social Reward Type for Facebook', 'rewardsystem'),
                'desc' => __('Select Social Reward Type for Facebook by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_global_social_reward_type_facebook',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_social_reward_type_facebook',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Social Reward Points for Facebook', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points for facebook', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_social_facebook_reward_points',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_social_facebook_reward_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Social Reward Points for Facebook in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_social_facebook_reward_percent',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_social_facebook_reward_percent',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Twitter Tweet', 'multivendor'),
                'desc' => __('Choose Option to Show/Hide Twitter Tweet Button', 'multivendor'),
                'id' => 'rs_global_show_hide_twitter_tweet_button',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_show_hide_twitter_tweet_button',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Social Reward Type for Twitter', 'rewardsystem'),
                'desc' => __('Select Social Reward Type for Twitter by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_global_social_reward_type_twitter',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_social_reward_type_twitter',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Social Reward Points for Twitter', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points for Twitter', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_social_twitter_reward_points',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_social_twitter_reward_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Social Reward Points for Twitter in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_social_twitter_reward_percent',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_social_twitter_reward_percent',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Google+', 'multivendor'),
                'desc' => __('Choose Option to Show/Hide Google+ Button', 'multivendor'),
                'id' => 'rs_global_show_hide_google_plus_button',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_show_hide_google_plus_button',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Social Reward Type for Google', 'rewardsystem'),
                'desc' => __('Select Social Reward Type for Google by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_global_social_reward_type_google',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_social_reward_type_google',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Social Reward Points for Google', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points for Google', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_social_google_reward_points',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_social_google_reward_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Social Reward Points for Google in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_social_google_reward_percent',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_social_google_reward_percent',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_global_social_reward_points'),
            array(
                'name' => __('Social Message Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_global_social_message_settings'
            ),
            array(
                'name' => __('Show/Hide Social ToolTip', 'rewardsystem'),
                'desc' => __('Select Show/Hide Social ToolTip', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_show_hide_social_tooltip',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'options' => array(
                    '1' => 'Show',
                    '2' => 'Hide'
                ),
                'newids' => 'rs_global_show_hide_social_tooltip',
                'desc_tip' => true,
            ),
            array(
                'name' => __('ToolTip Facebook Message ', 'rewardsystem'),
                'desc' => __('Enter ToolTip Message for Facebook', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_social_message_for_facebook',
                'css' => 'min-width:550px;',
                'std' => 'Facebook Like will fetch you [facebook_like_reward_points] Reward Points',
                'type' => 'textarea',
                'newids' => 'rs_social_message_for_facebook',
                'desc_tip' => true,
            ),
            array(
                'name' => __('ToolTip Twitter Message ', 'rewardsystem'),
                'desc' => __('Enter ToolTip Message for Twitter', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_social_message_for_twitter',
                'css' => 'min-width:550px;',
                'std' => 'Twitter Tweet will fetch you [twitter_tweet_reward_points] Reward Points',
                'type' => 'textarea',
                'newids' => 'rs_social_message_for_twitter',
                'desc_tip' => true,
            ),
            array(
                'name' => __('ToolTip Google+ Share Message ', 'rewardsystem'),
                'desc' => __('Enter ToolTip Message for Google+ Share', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_social_message_for_google_plus',
                'css' => 'min-width:550px;',
                'std' => 'Google+ Share will fetch you [google_share_reward_points] Reward Points',
                'type' => 'textarea',
                'newids' => 'rs_social_message_for_google_plus',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_global_social_message_settings'),
            array(
                'name' => __('ToolTip Customization', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_global_social_color_customization'
            ),
            array(
                'name' => __('ToolTip Background Color', 'rewardsystem'),
                'desc' => __('Enter ToolTip Background Color', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_social_tooltip_bg_color',
                'css' => 'min-width:150px;',
                'std' => '000',
                'type' => 'text',
                'class' => 'color',
                'newids' => 'rs_social_tooltip_bg_color',
                'desc_tip' => true,
            ),
            array(
                'name' => __('ToolTip Text Color', 'rewardsystem'),
                'desc' => __('Enter ToolTip Text Color', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_social_tooltip_text_color',
                'css' => 'min-width:150px;',
                'std' => 'fff',
                'type' => 'text',
                'class' => 'color',
                'newids' => 'rs_social_tooltip_text_color',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_global_social_color_customization'),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_global_social_troubleshoot'
            ),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'desc' => __('Choose your Custom CSS Style for Social Sharing Buttons', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_social_custom_css',
                'css' => 'min-width:350px;min-height:250px;',
                'std' => '.rs_social_sharing_buttons{}',
                'newids' => 'rs_social_custom_css',
                'type' => 'textarea',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_global_social_troubleshoot'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemSocialRewardsTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemSocialRewardsTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemSocialRewardsTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function reward_system_social_url() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                if ((jQuery('#rs_global_social_facebook_url').val()) === '2') {
                    jQuery('#rs_global_social_facebook_url_custom').parent().parent().show();
                } else {
                    jQuery('#rs_global_social_facebook_url_custom').parent().parent().hide();
                }
                if ((jQuery('#rs_global_social_twitter_url').val()) === '2') {
                    jQuery('#rs_global_social_twitter_url_custom').parent().parent().show();
                } else {
                    jQuery('#rs_global_social_twitter_url_custom').parent().parent().hide();
                }
                if ((jQuery('#rs_global_social_google_url').val()) === '2') {
                    jQuery('#rs_global_social_google_url_custom').parent().parent().show();
                } else {
                    jQuery('#rs_global_social_google_url_custom').parent().parent().hide();
                }
                jQuery('#rs_global_social_facebook_url').change(function() {
                    jQuery('#rs_global_social_facebook_url_custom').parent().parent().toggle();
                });
                jQuery('#rs_global_social_twitter_url').change(function() {
                    jQuery('#rs_global_social_twitter_url_custom').parent().parent().toggle();
                });
                jQuery('#rs_global_social_google_url').change(function() {
                    jQuery('#rs_global_social_google_url_custom').parent().parent().toggle();
                });
            });
        </script>
        <?php
    }

    public static function reward_system_social_likes() {
        global $woocommerce;
        global $post;
        $twitter_points = '50';
        $facebook_points = '100';
        $google_points = '150';
        ?>
        <div id="fb-root"></div>
        <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({
                    appId: "<?php echo get_option('rs_facebook_application_id'); ?>",
                    xfbml: true,
                    version: 'v2.0'
                });
            };
            console.log('loaded script . . . . . ');
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            console.log('script loaded');</script>

        <script>!function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + '://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');</script>

        <script>
            var originalCallback = function(o) {
                console.log(o);
                console.log('original callback - ' + o.state);
                var state = o.state;
                var dataparam = ({
                    action: 'rssocialgooglecallback',
                    state: state,
                    points: '<?php echo $google_points; ?>',
                    postid: '<?php echo $post->ID; ?>',
                    currentuserid: '<?php echo get_current_user_id(); ?>',
                });
                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                        function(response) {
                            //alert(response);
                            //                                var newresponse = response.replace(/\s/g, '');
                            //                                if (newresponse === 'success') {
                            //
                            //                                }
                        });
                return false;
            };</script>

        <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
        </script>
        <style type="text/css">
            .fb_iframe_widget {
                display:inline-flex !important;
            }
            .twitter-share-button {
                width:88px !important;
            }
        </style>
                <?php
                $enablerewards = get_post_meta($post->ID, '_socialrewardsystemcheckboxvalue', true);
                if ($enablerewards == 'yes') {
                    ?>
            <style type="text/css">
                    <?php echo get_option('rs_social_custom_css'); ?>
            </style>
            <table class="rs_social_sharing_buttons" style="display:<?php echo get_option('rs_social_button_position_troubleshoot'); ?>">
                <tr>
            <?php if (get_option('rs_global_show_hide_facebook_like_button') == '1') { ?>
                        <td> <div class="fb-like" data-href="<?php echo get_option('rs_global_social_facebook_url') == '1' ? get_permalink() : get_option('rs_global_social_facebook_url_custom'); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div></td>
            <?php } ?>
            <?php if (get_option('rs_global_show_hide_twitter_tweet_button') == '1') { ?>
                        <!--<td><a href="https://twitter.com/share" class="twitter-share-button" data-href="get_permalink()"> data-lang="en">Tweet</a></td>-->
                        <td><a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo get_option('rs_global_social_twitter_url') == '1' ? get_permalink() : get_option('rs_global_social_twitter_url_custom'); ?>">Tweet</a></td>           
 <?php } ?>
            <?php if (get_option('rs_global_show_hide_google_plus_button') == '1') { ?>
                        <td> <g:plusone annotation="bubble" class="google-plus-one" href='<?php echo get_option('rs_global_social_google_url') == '1' ? get_permalink() : get_option('rs_global_social_google_url_custom'); ?>' callback="originalCallback"></g:plusone></td>
            <?php } ?>
            </tr>
            </table>
        <?php } ?>
        <script type='text/javascript'>
            jQuery(window).load(function() {
                /* This is for facebook which is been like or not */
                var page_like_callback = function(url, html_element) {
                    console.log("page_like");
                    console.log(url);
                    console.log(html_element);
                    var dataparam = ({
                        action: 'rssocialfacebookcallback',
                        state: 'on',
                        postid: '<?php echo $post->ID; ?>',
                        points: '<?php echo $facebook_points; ?>',
                        currentuserid: '<?php echo get_current_user_id(); ?>',
                    });
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                            function(response) {
                                //alert(response);
                                //                                var newresponse = response.replace(/\s/g, '');
                                //                                if (newresponse === 'success') {
                                //
                                //                                }
                            });
                    return false;
                }

                var page_unlike_callback = function(url, html_element) {
                    console.log('page_unlike');
                    console.log(url);
                    console.log(html_element);
                    var dataparam = ({
                        action: 'rssocialfacebookcallback',
                        state: 'off',
                        postid: '<?php echo $post->ID; ?>',
                        points: '<?php echo $facebook_points; ?>',
                        currentuserid: '<?php echo get_current_user_id(); ?>'
                    });
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                            function(response) {
                                //alert(response);
                                //                                var newresponse = response.replace(/\s/g, '');
                                //                                if (newresponse === 'success') {
                                //
                                //                                }
                            });
                    return false;
                }
                // Detect Like or Unlike using Event Subscribe of Facebook
                FB.Event.subscribe('edge.create', page_like_callback);
                FB.Event.subscribe('edge.remove', page_unlike_callback);
                // This below code is for Twitter Tweet
                twttr.events.bind('tweet', function(ev) {

                    console.log('You Tweet Successfully');
                    var dataparam = ({
                        action: 'rssocialtwittercallback',
                        state: 'on',
                        postid: '<?php echo $post->ID; ?>',
                        points: '<?php echo $twitter_points; ?>',
                        currentuserid: '<?php echo get_current_user_id(); ?>',
                    });
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                            function(response) {
                                //alert(response);
                                //                                var newresponse = response.replace(/\s/g, '');
                                //                                if (newresponse === 'success') {
                                //
                                //                                }
                            });
                    return false;
                });
            });</script>
        <?php
    }

    public static function updation_social_twitter_reward_points() {
        $banned_user_list = get_option('rs_banned-users_list');
        if (!in_array(get_current_user_id(), $banned_user_list)) {
               $getarrayofuserdata = get_userdata(get_current_user_id());
                $banninguserrole = get_option('rs_banning_user_role');
                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
            if (get_option('timezone_string') != '') {
                $timezonedate = date_default_timezone_set(get_option('timezone_string'));
            } else {
                $timezonedate = date_default_timezone_set('UTC');
            }
            global $woocommerce;
            $global_enable = get_option('rs_global_social_enable_disable_reward');
            $global_reward_type = get_option('rs_global_social_reward_type_twitter');
            if (isset($_POST['state']) && ($_POST['postid']) && ($_POST['currentuserid'])) {
                $rewardpoints = array('0');
                $global_enable = get_option('rs_global_social_enable_disable_reward');
                $global_reward_type = get_option('rs_global_social_reward_type_twitter');
                $enablerewards = get_post_meta($_POST['postid'], '_socialrewardsystemcheckboxvalue', true);
                $getaction = get_post_meta($_POST['postid'], '_social_rewardsystem_options_twitter', true);
                $getpoints = get_post_meta($_POST['postid'], '_socialrewardsystempoints_twitter', true);
                $getpercent = get_post_meta($_POST['postid'], '_socialrewardsystempercent_twitter', true);
                $getregularprice = get_post_meta($_POST['postid'], '_regular_price', true);

                $checkproduct = get_product($_POST['postid']);
                if ($enablerewards == 'yes') {
                    if ($getaction == '1') {
                        if ($getpoints == '') {
                            $term = get_the_terms($_POST['postid'], 'product_cat');
                            if (is_array($term)) {
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'social_twitter_enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $rewardpoints[] = $getpoints;
                        }
                    } else {
                        $points = get_option('rs_earn_point');
                        $pointsequalto = get_option('rs_earn_point_value');
                        $takeaverage = $getpercent / 100;
                        $mainaveragevalue = $takeaverage * $getregularprice;
                        $addinpoint = $mainaveragevalue * $points;
                        $totalpoint = $addinpoint / $pointsequalto;
                        if ($getpercent === '') {
                            $term = get_the_terms($_POST['postid'], 'product_cat');
// var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'social_twitter_enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) == '') {

                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_enable == '1') {
                                    if ($global_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    }
                    if (!empty($rewardpoints)) {
                        $getpoints = max($rewardpoints);
                    }
                }

                $getearnedpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                $getarrayids[] = $_POST['postid'];
                $oldoption = get_user_meta($_POST['currentuserid'], '_rstwittertweet', true);
//delete_user_meta(get_current_user_id(), '_mytestids');
                if (!empty($oldoption)) {
                    if (!in_array($_POST['postid'], $oldoption)) {
                        $mergedata = array_merge((array) $oldoption, $getarrayids);
                        update_user_meta(get_current_user_id(), '_rstwittertweet', $mergedata);
                        if ($_POST['state'] == 'on') {
                            $totalpoints = $getearnedpoints + $getpoints;
                            update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                            $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                            $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => $getpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_twitter_tweet'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_twitter_tweet'));
                            $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_twitter_tweet'), 'date' => date('Y-m-d H:i:s'));
                            $getoveralllogs = get_option('rsoveralllog');
                            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                            update_option('rsoveralllog', $logmerges);
                            $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                            $mergeds = array_merge((array) $getmypointss, $pointslogs);
                            update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                        }
                    } else {
                        echo "Current Post Id is already there ";
                    }
                } else {
                    update_user_meta($_POST['currentuserid'], '_rstwittertweet', $getarrayids);
                    if ($_POST['state'] == 'on') {
                        $totalpoints = $getearnedpoints + $getpoints;
                        update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                        $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                        $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => $getpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_twitter_tweet'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_twitter_tweet'));
                        $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_twitter_tweet'), 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);
                        $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                        $mergeds = array_merge((array) $getmypointss, $pointslogs);
                        update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                    }
                }



                echo "Ajax Call Successfully Triggered";
            }
            exit();
        }
    }
    }

    public static function updation_social_google_reward_points() {
        $banned_user_list = get_option('rs_banned-users_list');
        if (!in_array(get_current_user_id(), $banned_user_list)) {
               $getarrayofuserdata = get_userdata(get_current_user_id());
                $banninguserrole = get_option('rs_banning_user_role');
                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
            if (get_option('timezone_string') != '') {
                $timezonedate = date_default_timezone_set(get_option('timezone_string'));
            } else {
                $timezonedate = date_default_timezone_set('UTC');
            }
            if (isset($_POST['state']) && ($_POST['postid']) && ($_POST['currentuserid'])) {
                $rewardpoints = array('0');
                $global_enable = get_option('rs_global_social_enable_disable_google');
                $global_reward_type = get_option('rs_global_social_reward_type_google');
                $enablerewards = get_post_meta($_POST['postid'], '_socialrewardsystemcheckboxvalue', true);
                $getaction = get_post_meta($_POST['postid'], '_social_rewardsystem_options_google', true);
                $getpoints = get_post_meta($_POST['postid'], '_socialrewardsystempoints_google', true);
                $getpercent = get_post_meta($_POST['postid'], '_socialrewardsystempercent_google', true);
                $getregularprice = get_post_meta($_POST['postid'], '_regular_price', true);
                if ($enablerewards == 'yes') {
                    if ($getaction == '1') {
                        if ($getpoints == '') {
                            $term = get_the_terms($_POST['postid'], 'product_cat');
                            if (is_array($term)) {
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'social_google_enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $rewardpoints[] = $getpoints;
                        }
                    } else {
                        $points = get_option('rs_earn_point');
                        $pointsequalto = get_option('rs_earn_point_value');
                        $takeaverage = $getpercent / 100;
                        $mainaveragevalue = $takeaverage * $getregularprice;
                        $addinpoint = $mainaveragevalue * $points;
                        $totalpoint = $addinpoint / $pointsequalto;
                        if ($getpercent === '') {
                            $term = get_the_terms($_POST['postid'], 'product_cat');
// var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'social_google_enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_google_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_percent', true) == '') {

                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_google_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_enable == '1') {
                                    if ($global_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_social_google_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    }
                    if (!empty($rewardpoints)) {
                        $getpoints = max($rewardpoints);
                    }
                }

                $getearnedpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                $getarrayids[] = $_POST['postid'];
                $oldoption = get_user_meta(get_current_user_id(), '_rsgoogleshares', true);
//delete_user_meta(get_current_user_id(), '_mytestids');
                if (!empty($oldoption)) {
                    if (!in_array($_POST['postid'], $oldoption)) {
                        $mergedata = array_merge((array) $oldoption, $getarrayids);
                        update_user_meta($_POST['currentuserid'], '_rsgoogleshares', $mergedata);
                        if ($_POST['state'] == 'on') {
                            $totalpoints = $getearnedpoints + $getpoints;
                            update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                            $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                            $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => $getpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_google_plus'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_google_plus'));
                            $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_google_plus'), 'date' => date('Y-m-d H:i:s'));
                            $getoveralllogs = get_option('rsoveralllog');
                            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                            update_option('rsoveralllog', $logmerges);
                            $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                            $mergeds = array_merge((array) $getmypointss, $pointslogs);
                            update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                        }
                    } else {
                        echo "Current Post Id is already there ";
                    }
                } else {
                    update_user_meta(get_current_user_id(), '_rsgoogleshares', $getarrayids);
                    if ($_POST['state'] == 'on') {
                        $totalpoints = $getearnedpoints + $getpoints;
                        update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                        $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                        $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => $getpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_google_plus'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_google_plus'));
                        $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_google_plus'), 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);
                        $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                        $mergeds = array_merge((array) $getmypointss, $pointslogs);
                        update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                    }
                }

                if ($_POST['state'] == 'off') {
                    $getarrayunlikeids[] = $_POST['postid'];
                    $oldunlikeoption = get_user_meta($_POST['currentuserid'], '_rsgoogleplusunlikes', true);
                    if (!empty($oldunlikeoption)) {
                        if (!in_array($_POST['postid'], $oldunlikeoption)) {
                            $totalpoints = $getearnedpoints - $getpoints;
                            update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                            $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                            $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => '', 'points_redeemed' => $getpoints, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_google_plus_revised'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_google_plus_revised'));
                            $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_google_plus_revised'), 'date' => date('Y-m-d H:i:s'));
                            $getoveralllogs = get_option('rsoveralllog');
                            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                            update_option('rsoveralllog', $logmerges);
                            $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                            $mergeds = array_merge((array) $getmypointss, $pointslogs);
                            update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                        }
                    } else {
                        update_user_meta($_POST['currentuserid'], '_rsgoogleplusunlikes', $getarrayunlikeids);
                        $totalpoints = $getearnedpoints - $getpoints;
                        update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                        $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                        $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => '', 'points_redeemed' => $getpoints, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_google_plus_revised'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_google_plus_revised'));
                        $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_google_plus_revised'), 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);
                        $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                        $mergeds = array_merge((array) $getmypointss, $pointslogs);
                        update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                    }
                }
                echo "Ajax Call Successfully Triggered";
            }

            exit();
        }
    }
    }

    public static function add_shortcode_for_social_google_share($contents) {
        ob_start();
        global $post;
        global $woocommerce;
        $rewardpoints = array('0');
        $global_enable = get_option('rs_global_social_enable_disable_google');
        $global_reward_type = get_option('rs_global_social_reward_type_google');
        $enablerewards = get_post_meta($post->ID, '_socialrewardsystemcheckboxvalue', true);
        $getaction = get_post_meta($post->ID, '_social_rewardsystem_options_google', true);
        $getpoints = get_post_meta($post->ID, '_socialrewardsystempoints_google', true);
        $getpercent = get_post_meta($post->ID, '_socialrewardsystempercent_google', true);
        $getregularprice = get_post_meta($post->ID, '_regular_price', true);
        if ($enablerewards == 'yes') {
            if ($getaction == '1') {
                if ($getpoints == '') {
                    $term = get_the_terms($post->ID, 'product_cat');
                    if (is_array($term)) {
                        foreach ($term as $term) {
                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'social_google_enable_rs_rule', true);
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $rewardpoints[] = $getpoints;
                }
            } else {
                $points = get_option('rs_earn_point');
                $pointsequalto = get_option('rs_earn_point_value');
                $takeaverage = $getpercent / 100;
                $mainaveragevalue = $takeaverage * $getregularprice;
                $addinpoint = $mainaveragevalue * $points;
                $totalpoint = $addinpoint / $pointsequalto;
                if ($getpercent === '') {
                    $term = get_the_terms($post->ID, 'product_cat');
// var_dump($term);
                    if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                        foreach ($term as $term) {
                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'social_google_enable_rs_rule', true);
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_google_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_percent', true) == '') {

                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_google_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($global_enable == '1') {
                            if ($global_reward_type == '1') {
                                $rewardpoints[] = get_option('rs_global_social_google_reward_points');
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_option('rs_global_social_google_reward_percent') / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                            }
                        }
                    }
                }
            }
            if (!empty($rewardpoints)) {
                echo $getpoints = max($rewardpoints);
            }
            $newcontnt = ob_get_clean();
            return $newcontnt;
        }
    }

    public static function add_shortcode_for_social_facebook_like($contents) {
        ob_start();
        global $woocommerce;
        global $post;
        $rewardpoints = array('0');
        $global_enable = get_option('rs_global_social_enable_disable_facebook');
        $global_reward_type = get_option('rs_global_social_reward_type_facebook');
        $enablerewards = get_post_meta($post->ID, '_socialrewardsystemcheckboxvalue', true);
        $getaction = get_post_meta($post->ID, '_social_rewardsystem_options_facebook', true);
        $getpoints = get_post_meta($post->ID, '_socialrewardsystempoints_facebook', true);
        $getpercent = get_post_meta($post->ID, '_socialrewardsystempercent_facebook', true);
        $getregularprice = get_post_meta($post->ID, '_regular_price', true);
        if ($enablerewards == 'yes') {
            if ($getaction == '1') {
                if ($getpoints == '') {
                    $term = get_the_terms($post->ID, 'product_cat');
                    if (is_array($term)) {
                        foreach ($term as $term) {
                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'social_facebook_enable_rs_rule', true);
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $rewardpoints[] = $getpoints;
                }
            } else {
                $points = get_option('rs_earn_point');
                $pointsequalto = get_option('rs_earn_point_value');
                $takeaverage = $getpercent / 100;
                $mainaveragevalue = $takeaverage * $getregularprice;
                $addinpoint = $mainaveragevalue * $points;
                $totalpoint = $addinpoint / $pointsequalto;
                if ($getpercent === '') {
                    $term = get_the_terms($post->ID, 'product_cat');
// var_dump($term);
                    if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                        foreach ($term as $term) {
                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'social_facebook_enable_rs_rule', true);
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) == '') {

                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($global_enable == '1') {
                            if ($global_reward_type == '1') {
                                $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                            }
                        }
                    }
                }
            }
            if (!empty($rewardpoints)) {
                echo $getpoints = max($rewardpoints);
            }
            $newcontentss = ob_get_clean();
            return $newcontentss;
        }
    }

    public static function add_shortcode_for_social_twitter_tweet() {
        ob_start();
        global $woocommerce;
        global $post;
        $rewardpoints = array('0');
        $global_enable = get_option('rs_global_social_enable_disable_reward');
        $global_reward_type = get_option('rs_global_social_reward_type_twitter');
        $enablerewards = get_post_meta($post->ID, '_socialrewardsystemcheckboxvalue', true);
        $getaction = get_post_meta($post->ID, '_social_rewardsystem_options_twitter', true);
        $getpoints = get_post_meta($post->ID, '_socialrewardsystempoints_twitter', true);
        $getpercent = get_post_meta($post->ID, '_socialrewardsystempercent_twitter', true);
        $getregularprice = get_post_meta($post->ID, '_regular_price', true);

        $checkproduct = get_product($post->ID);
        if ($enablerewards == 'yes') {
            if ($getaction == '1') {
                if ($getpoints == '') {
                    $term = get_the_terms($post->ID, 'product_cat');
                    if (is_array($term)) {
                        foreach ($term as $term) {
                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'social_twitter_enable_rs_rule', true);
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $rewardpoints[] = $getpoints;
                }
            } else {
                $points = get_option('rs_earn_point');
                $pointsequalto = get_option('rs_earn_point_value');
                $takeaverage = $getpercent / 100;
                $mainaveragevalue = $takeaverage * $getregularprice;
                $addinpoint = $mainaveragevalue * $points;
                $totalpoint = $addinpoint / $pointsequalto;
                if ($getpercent === '') {
                    $term = get_the_terms($post->ID, 'product_cat');
// var_dump($term);
                    if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                        foreach ($term as $term) {
                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                            $display_type = get_woocommerce_term_meta($term->term_id, 'social_twitter_enable_rs_rule', true);
                            if ($enablevalue == 'yes') {
                                if ($display_type == '1') {
                                    if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true) == '') {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true);
                                    }
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    if (get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true) == '') {

                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($global_enable == '1') {
                            if ($global_reward_type == '1') {
                                $rewardpoints[] = get_option('rs_global_social_twitter_reward_points');
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = get_option('rs_global_social_twitter_reward_percent') / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                            }
                        }
                    }
                }
            }
            if (!empty($rewardpoints)) {
                echo $getpoints = max($rewardpoints);
            }
            $newcontents = ob_get_clean();
            return $newcontents;
        }
    }

    public static function updation_social_facebook_reward_points() {
        $banned_user_list = get_option('rs_banned-users_list');
        if (!in_array(get_current_user_id(), $banned_user_list)) {
               $getarrayofuserdata = get_userdata(get_current_user_id());
                $banninguserrole = get_option('rs_banning_user_role');
                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
            if (get_option('timezone_string') != '') {
                $timezonedate = date_default_timezone_set(get_option('timezone_string'));
            } else {
                $timezonedate = date_default_timezone_set('UTC');
            }
            if (isset($_POST['state']) && ($_POST['postid']) && ($_POST['currentuserid'])) {
                $rewardpoints = array('0');
                $global_enable = get_option('rs_global_social_enable_disable_facebook');
                $global_reward_type = get_option('rs_global_social_reward_type_facebook');
                $enablerewards = get_post_meta($_POST['postid'], '_socialrewardsystemcheckboxvalue', true);
                $getaction = get_post_meta($_POST['postid'], '_social_rewardsystem_options_facebook', true);
                $getpoints = get_post_meta($_POST['postid'], '_socialrewardsystempoints_facebook', true);
                $getpercent = get_post_meta($_POST['postid'], '_socialrewardsystempercent_facebook', true);
                $getregularprice = get_post_meta($_POST['postid'], '_regular_price', true);
                if ($enablerewards == 'yes') {
                    if ($getaction == '1') {
                        if ($getpoints == '') {
                            $term = get_the_terms($_POST['postid'], 'product_cat');
                            if (is_array($term)) {
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'social_facebook_enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $rewardpoints[] = $getpoints;
                        }
                    } else {
                        $points = get_option('rs_earn_point');
                        $pointsequalto = get_option('rs_earn_point_value');
                        $takeaverage = $getpercent / 100;
                        $mainaveragevalue = $takeaverage * $getregularprice;
                        $addinpoint = $mainaveragevalue * $points;
                        $totalpoint = $addinpoint / $pointsequalto;
                        if ($getpercent === '') {
                            $term = get_the_terms($_POST['postid'], 'product_cat');
// var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'social_facebook_enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true) == '') {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true) == '') {

                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_enable == '1') {
                                    if ($global_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_social_facebook_reward_points');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_social_facebook_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                        }
                    }
                    if (!empty($rewardpoints)) {
                        $getpoints = max($rewardpoints);
                    }
                }


                $getearnedpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                $getarrayids[] = $_POST['postid'];
                $oldoption = get_user_meta($_POST['currentuserid'], '_rsfacebooklikes', true);
//delete_user_meta(get_current_user_id(), '_mytestids');
                if (!empty($oldoption)) {
                    if (!in_array($_POST['postid'], $oldoption)) {
                        $mergedata = array_merge((array) $oldoption, $getarrayids);
                        update_user_meta($_POST['currentuserid'], '_rsfacebooklikes', $mergedata);
                        if ($_POST['state'] == 'on') {
                            $totalpoints = $getearnedpoints + $getpoints;
                            update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                            $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                            $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => $getpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_facebook_like'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_facebook_like'));
                            $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_facebook_like'), 'date' => date('Y-m-d H:i:s'));
                            $getoveralllogs = get_option('rsoveralllog');
                            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                            update_option('rsoveralllog', $logmerges);
                            $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                            $mergeds = array_merge((array) $getmypointss, $pointslogs);
                            update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                        }
                    } else {
                        echo "You already liked this post ";
                    }
                } else {
                    update_user_meta($_POST['currentuserid'], '_rsfacebooklikes', $getarrayids);
                    if ($_POST['state'] == 'on') {
                        $totalpoints = $getearnedpoints + $getpoints;
                        update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                        $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                        $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => $getpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_facebook_like'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_facebook_like'));
                        $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_facebook_like'), 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);
                        $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                        $mergeds = array_merge((array) $getmypointss, $pointslogs);
                        update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                    }
                }

                if ($_POST['state'] == 'off') {
                    $getarrayunlikeids[] = $_POST['postid'];
                    $oldunlikeoption = get_user_meta($_POST['currentuserid'], '_rsfacebookunlikes', true);
                    if (!empty($oldunlikeoption)) {
                        if (!in_array($_POST['postid'], $oldunlikeoption)) {
                            $mergedunlikedata = array_merge((array) $oldunlikeoption, $getarrayunlikeids);
                            update_user_meta($_POST['currentuserid'], '_rsfacebookunlikes', $mergedunlikedata);
                            $totalpoints = $getearnedpoints - $getpoints;
                            update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                            $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                            $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => '', 'points_redeemed' => $getpoints, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_facebook_like_revised'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_facebook_like_revised'));
                            $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_facebook_like_revised'), 'date' => date('Y-m-d H:i:s'));
                            $getoveralllogs = get_option('rsoveralllog');
                            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                            update_option('rsoveralllog', $logmerges);
                            $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                            $mergeds = array_merge((array) $getmypointss, $pointslogs);
                            update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                        }
                    } else {
                        update_user_meta($_POST['currentuserid'], '_rsfacebookunlikes', $getarrayunlikeids);
                        $totalpoints = $getearnedpoints - $getpoints;
                        update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
                        $mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
                        $pointslogs[] = array('orderid' => '', 'userid' => $_POST['currentuserid'], 'points_earned_order' => '', 'points_redeemed' => $getpoints, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => get_option('_rs_localize_reward_for_facebook_like_revised'), 'rewarder_for_frontend' => get_option('_rs_localize_reward_for_facebook_like_revised'));
                        $overalllogs[] = array('userid' => $_POST['currentuserid'], 'totalvalue' => $getpoints, 'eventname' => get_option('_rs_localize_reward_for_facebook_like_revised'), 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);
                        $getmypointss = get_user_meta($_POST['currentuserid'], '_my_points_log', true);
                        $mergeds = array_merge((array) $getmypointss, $pointslogs);
                        update_user_meta($_POST['currentuserid'], '_my_points_log', $mergeds);
                    }
                }
                echo "Ajax Call Successfully Triggered";
            }
            exit();
        }
    }
    }

    public static function social_rewards_global_settings_script() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                /*Facebook Reward Type Validation in jQuery Start*/
                if ((jQuery('#rs_global_social_reward_type_facebook').val()) === '1') {
                    jQuery('#rs_global_social_facebook_reward_points').parent().parent().show();
                    jQuery('#rs_global_social_facebook_reward_percent').parent().parent().hide();
                } else {
                    jQuery('#rs_global_social_facebook_reward_points').parent().parent().hide();
                    jQuery('#rs_global_social_facebook_reward_percent').parent().parent().show();
                }
                jQuery('#rs_global_social_reward_type_facebook').change(function() {
                    if ((jQuery(this).val()) === '1') {
                        jQuery('#rs_global_social_facebook_reward_points').parent().parent().show();
                        jQuery('#rs_global_social_facebook_reward_percent').parent().parent().hide();
                    } else {
                        jQuery('#rs_global_social_facebook_reward_points').parent().parent().hide();
                        jQuery('#rs_global_social_facebook_reward_percent').parent().parent().show();
                    }
                });
                /*Facebook Reward Type Validation in jQuery Ends*/

                /*Twitter Reward Type Validation in jQuery Start*/
                if ((jQuery('#rs_global_social_reward_type_twitter').val()) === '1') {
                    jQuery('#rs_global_social_twitter_reward_points').parent().parent().show();
                    jQuery('#rs_global_social_twitter_reward_percent').parent().parent().hide();
                } else {
                    jQuery('#rs_global_social_twitter_reward_points').parent().parent().hide();
                    jQuery('#rs_global_social_twitter_reward_percent').parent().parent().show();
                }
                jQuery('#rs_global_social_reward_type_twitter').change(function() {
                    if ((jQuery(this).val()) === '1') {
                        jQuery('#rs_global_social_twitter_reward_points').parent().parent().show();
                        jQuery('#rs_global_social_twitter_reward_percent').parent().parent().hide();
                    } else {
                        jQuery('#rs_global_social_twitter_reward_points').parent().parent().hide();
                        jQuery('#rs_global_social_twitter_reward_percent').parent().parent().show();
                    }
                });
                /*Twitter Reward Type Validation in jQuery Ends*/

                /*Google Reward Type Validation in jQuery Start*/
                if ((jQuery('#rs_global_social_reward_type_google').val()) === '1') {
                    jQuery('#rs_global_social_google_reward_points').parent().parent().show();
                    jQuery('#rs_global_social_google_reward_percent').parent().parent().hide();
                } else {
                    jQuery('#rs_global_social_google_reward_points').parent().parent().hide();
                    jQuery('#rs_global_social_google_reward_percent').parent().parent().show();
                }
                jQuery('#rs_global_social_reward_type_google').change(function() {
                    if ((jQuery(this).val()) === '1') {
                        jQuery('#rs_global_social_google_reward_points').parent().parent().show();
                        jQuery('#rs_global_social_google_reward_percent').parent().parent().hide();
                    } else {
                        jQuery('#rs_global_social_google_reward_points').parent().parent().hide();
                        jQuery('#rs_global_social_google_reward_percent').parent().parent().show();
                    }
                });
                /*Google Reward Type Validation in jQuery Ends*/

                if (jQuery('#product-type').val() === 'variable') {
                    jQuery('._social_rewardsystem_options_facebook_field').css('display', 'none');
                    jQuery('._social_rewardsystem_options_twitter_field').css('display', 'none');
                    jQuery('._social_rewardsystem_options_google_field').css('display', 'none');
                } else {
                    jQuery('._social_rewardsystem_options_facebook_field').css('display', 'block');
                    jQuery('._social_rewardsystem_options_twitter_field').css('display', 'block');
                    jQuery('._social_rewardsystem_options_google_field').css('display', 'block');
                }
                jQuery('#product-type').change(function() {
                    if (jQuery(this).val() === 'variable') {
                        jQuery('._social_rewardsystem_options_facebook_field').css('display', 'none');
                        jQuery('._social_rewardsystem_options_twitter_field').css('display', 'none');
                        jQuery('._social_rewardsystem_options_google_field').css('display', 'none');
                    } else {
                        jQuery('._social_rewardsystem_options_facebook_field').css('display', 'block');
                        jQuery('._social_rewardsystem_options_twitter_field').css('display', 'block');
                        jQuery('._social_rewardsystem_options_google_field').css('display', 'block');
                    }
                });
            });
        </script>
        <?php
    }

    public static function add_fb_style_hide_comment_box() {
        ?>
        <style type="text/css">
            .fb_edge_widget_with_comment span.fb_edge_comment_widget iframe.fb_ltr {
                display: none !important;
            }
            .fb-like{
                height: 20px !important;
                overflow: hidden !important;
            }
            .tipsy-inner {
                background-color:#<?php echo get_option('rs_social_tooltip_bg_color'); ?>;

                color:#<?php echo get_option('rs_social_tooltip_text_color'); ?>;
            }
            .tipsy-arrow-s { border-top-color: #<?php echo get_option('rs_social_tooltip_bg_color'); ?>; }
        </style>
        <?php if (get_option('rs_reward_point_enable_tipsy_social_rewards') == '1') { ?>
        <?php if (get_option('rs_global_show_hide_social_tooltip') == '1') { ?>
            <script type="text/javascript">
                jQuery(function() {
                    //                jQuery('.fb-like').trigger("mouseover");
                    //                jQuery('.twitter-share-button').trigger("mouseover")
                    //                jQuery('#___plusone_0').trigger("mouseover");

                    //jQuery('.twitter-share-button').tipsy({trigger: hover});
                    //jQuery('#___plusone_0').tipsy({trigger: hover});


                    // console.log("Facebook Like Hovered");
                    //jQuery('.fb-like').tipsy({trigger: hover});
            <?php $banned_user_list = get_option('rs_banned-users_list');
            if (!in_array(get_current_user_id(), (array)$banned_user_list)) {
                   $getarrayofuserdata = get_userdata(get_current_user_id());
                $banninguserrole = get_option('rs_banning_user_role');
                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
                ?>
                        jQuery('.fb-like').tipsy({gravity: 's', live: 'true', fallback: '<?php echo do_shortcode(get_option('rs_social_message_for_facebook')); ?>'});
                        jQuery('.twitter-share-button').tipsy({gravity: 's', live: 'true', fallback: '<?php echo do_shortcode(get_option('rs_social_message_for_twitter')); ?>'});
                        jQuery('#___plusone_0').tipsy({gravity: 's', live: 'true', fallback: '<?php echo do_shortcode(get_option('rs_social_message_for_google_plus')); ?>'});
        <?php } }?>
                });
            </script>
            
        <?php }
        }?>
        <?php
    }

//    public static function display_social_message() {
//        echo "<br>";
//        echo "For <strong>Facebook Like</strong> You will Earn <strong>" . do_shortcode('[facebook_like_reward_points]') . '</strong> Points' . "<br>";
//        echo "For <strong>Twitter Tweet</strong> You will Earn <strong>" . do_shortcode('[twitter_tweet_reward_points]') . '</strong> Points' . "<br>";
//        echo "For <strong>Google Share</strong> You will Earn <strong>" . do_shortcode('[google_share_reward_points]') . '</strong> Points' . "<br>";
//    }
}

add_shortcode('google_share_reward_points', array('FPRewardSystemSocialRewardsTab', 'add_shortcode_for_social_google_share'));
add_shortcode('twitter_tweet_reward_points', array('FPRewardSystemSocialRewardsTab', 'add_shortcode_for_social_twitter_tweet'));
add_shortcode('facebook_like_reward_points', array('FPRewardSystemSocialRewardsTab', 'add_shortcode_for_social_facebook_like'));


add_action('wp_head', array('FPRewardSystemSocialRewardsTab', 'add_fb_style_hide_comment_box'));
add_action('admin_head', array('FPRewardSystemSocialRewardsTab', 'social_rewards_global_settings_script'));
add_action('admin_head', array('FPRewardSystemSocialRewardsTab', 'reward_system_social_url'));

add_action('wp_ajax_nopriv_rssocialtwittercallback', array('FPRewardSystemSocialRewardsTab', 'updation_social_twitter_reward_points'));
add_action('wp_ajax_rssocialtwittercallback', array('FPRewardSystemSocialRewardsTab', 'updation_social_twitter_reward_points'));

add_action('wp_ajax_nopriv_rssocialfacebookcallback', array('FPRewardSystemSocialRewardsTab', 'updation_social_facebook_reward_points'));
add_action('wp_ajax_rssocialfacebookcallback', array('FPRewardSystemSocialRewardsTab', 'updation_social_facebook_reward_points'));

add_action('wp_ajax_nopriv_rssocialgooglecallback', array('FPRewardSystemSocialRewardsTab', 'updation_social_google_reward_points'));
add_action('wp_ajax_rssocialgooglecallback', array('FPRewardSystemSocialRewardsTab', 'updation_social_google_reward_points'));

new FPRewardSystemSocialRewardsTab();

/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

if (get_option('rs_global_position_sumo_social_buttons') == '1') {
    add_action('woocommerce_before_single_product', array('FPRewardSystemSocialRewardsTab', 'reward_system_social_likes'));
} elseif (get_option('rs_global_position_sumo_social_buttons') == '2') {
    add_action('woocommerce_before_single_product_summary', array('FPRewardSystemSocialRewardsTab', 'reward_system_social_likes'));
} elseif (get_option('rs_global_position_sumo_social_buttons') == '3') {
    add_action('woocommerce_single_product_summary', array('FPRewardSystemSocialRewardsTab', 'reward_system_social_likes'));
} elseif (get_option('rs_global_position_sumo_social_buttons') == '4') {
    add_action('woocommerce_after_single_product', array('FPRewardSystemSocialRewardsTab', 'reward_system_social_likes'));
} else {
    add_action('woocommerce_after_single_product_summary', array('FPRewardSystemSocialRewardsTab', 'reward_system_social_likes'));
}
//add_action('woocommerce_after_single_product_summary', array('FPRewardSystemSocialRewardsTab', 'display_social_message'));
// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemSocialRewardsTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_socialrewardstab', array('FPRewardSystemSocialRewardsTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemSocialRewardsTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_socialrewardstab', array('FPRewardSystemSocialRewardsTab', 'reward_system_register_admin_settings'));
?>