<?php

class FPRewardSystemStatusTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_statustab'] = __('Status', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        $newcombinedarray = '';
        if (function_exists('wc_get_order_statuses')) {
            $orderstatus = str_replace('wc-', '', array_keys(wc_get_order_statuses()));
            $orderslugs = array_values(wc_get_order_statuses());
            $newcombinedarray = array_combine((array) $orderstatus, (array) $orderslugs);
        } else {
            $taxonomy = 'shop_order_status';
            $orderstatus = '';
            $orderslugs = '';

            $term_args = array(
                'hide_empty' => false,
                'orderby' => 'date',
                    //'order' => 'DESC'
            );
            $tax_terms = get_terms($taxonomy, $term_args);
            foreach ($tax_terms as $getterms) {
                $orderstatus[] = $getterms->name;
                $orderslugs[] = $getterms->slug;
            }
            $newcombinedarray = array_combine($orderslugs, $orderstatus);
        }
        return apply_filters('woocommerce_rewardsystem_status_settings', array(
            array(
                'name' => __('Status Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can set the Status on which Reward Points should be applied', 'rewardsystem'),
                'id' => '_rs_reward_point_status'
            ),
            array(
                'name' => __('Status on which Reward Points for Product Review should be applied', 'rewardsystem'),
                'desc' => __('Here you can set on which Status Reward Points for Product Review should be applied', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_review_reward_status',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'Approve', '2' => 'UnApprove'),
                'newids' => 'rs_review_reward_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward Points awarded when Order Status is', 'rewardsystem'),
                'desc' => __('Here you can set Reward Points should awarded on which Status of Order', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_order_status_control',
                'css' => 'min-width:150px;',
                'std' => 'completed',
                'type' => 'select',
                'options' => $newcombinedarray,
                'newids' => 'rs_order_status_control',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_status'),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'type' => 'title',
//                'desc' => __('Reward Points/Referral Reward Points awarded for the following Status', 'rewardsystem'),
//                'id' => '_rs_reward_point_order_status',
//            ),
//            array(
//                'name' => __('Award Reward Points on Order Status', 'rewardsystem'),
//                'desc' => __('Completed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_completed',
//                'css' => 'min-width:150px;',
//                'std' => 'yes',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_completed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Pending', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_pending',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_pending',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Failed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_failed',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_failed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('On Hold', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_on-hold',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_on-hold',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Processing', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_processing',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_processing',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Refunded', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_refunded',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_refunded',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Cancelled', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_cancelled',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_cancelled',
//                'desc_tip' => false,
//            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_order_status'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemStatusTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemStatusTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemStatusTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function add_script_to_admin_head() {
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'rewardsystem_statustab') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#rs_order_status_control').chosen();
                    });
                </script>
                <?php

            }
        }
    }

}

new FPRewardSystemStatusTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */
add_action('admin_head', array('FPRewardSystemStatusTab', 'add_script_to_admin_head'));
// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemStatusTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_statustab', array('FPRewardSystemStatusTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemStatusTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_statustab', array('FPRewardSystemStatusTab', 'reward_system_register_admin_settings'));
?>
