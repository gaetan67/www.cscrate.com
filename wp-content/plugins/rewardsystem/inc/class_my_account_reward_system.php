<?php

class FPRewardSystemMyAccount {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_myaccount'] = __('My Account', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_my_account_settings', array(
            array(
                'name' => __('My Account Referral Link Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_my_account_referral_link_settings'
            ),
            array(
                'name' => __('[rs_generate_referral referralbutton="show" referraltable="show"] - Use this Shortcode for displaying Referral Link Generation and its Table', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Shortcode Parameters are referralbutton and referraltable, make it as Show/Hide',
                'id' => '_rs_referraltable_shortcode',
            ),
            array(
                'name' => __(''),
                'type' => 'title',
                'desc' => '<h3>[rs_redeem_vouchercode] - Use this Shortcode for displaying the Redeeming Voucher Field <br><br> '
                . '[rs_my_rewards_log] - Use this Shortcode for displaying the log of Reward Points <br><br> [rs_my_reward_points] - Use this Shortcode for displaying the Reward Points of Current User <br></h3>'
            ),
            array(
                'name' => __('Show/Hide Generate Referral Link', 'rewardsystem'),
                'desc' => __('Show/Hide the Generate Referral Link', 'rewardsystem'),
                'id' => 'rs_show_hide_generate_referral',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_generate_referral',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Prefill Generate Referral Link', 'galaxyfunder'),
                'desc' => __('Show/Hide the Prefill Referral Link', 'galaxyfunder'),
                'id' => 'rs_prefill_generate_link',
                'css' => 'min-width:550px',
                'std' => site_url(),
                'type' => 'text',
                'newids' => 'rs_prefill_generate_link',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_my_account_referral_link_settings'),
            array(
                'name' => __('My Account Gift Voucher Redeem Table', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_my_account_voucher_table_settings'
            ),
            array(
                'name' => __('Show/Hide Gift Voucher Field', 'rewardsystem'),
                'desc' => __('Show/Hide the Redeem Voucher Field', 'rewardsystem'),
                'id' => 'rs_show_hide_redeem_voucher',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_redeem_voucher',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Voucher Field Position', 'rewardsystem'),
                'desc' => __('Select the position of Redeem Voucher Field', 'rewardsystem'),
                'id' => 'rs_redeem_voucher_position',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_redeem_voucher_position',
                'type' => 'select',
                'options' => array(
                    '1' => __('Before My Account', 'rewardsystem'),
                    '2' => __('After My Account', 'rewardsystem'),
                ),
            ),
            array('type' => 'sectionend', 'id' => '_rs_my_account_voucher_table_settings'),
            array(
                'name' => __('My Account Reward Table Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_my_account_rewards_table_settings'
            ),
            array(
                'name' => __('Points Log Sorting', 'rewardsystem'),
                'desc' => __('Select the Points Log Sorting Option', 'rewardsystem'),
                'id' => 'rs_points_log_sorting',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_points_log_sorting',
                'type' => 'select',
                'options' => array(
                    '1' => __('Ascending Order', 'rewardsystem'),
                    '2' => __('Descending Order', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide My Rewards Table', 'rewardsystem'),
                'desc' => __('Show/Hide the My Rewards Table', 'rewardsystem'),
                'id' => 'rs_my_reward_table',
                'css' => 'min-width:150px;',
                'std' => '1',
                'desc_tip' => true,
                'default' => '1',
                'newids' => 'rs_my_reward_table',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Search Box in My Rewards Table', 'rewardsystem'),
                'desc' => __('Show/Hide Search Box in My Rewards Table', 'rewardsystem'),
                'id' => 'rs_show_hide_search_box_in_my_rewards_table',
                'css' => 'min-width:150px;',
                'std' => '1',
                'desc_tip' => true,
                'default' => '1',
                'newids' => 'rs_show_hide_search_box_in_my_rewards_table',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'multivendor'),
                    '2' => __('Hide', 'multivendor'),
                ),
            ),
            array(
                'name' => __('Show/Hide Page Size in My Rewards Table', 'rewardsystem'),
                'desc' => __('Show/Hide Page Size in My Rewards Table', 'rewardsystem'),
                'id' => 'rs_show_hide_page_size_my_rewards',
                'css' => 'min-width:150px;',
                'std' => '1',
                'desc_tip' => true,
                'default' => '1',
                'newids' => 'rs_show_hide_page_size_my_rewards',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array('type' => 'sectionend', 'id' => '_rs_my_account_rewards_table_settings'),
            array(
                'name' => __('My Reward Table Label Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_my_reward_label_settings'
            ),
            array(
                'name' => __('My Rewards Label', 'rewardsystem'),
                'desc' => __('Enter the My Rewards Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_title',
                'css' => 'min-width:550px;',
                'std' => 'My Rewards',
                'type' => 'text',
                'newids' => 'rs_my_rewards_title',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Total Points Label', 'rewardsystem'),
                'desc' => __('Enter the Total Points Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_total',
                'css' => 'min-width:550px;',
                'std' => 'Total Points: ',
                'type' => 'text',
                'newids' => 'rs_my_rewards_total',
                'desc_tip' => true,
            ),
            array(
                'name' => __('S.No Label', 'rewardsystem'),
                'desc' => __('Enter the Serial Number Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_sno_label',
                'css' => 'min-width:550px;',
                'std' => 'S.No',
                'type' => 'text',
                'newids' => 'rs_my_rewards_sno_label',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Order ID Label', 'rewardsystem'),
//                'desc' => __('Customize the Order ID Label in the My Rewards Table', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_my_rewards_orderid_label',
//                'css' => 'min-width:550px;',
//                'std' => 'Order ID',
//                'type' => 'text',
//                'newids' => 'rs_my_rewards_orderid_label',
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('User Name Label', 'rewardsystem'),
                'desc' => __('Enter the User Name Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_userid_label',
                'css' => 'min-width:550px;',
                'std' => 'User Name',
                'type' => 'text',
                'newids' => 'rs_my_rewards_userid_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward for Label', 'rewardsystem'),
                'desc' => __('Enter the Reward for Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_rewarder_label',
                'css' => 'min-width:550px;',
                'std' => 'Reward for',
                'type' => 'text',
                'newids' => 'rs_my_rewards_rewarder_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Earned Points Label', 'rewardsystem'),
                'desc' => __('Enter the Earned Points Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_points_earned_label',
                'css' => 'min-width:550px;',
                'std' => 'Earned Points',
                'type' => 'text',
                'newids' => 'rs_my_rewards_points_earned_label',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Before Order Your Points Label', 'rewardsystem'),
//                'desc' => __('Customize the Before Order Your Points Label in the My Rewards Table', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_my_rewards_before_points_label',
//                'css' => 'min-width:550px;',
//                'std' => 'Before Order your Points',
//                'type' => 'text',
//                'newids' => 'rs_my_rewards_before_points_label',
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('Redeemed Points Label', 'rewardsystem'),
                'desc' => __('Enter the Redeemed Points Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_redeem_points_label',
                'css' => 'min-width:550px;',
                'std' => 'Redeemed Points',
                'type' => 'text',
                'newids' => 'rs_my_rewards_redeem_points_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Total Points Label', 'rewardsystem'),
                'desc' => __('Enter the Total Points Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_total_points_label',
                'css' => 'min-width:550px;',
                'std' => 'Total Points',
                'type' => 'text',
                'newids' => 'rs_my_rewards_total_points_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Date Label', 'rewardsystem'),
                'desc' => __('Enter the Date Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_date_label',
                'css' => 'min-width:550px;',
                'std' => 'Date',
                'type' => 'text',
                'newids' => 'rs_my_rewards_date_label',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_my_reward_label_settings'),
            array(
                'name' => __('Extra Class Name for Button', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_myaccount_custom_class_name',
            ),
            array(
                'name' => __('Extra Class Name for Generate Referral Link Button', 'rewardsystem'),
                'desc' => __('Add Extra Class Name to the My Account Generate Referral Link Button, Don\'t Enter dot(.) before Class Name', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_extra_class_name_generate_referral_link',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_extra_class_name_generate_referral_link',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Extra Class Name for Redeem Gift Voucher Button', 'rewardsystem'),
                'desc' => __('Add Extra Class Name to the My Account Redeem Gift Voucher Button, Don\'t Enter dot(.) before Class Name', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_extra_class_name_redeem_gift_voucher_button',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_extra_class_name_redeem_gift_voucher_button',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_myaccount_custom_class_name'),
            array(
                'name' => __('Custom CSS Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Try !important if styles doesn\'t apply ',
                'id' => '_rs_my_reward_custom_css_settings'
            ),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'desc' => __('Enter the Custom CSS for My Account Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_myaccount_custom_css',
                'css' => 'min-width:350px;min-height:350px;',
                'std' => '#generate_referral_field { }  '
                . '#rs_redeem_voucher_code { }  '
                . '#ref_generate_now { } '
                . ' #rs_submit_redeem_voucher { }',
                'type' => 'textarea',
                'newids' => 'rs_myaccount_custom_css',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_my_reward_custom_css_settings'),
//            array(
//                'name' => __('Add to Cart Button Label', 'galaxyfunder'),
//                'desc' => __('Please Enter Add to cart Button Label to show', 'galaxyfunder'),
//                'tip' => '',
//                'id' => 'cf_add_to_cart_label',
//                'css' => 'min-width:550px;',
//                'std' => 'Contribute',
//                'type' => 'text',
//                'newids' => 'cf_add_to_cart_label',
//                'desc_tip' => true,
//            ),
//            array(
//                'name' => __('Redirect after Contribution', 'galaxyfunder'),
//                'desc' => __('Please Select the place you want redirect after Contribution', 'galaxyfunder'),
//                'tip' => '',
//                'id' => 'cf_add_to_cart_redirection',
//                'css' => '',
//                'std' => '1',
//                'type' => 'radio',
//                'options' => array('1' => 'Checkout Page', '2' => 'Cart Page'),
//                'newids' => 'cf_add_to_cart_redirection',
//                'desc_tip' => true,
//            ),
//            array('type' => 'sectionend', 'id' => '_cf_add_to_cart_button'),
//            array(
//                'name' => __('Campaign Out of Stock Settings', 'galaxyfunder'),
//                'type' => 'title',
//                'id' => '_cf_campaign_out_of_stock'
//            ),
//            array(
//                'name' => __('Out of Stock Label', 'galaxyfunder'),
//                'desc' => __('Please Enter Out of Stock Label', 'galaxyfunder'),
//                'tip' => '',
//                'id' => 'cf_outofstock_label',
//                'css' => 'min-width:550px;',
//                'std' => 'Campaign Closed',
//                'type' => 'text',
//                'newids' => 'cf_outofstock_label',
//                'desc_tip' => true,
//            ),
//            array('type' => 'sectionend', 'id' => '_cf_campaign_out_of_stock'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemMyAccount::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemMyAccount::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemMyAccount::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemMyAccount();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemMyAccount', 'reward_system_tab_settings'), 106);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_myaccount', array('FPRewardSystemMyAccount', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemMyAccount', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_myaccount', array('FPRewardSystemMyAccount', 'reward_system_register_admin_settings'));
?>