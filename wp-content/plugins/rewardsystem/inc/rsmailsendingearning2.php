<?php

$emailusers = unserialize($emails->sendmail_to);
foreach ($emailusers as $myuser) {
    $user = get_userdata($myuser);
    $to = $user->user_email;
    $subject = $emails->subject;
    $firstname = $user->user_firstname;
    $lastname = $user->user_lastname;
    $url_to_click = site_url();
    $userpoint = get_user_meta($myuser, '_my_reward_points', true);
    $minimumuserpoints = $emails->minimum_userpoints;
    if ($minimumuserpoints == '') {
        $minimumuserpoints = 0;
    } else {
        $minimumuserpoints = $emails->minimum_userpoints;
    }
    if ($minimumuserpoints < $userpoint) {
        if ($userpoint >= $emails->earningpoints) {
            $message = str_replace('{rssitelink}', $url_to_click, $emails->message);
            $message = str_replace('{rsfirstname}', $firstname, $message);
            $message = str_replace('{rslastname}', $lastname, $message);
            $message = str_replace('{rspoints}', $userpoint, $message);
            $message = do_shortcode($message); //shortcode feature
            ob_start();
            wc_get_template('emails/email-header.php', array('email_heading' => $subject));
            echo $message;
            wc_get_template('emails/email-footer.php');
            $woo_temp_msg = ob_get_clean();
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            if ($emails->sender_opt == 'local') {
                // $headers .= "From: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                $headers .= 'From: ' . $emails->from_name . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                //$headers .= "Reply-To: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
            } else {
                // $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                $headers .= 'From: ' . get_option('woocommerce_email_from_name') . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                //$headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
            }
            if ('2' == get_option('rs_select_mail_function')) {
                if (wp_mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                }
            } elseif ('1' == get_option('rs_select_mail_function')) {
                if (mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                }
            } else {
                if (wp_mail($to, $subject, $woo_temp_msg, $headers = '')) {

                }
            }
        }
    }
}
?>
