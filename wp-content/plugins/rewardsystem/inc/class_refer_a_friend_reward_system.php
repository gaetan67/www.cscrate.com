<?php

class FPRewardSystemReferFriendTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_referfriend'] = __('Refer a Friend', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_referfriend_settings', array(
            array(
                'name' => __('Use this [rs_refer_a_friend] Shortcode anywhere on Page/Post to Display Refer a Friend Form', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_reward_shortcode_status'
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_shortcode_status'),
            array(
                'name' => __('Refer a Friend Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can customize the Label and Placeholders', 'rewardsystem'),
                'id' => '_rs_reward_referfriend_status'
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_status'),
            array(
                'name' => __('Customization Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Refer a Friend Customization Settings', 'rewardsystem'),
                'id' => '_rs_reward_point_customization',
            ),
            array(
                'name' => __('Friend Name Label', 'rewardsystem'),
                'desc' => __('Enter Friend Name Label which will be available in Frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_name_label',
                'css' => 'min-width:550px;',
                'std' => 'Your Friend Name',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_name_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Friend Email Label', 'rewardsystem'),
                'desc' => __('Enter Friend Email Label which will be available in Frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_email_label',
                'css' => 'min-width:550px;',
                'std' => 'Your Friend Email',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_email_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Friend Email Subject', 'rewardsystem'),
                'desc' => __('Enter Friend Subject which will be appear in Frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_subject_label',
                'css' => 'min-width:550px;',
                'std' => 'Your Subject',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_subject_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Friend Email Message', 'rewardsystem'),
                'desc' => __('Enter Friend Email Message which will be appear in frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_message_label',
                'css' => 'min-width:550px;',
                'std' => 'Your Message',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_message_label',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_customization'),
            array(
                'name' => __('Field Placeholder Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can customize the field Placeholder which will be appear in frontend when you use shortcode', 'rewardsystem'),
                'id' => '_rs_reward_field_placeholder_settings'
            ),
            array(
                'name' => __('Friend Name Field Placeholder', 'rewardsystem'),
                'desc' => __('Enter Friend Name Field Placeholder which will be appear in frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_name_placeholder',
                'css' => 'min-width:550px;',
                'std' => 'Enter your Friend Name',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_name_placeholder',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Friend Email Field Placeholder', 'rewardsystem'),
                'desc' => __('Enter Friend Email Field Placeholder which will be appear in frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_email_placeholder',
                'css' => 'min-width:550px;',
                'std' => 'Enter your Friend Email',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_email_placeholder',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Friend Email Subject Field Placeholder', 'rewardsystem'),
                'desc' => __('Enter Friend Email Subject Field Placeholder which will be appear in frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_email_subject_placeholder',
                'css' => 'min-width:550px;',
                'std' => 'Enter your Subject',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_email_subject_placeholder',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Friend Email Message Field Placeholder', 'rewardsystem'),
                'desc' => __('Enter Friend Email Message Field Placeholder which will be appear in frontend when you use shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_email_message_placeholder',
                'css' => 'min-width:550px;',
                'std' => 'Enter your Message',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_email_message_placeholder',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_field_placeholder_settings'),
            array(
                'name' => __('Error Message Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can customize the Label and Placeholders', 'rewardsystem'),
                'id' => '_rs_reward_referfriend_error_settings'
            ),
            array(
                'name' => __('Error Message if Friend Name is Empty', 'rewardsystem'),
                'desc' => __('Enter your Error Message which will be appear in frontend if the Friend Name is Empty'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_name_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter your Friend Name',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_name_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message if Friend Email is Empty', 'rewardsystem'),
                'desc' => __('Enter your Error Message which will be appear in frontend if the Friend Email is Empty'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_email_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter your Friend Email',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_email_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message if Friend Email is not Valid', 'rewardsystem'),
                'desc' => __('Enter your Error Message which will be appear in frontend if the Friend Email is not Valid'),
                'tip' => '',
                'id' => 'rs_my_rewards_friend_email_is_not_valid',
                'css' => 'min-width:550px;',
                'std' => 'Enter Email is not Valid',
                'type' => 'text',
                'newids' => 'rs_my_rewards_friend_email_is_not_valid',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message if Email Subject is Empty', 'rewardsystem'),
                'desc' => __('Enter your Error Message which will be appear in frontend if the Email Subject is Empty'),
                'tip' => '',
                'id' => 'rs_my_rewards_email_subject_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Email Subject should not be left blank',
                'type' => 'text',
                'newids' => 'rs_my_rewards_email_subject_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message if Email Message is Empty', 'rewardsystem'),
                'desc' => __('Enter your Error Message which will be appear in frontend if the Email Message is Empty'),
                'tip' => '',
                'id' => 'rs_my_rewards_email_message_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter your Message',
                'type' => 'text',
                'newids' => 'rs_my_rewards_email_message_error_message',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_referfriend_error_settings'),
            array(
                'name' => __('Custom CSS Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Try !important if styles doesn\'t apply ',
                'id' => '_rs_refer_a_friend_custom_css_settings'
            ),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'desc' => __('Enter the Custom CSS which will be applied on top of Refer a Friend Shortcode', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_refer_a_friend_custom_css',
                'css' => 'min-width:350px;min-height:350px;',
                'std' => '#rs_refer_a_friend_form { } #rs_friend_name { } #rs_friend_email { } #rs_friend_subject { } #rs_your_message { } #rs_refer_submit { }',
                'type' => 'textarea',
                'newids' => 'rs_refer_a_friend_custom_css',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_refer_a_friend_custom_css_settings'),
//            array(
//                'name' => __('Award Reward Points on Order Status', 'rewardsystem'),
//                'desc' => __('Completed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_completed',
//                'css' => 'min-width:150px;',
//                'std' => 'yes',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_completed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Pending', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_pending',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_pending',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Failed', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_failed',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_failed',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('On Hold', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_on-hold',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_on-hold',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Processing', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_processing',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_processing',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Refunded', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_refunded',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_refunded',
//                'desc_tip' => false,
//            ),
//            array(
//                'name' => __('', 'rewardsystem'),
//                'desc' => __('Cancelled', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_award_points_on_order_status_cancelled',
//                'css' => 'min-width:150px;',
//                'std' => 'no',
//                'type' => 'checkbox',
//                'newids' => 'rs_award_points_on_order_status_cancelled',
//                'desc_tip' => false,
//            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_order_status'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemReferFriendTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemReferFriendTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemReferFriendTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function reward_system_refer_a_friend_shortcode($content) {
        ob_start();
        ?>
        <style type="text/css">
            <?php echo get_option('rs_refer_a_friend_custom_css'); ?>;
        </style>
        <?php
        if (is_user_logged_in()) {
            ?>
            <form id="rs_refer_a_friend_form" method="post">
                <table class="shop_table my_account_referrals">
                    <tr>
                        <td><h3><?php echo get_option('rs_my_rewards_friend_name_label'); ?></h3></td>
                        <td><input type="text" name="rs_friend_name" placeholder ="<?php echo get_option('rs_my_rewards_friend_name_placeholder'); ?>" id="rs_friend_name" value=""/>
                            <br>
                            <div class="rs_notification"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><h3><?php echo get_option('rs_my_rewards_friend_email_label'); ?></h3></td>
                        <td><input type="text" name="rs_friend_email" placeholder="<?php echo get_option('rs_my_rewards_friend_email_placeholder'); ?>" id="rs_friend_email" value=""/>
                            <br>
                            <div class="rs_notification"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><h3><?php echo get_option('rs_my_rewards_friend_subject_label'); ?></h3></td>
                        <td><input type="text" name="rs_friend_subject" id="rs_friend_subject" placeholder ="<?php echo get_option('rs_my_rewards_friend_email_subject_placeholder'); ?>" value=""/>
                            <br>
                            <div class="rs_notification"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><h3><?php echo get_option('rs_my_rewards_friend_message_label'); ?></h3></td>
                        <td><textarea rows="5" cols="35" id="rs_your_message" placeholder ="<?php echo get_option('rs_my_rewards_friend_email_message_placeholder'); ?>" name="rs_your_message"></textarea>
                            <br>
                            <div class="rs_notification"></div>
                        </td>
                    </tr>
                </table>
                <div class="rs_notification_final"></div>
                <input type="submit" class="button-primary" name="submit" id="rs_refer_submit" value="Send Mail"/>
            </form>
            <script type="text/javascript">
                function checkemail(email) {
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    return regex.test(email);
                }
                jQuery(document).ready(function() {
                    jQuery('#rs_refer_submit').click(function() {
                        var firstname = jQuery('#rs_friend_name').val();
                        var friendemail = jQuery('#rs_friend_email').val();
                        var friendmessage = jQuery('#rs_your_message').val();
                        var friendsubject = jQuery('#rs_friend_subject').val();

                        /* Validation for Friend Name, Email and Message should be not empty */
                        if (firstname === '') {
                            jQuery('#rs_friend_name').css('border', '2px solid red');
                            jQuery('#rs_friend_name').parent().find('.rs_notification').css('color', 'red');
                            jQuery('#rs_friend_name').parent().find('.rs_notification').html('<p><?php echo get_option('rs_my_rewards_friend_name_error_message'); ?> </p>');
                            return false;
                        } else {
                            jQuery('#rs_friend_name').css('border', '');
                            jQuery('#rs_friend_name').parent().find('.rs_notification').html('');
                        }
                        if (friendemail === '') {
                            jQuery('#rs_friend_email').css('border', '2px solid red');
                            jQuery('#rs_friend_email').parent().find('.rs_notification').css('color', 'red');
                            jQuery('#rs_friend_email').parent().find('.rs_notification').html('<p><?php echo get_option('rs_my_rewards_friend_email_error_message'); ?></p>');
                            return false;
                        } else {
                            jQuery('#rs_friend_email').css('border', '');
                            jQuery('#rs_friend_email').parent().find('.rs_notification').html('');
                        }

                        //check email is valid or not
                        if (!checkemail(friendemail)) {
                            jQuery('#rs_friend_email').css('border', '2px solid red');
                            jQuery('#rs_friend_email').parent().find('.rs_notification').css('color', 'red');
                            jQuery('#rs_friend_email').parent().find('.rs_notification').html('<p><?php echo get_option('rs_my_rewards_friend_email_is_not_valid'); ?> </p>');
                            return false;
                        } else {
                            jQuery('#rs_friend_email').css('border', '');
                            jQuery('#rs_friend_email').parent().find('.rs_notification').html('');
                        }


                        if (friendsubject === '') {
                            jQuery('#rs_friend_subject').css('border', '2px solid red');
                            jQuery('#rs_friend_subject').parent().find('.rs_notification').css('color', 'red');
                            jQuery('#rs_friend_subject').parent().find('.rs_notification').html('<p><?php echo get_option('rs_my_rewards_email_subject_error_message'); ?></p>');
                            return false;
                        } else {
                            jQuery('#rs_friend_subject').css('border', '');
                            jQuery('#rs_friend_subject').parent().find('.rs_notification').html('');
                        }
                        //Email Subject Field Should not be empty


                        //  Message should not be empty
                        if (friendmessage === '') {
                            jQuery('#rs_your_message').css('border', '2px solid red');
                            jQuery('#rs_your_message').parent().find('.rs_notification').css('color', 'red');
                            jQuery('#rs_your_message').parent().find('.rs_notification').html('<p><?php echo get_option('rs_my_rewards_email_message_error_message'); ?></p>');
                            return false;
                        } else {
                            jQuery('#rs_your_message').css('border', '');
                            jQuery('#rs_your_message').parent().find('.rs_notification').html('');
                        }

                        var dataparam = ({
                            action: 'rs_refer_a_friend_ajax',
                            friendname: firstname,
                            friendemail: friendemail,
                            friendsubject: friendsubject,
                            friendmessage: friendmessage
                        });
                        jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                function(response) {
                                    jQuery('.rs_notification_final').css('color', 'green');
                                    //jQuery('#rs_refer_a_friend_form').reset();
                                    document.getElementById("rs_refer_a_friend_form").reset();
                                    jQuery('.rs_notification_final').html('<p><?php _e('Mail Sent Successfully', 'rewardsystem'); ?></p>');
                                });
                        return false;

                    });
                });
            </script>

            <?php
        } else {
            $myaccountlink = get_permalink(get_option('woocommerce_myaccount_page_id'));
            $myaccounttitle = get_the_title(get_option('woocommerce_myaccount_page_id'));
            echo 'Please Login to View this Page <a href=' . $myaccountlink . '> Login </a>';
        }
        $content = ob_get_clean();
        return $content;
    }

    public static function reward_system_process_ajax_request() {
        if (isset($_POST)) {
            if (isset($_POST['friendname'])) {
                $friendname = $_POST['friendname'];
            }
            if (isset($_POST['friendemail'])) {
                $friendemail = $_POST['friendemail'];
            }
            if (isset($_POST['friendsubject'])) {
                $friendsubject = $_POST['friendsubject'];
            }
            if (isset($_POST['friendmessage'])) {
                $friendmessage = __('Hi ', 'rewardsystem') . $_POST['friendname'] . '<br>';
                $friendmessage .= $_POST['friendmessage'];
            }
            ob_start();
            wc_get_template('emails/email-header.php', array('email_heading' => $friendsubject));
            echo $friendmessage;
            wc_get_template('emails/email-footer.php');
            $woo_rs_msg = ob_get_clean();
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= 'From: ' . get_option("blogname") . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
            //$headers .= "Reply-To: " . $friendname . " <" . $friendemail . ">\r\n";
            //mail($to, 'User Registration', $friendmessage, $headers);
            if (get_option('rs_select_mail_function') == '1') {
                mail($friendemail, $friendsubject, $woo_rs_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME']);
            } else {
                wp_mail($friendemail, $friendsubject, $woo_rs_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME']);
            }
            error_reporting(E_ALL);
            ini_set('display_errors', '1');
        }
        exit();
    }

}

new FPRewardSystemReferFriendTab();

add_action('wp_ajax_nopriv_rs_refer_a_friend_ajax', array('FPRewardSystemReferFriendTab', 'reward_system_process_ajax_request'));
add_action('wp_ajax_rs_refer_a_friend_ajax', array('FPRewardSystemReferFriendTab', 'reward_system_process_ajax_request'));

/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemReferFriendTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_referfriend', array('FPRewardSystemReferFriendTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemReferFriendTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_referfriend', array('FPRewardSystemReferFriendTab', 'reward_system_register_admin_settings'));

//Register a Shortcode which was in admin settings
add_shortcode('rs_refer_a_friend', array('FPRewardSystemReferFriendTab', 'reward_system_refer_a_friend_shortcode'));
?>