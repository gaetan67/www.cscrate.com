<?php

class FPRewardSystemCart {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_cart'] = __('Cart', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_my_account_settings', array(
            array(
                'name' => __('Cart Redeem Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_redeem_settings'
            ),
            array(
                'name' => __('Apply Redeeming Before Tax', 'rewardsystem'),
                'desc' => __('Enable this box if the coupon should be applied before calculating cart tax.', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_apply_redeem_before_tax',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_apply_redeem_before_tax',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Free Shipping ', 'rewardsystem'),
                'desc' => __('Enable this box if shipping should be made free ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_apply_shipping_tax',
                'css' => 'min-width:150px;',
                'std' => '2',
                'type' => 'select',
                'newids' => 'rs_apply_shipping_tax',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Redeeming Field', 'rewardsystem'),
                'desc' => __('Show/Hide Redeeming Field in a Cart Page of WooCommerce', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_show_hide_redeem_field',
                'css' => '',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_show_hide_redeem_field',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Points to Earn for Redeeming First Time', 'rewardsystem'),
                'desc' => __('Enter Minimum Points to Earn for Redeeming First Time in Cart/Checkout', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_first_time_minimum_user_points',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_first_time_minimum_user_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Balance Points for Redeeming', 'rewardsystem'),
                'desc' => __('Enter Minimum Balance Points for Redeeming in Cart/Checkout', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_user_points_to_redeem',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_minimum_user_points_to_redeem',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Points for Redeeming', 'rewardsystem'),
                'desc' => __('Enter Minimum Points for Redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_redeeming_points',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_minimum_redeeming_points',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Redeeming Point Apply Option', 'rewardsystem'),
//                'desc' => __('Select the Redeeming Point Apply Option through Apply Button or Slider Control', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_redeem_point_apply_option',
//                'css' => '',
//                'std' => '1',
//                'type' => 'select',
//                'options' => array('1' => __('Apply Button', 'rewardsystem'), '2' => __('Slider Control', 'rewardsystem')),
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('Redeem Field Caption', 'rewardsystem'),
                'desc' => __('Enter the Label which will be displayed in Redeem Field', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_caption',
                'css' => 'min-width:550px;',
                'std' => 'Redeem your Reward Points:',
                'type' => 'text',
                'newids' => 'rs_redeem_field_caption',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeem Field Caption Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Redeem Field Caption', 'rewardsystem'),
                'id' => 'rs_show_hide_redeem_caption',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_redeem_caption',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Redeem Field Placeholder', 'rewardsystem'),
                'desc' => __('Enter the Placeholder which will be displayed in Redeem Field', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_placeholder',
                'css' => 'min-width:550px;',
                'std' => 'Reward Points to Enter',
                'type' => 'text',
                'newids' => 'rs_redeem_field_placeholder',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeem Field Placeholder Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Redeem Field Placeholder', 'rewardsystem'),
                'id' => 'rs_show_hide_redeem_placeholder',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_redeem_placeholder',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Redeem Field Submit Button Caption', 'rewardsystem'),
                'desc' => __('Enter the Label which will be displayed in Submit Button', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_submit_button_caption',
                'css' => 'min-width:550px;margin-bottom:40px;',
                'std' => 'Apply Reward Points',
                'type' => 'text',
                'newids' => 'rs_redeem_field_submit_button_caption',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_redeem_settings'),
            array(
                'name' => __('Cart Redeem Error Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_redeem_error_settings'
            ),
            array(
                'name' => __('Empty Redeem Point Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when Redeem Field has Empty Value', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_empty_error_message',
                'css' => 'min-width:550px;',
                'std' => 'No Reward Points Entered',
                'type' => 'text',
                'newids' => 'rs_redeem_empty_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Maximum Redeem Point Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when Entered Reward Points is more than Earned Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_max_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Reward Points you entered is more than Your Earned Reward Points ',
                'type' => 'text',
                'newids' => 'rs_redeem_max_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Redeem Point Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when Entered Points is less than Minimum Redeeming Points which is set in this Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_redeem_point_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter Points more than [rsminimumpoints]',
                'type' => 'text',
                'newids' => 'rs_minimum_redeem_point_error_message',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_redeem_error_settings'),
            array(
                'name' => __('Coupon Label Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_redeem_error_settings'
            ),
            array(
                'name' => __('Coupon Label Settings', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed in Cart Subtotal', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_coupon_label_message',
                'css' => 'min-width:550px;',
                'std' => 'Redeem Points Value',
                'type' => 'text',
                'newids' => 'rs_coupon_label_message',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_redeem_error_settings'),
            array(
                'name' => __('Extra Class Name for Button', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_custom_class_name',
            ),
            array(
                'name' => __('Extra Class Name for Cart Apply Reward Points Button', 'rewardsystem'),
                'desc' => __('Add Extra Class Name to the Cart Apply Reward Points Button, Don\'t Enter dot(.) before Class Name', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_extra_class_name_apply_reward_points',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_extra_class_name_apply_reward_points',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_custom_class_name'),
            array(
                'name' => __('Custom CSS Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Try !important if styles doesn\'t apply ',
                'id' => '_rs_cart_custom_css_settings',
            ),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'desc' => __('Enter the Custom CSS for the Cart Page ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_cart_page_custom_css',
                'css' => 'min-width:350px; min-height:350px;',
                'std' => '#rs_apply_coupon_code_field { } #mainsubmi { } .fp_apply_reward{ }',
                'type' => 'textarea',
                'newids' => 'rs_cart_page_custom_css',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_custom_css_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemCart::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemCart::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemCart::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemCart();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemCart', 'reward_system_tab_settings'), 106);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_cart', array('FPRewardSystemCart', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemCart', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_cart', array('FPRewardSystemCart', 'reward_system_register_admin_settings'));
?>