<?php

/*
 * Checkout Tab Settings
 */

class FPRewardSystemCheckoutTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_checkouttab'] = __('Checkout', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_checkout_settings', array(
            array(
                'name' => __('Checkout Settings Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the Checkout Customization Options', 'rewardsystem'),
                'id' => '_rs_reward_point_checkout_settings'
            ),
            array(
                'name' => __('Show/Hide Redeeming Field in Checkout Page', 'rewardsystem'),
                'desc' => __('Show/Hide Redeeming Field in Checkout Page of WooCommerce', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_show_hide_redeem_field_checkout',
                'css' => '',
                'std' => '2',
                'type' => 'select',
                'newids' => 'rs_show_hide_redeem_field_checkout',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_checkout_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemCheckoutTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemCheckoutTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemCheckoutTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemCheckoutTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemCheckoutTab', 'reward_system_tab_settings'), 106);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_checkouttab', array('FPRewardSystemCheckoutTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemCheckoutTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_checkouttab', array('FPRewardSystemCheckoutTab', 'reward_system_register_admin_settings'));
