<?php

class FPRewardSystemImportExportTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_import_export_csv'] = __('Import/Export Points in CSV', 'rewardsystem');
        return $settings_tabs;
    }

// Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_import_export_csv_settings', array(
            array(
                'name' => __('Import/Export User Points in CSV Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can Import/Export Users and their Points in CSV', 'rewardsystem'),
                'id' => '_rs_reward_system_import_export_csv'
            ),
            array(
                'type' => 'import_export',
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_system_import_export_csv'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemImportExportTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemImportExportTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemImportExportTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function outputCSV($data) {
        $output = fopen("php://output", "w");
        foreach ($data as $row) {
            fputcsv($output, $row); // here you can change delimiter/enclosure
        }
        fclose($output);
    }

    public static function inputCSV($data_path) {
        $row = 1;
        if (($handle = fopen($data_path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//            $num = count($data);
                $row++;
                $collection[] = array_filter(array($data[0], $data[1]));
            }
            update_option('rewardsystem_csv_array', array_merge(array_filter($collection)));
            fclose($handle);
        }
    }

    function wps_wp_admin_area_notice() {
        echo ' <div class="error">
                 <p>We are performing website Maintenance. Please dont do any activity until further notice!</p>
          </div>';
    }

    public static function reward_system_page_customization() {
        foreach (get_users() as $users) {
            $arraylist[] = array($users->user_login, get_user_meta($users->ID, '_my_reward_points', true));
        }
// var_dump(get_option('testing_csv_array'));
        if (isset($_POST['rs_import_user_points'])) {
            //  echo "google";
            //    var_dump($_FILES);
            if ($_FILES["file"]["error"] > 0) {
                echo "Error: " . $_FILES["file"]["error"] . "<br>";
            } else {
                $mimes = array('text/csv',
                    'text/plain',
                    'application/csv',
                    'text/comma-separated-values',
                    'application/excel',
                    'application/vnd.ms-excel',
                    'application/vnd.msexcel',
                    'text/anytext',
                    'application/octet-stream',
                    'application/txt');
                if (in_array($_FILES['file']['type'], $mimes)) {
                    // do something
                    FPRewardSystemImportExportTab::inputCSV($_FILES["file"]["tmp_name"]);
                } else {
                    ?>
                    <style type="text/css">
                        div.error {
                            display:block;
                        }
                    </style>
                    <?php
//  add_action('admin_notices', array('FPRewardSystemImportExportTab', 'wps_wp_admin_area_notice'));
                }
            }
            $myurl = get_permalink();
            //  header("Location: $myurl");
        }
        if (isset($_POST['rs_export_user_points_csv'])) {
//var_dump($_POST);
            ob_end_clean();
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=reward_points_" . date("Y-m-d") . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            FPRewardSystemImportExportTab::outputCSV($arraylist);
            exit();
        }

//
//        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//            /// do your magic
//            session_start();
//            $_SESSION['error'] = "Thanks for your message!";
//
//            // this should be the full URL per spec, but "/yourscript.php" will work
//            $myurl = get_permalink();
//
//            header("Location: $myurl");
//            header("HTTP/1.1 303 See Other");
//            die("redirecting");
//        }
//
//        if (isset($_SESSION['error'])) {
//            print "The result of your submission: " . $_SESSION['error'];
//            unset($_SESSION['error']);
//        }
        ?>


        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>

        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_import_user_points_csv"><?php _e('Import User Points to CSV', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="file" id="rs_import_user_points_csv" name="file" />
                <input type="submit" id="rs_import_user_points" name="rs_import_user_points" value="Upload"/>
            </td>

        </tr>
        <tr valign ="top">
            <th class="titledesc" scope="row">
                <label for="rs_export_user_points_csv"><?php _e('Export User Points to CSV', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="submit" id="rs_export_user_points_csv" name="rs_export_user_points_csv" value="Export User Points"/>
            </td>
        </tr>
        <?php if (get_option('rewardsystem_csv_array') != '') { ?>
            <table class="wp-list-table widefat fixed posts">
                <thead>
                    <tr>
                        <th>
                            User Name
                        </th>
                        <th>
                            User Reward Points
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach (get_option('rewardsystem_csv_array')as $newcsv) {
                        ?>
                        <tr>
                            <td><?php echo $newcsv[0]; ?></td><td><?php echo $newcsv[1]; ?></td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>

            <table>
                <tr valign="top">
                    <td>
                        <input type="submit" id="rs_new_action_reward_points" name="rs_new_action_reward_points" value="Override Existing User Points"/>
                    </td>
                    <td>
                        <input type="submit" id="rs_exist_action_reward_points" name="rs_exist_action_reward_points" value="Add Points with Already Earned Points"/>
                    </td>
                </tr>
            </table>
            <?php
        }

        if (isset($_POST['rs_new_action_reward_points'])) {
            $getvalues = get_option('rewardsystem_csv_array');
            if (is_array($getvalues)) {
                foreach ($getvalues as $newvalues) {
                    $datalogin = get_user_by('login', $newvalues[0]);
                    if (!empty($datalogin)) {
                        update_user_meta($datalogin->ID, '_my_reward_points', $newvalues[1]);
                    }
                }
            }
            delete_option('rewardsystem_csv_array');
            $redirect = add_query_arg(array('saved' => 'true'));
            wp_safe_redirect($redirect);
            exit();
        }

        if (isset($_POST['rs_exist_action_reward_points'])) {
            $getvalues = get_option('rewardsystem_csv_array');
            if (is_array($getvalues)) {
                foreach ($getvalues as $newvalues) {
                    $datalogin = get_user_by('login', $newvalues[0]);
                    if (!empty($datalogin)) {
                        // echo "Data Found <br>";
                        //echo $newvalues[0] . " has been found with user id " . $datalogin->ID . " has going to earn points of $newvalues[2]<br>";
                        $oldpoints = get_user_meta($datalogin->ID, '_my_reward_points', true);
                        $currentpoints = $oldpoints + $newvalues[1];
                        update_user_meta($datalogin->ID, '_my_reward_points', $currentpoints);
                    }
                    //var_dump($datalogin);
                }
            }
            delete_option('rewardsystem_csv_array');
            $myurl = get_permalink();
            //  header("Location: $myurl");
            $redirect = add_query_arg(array('saved' => 'true'));
            wp_safe_redirect($redirect);
            exit();
        }
    }

}

new FPRewardSystemImportExportTab();


add_action('woocommerce_admin_field_import_export', array('FPRewardSystemImportExportTab', 'reward_system_page_customization'));
/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

//add_action('admin_head', array('FPRewardSystemImportExportTab', 'reward_system_page_customization'));
//
// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemImportExportTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_import_export_csv', array('FPRewardSystemImportExportTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemImportExportTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_import_export_csv', array('FPRewardSystemImportExportTab', 'reward_system_register_admin_settings'));
?>