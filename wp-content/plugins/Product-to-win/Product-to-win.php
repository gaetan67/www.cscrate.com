<?php
/**
* Plugin Name: Prouct-to-win
* Plugin URI:
* Description: Création de produits items pour le tirage au sort.
* Version: 1.0 
* Author: Toussaint NAMA
* Author URI: 
* License: GPL12
*/

add_action( 'admin_menu', 'my_admin_menu' );

function my_admin_menu() {
	add_menu_page( 'Products to win', 'Products to win', 'manage_options', 'Product-to-win/Prouct-to-win.php', 'Product_to_win', 'dashicons-tickets', 6  );
}
include($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/Product-to-win/Classes/PRODUCT_TO_WIN.class.php');

add_action( 'init', 'crate_post_type' );
function crate_post_type() {
  register_post_type( 'crate',
    array(
      'labels' => array(
        'name' => __( 'Crate' ),
        'singular_name' => __( 'Crate' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
    )
  );
}

function crate_taxonomy() {
	// create a new taxonomy
	register_taxonomy(
		'category_crate',
		'crate',
		array(
			'label' => __( 'Crate Categories' ),
			'rewrite' => array( 'slug' => 'crate_category' ),
		)
	);
}
add_action( 'init', 'crate_taxonomy' );


function Product_to_win()
{ 



if (isset($_POST['create_post'])) {	

	for ($i=0; $i < $_POST['nbr_col']; $i++) {
		if (!empty( $_POST['name_product'][$i] ) ){
			PRODUCT_TO_WIN::create_item($_POST['name_product'][$i],$_POST['categorie'][$i],$_POST['position'][$i],$_POST['image_prod'][$i], $_POST['price'][$i]);
		}
		
	}
}

if (isset($_POST['delete_posit'])) {
	if (!empty($_POST['delete_prod'])) {
		foreach ($_POST['delete_prod'] as $delete_prod) {
			PRODUCT_TO_WIN::delete_post_position($delete_prod);
		}
	}
}

//recevoir tous les utilisateurs
$users = PRODUCT_TO_WIN::get_number_user_by_groups();
if ($users) {
	$total_item = ($users['gold']*3) + ($users['silver']*2) + $users['bronze'];
}

// recevoir toutes les Categories
$categories_crate = PRODUCT_TO_WIN::get_all_categories();

// recevoir les posts
$crate_post = PRODUCT_TO_WIN::get_all_crate();

// Recevoir le nombre de post
$nbr_post = PRODUCT_TO_WIN::get_all_crate()->post_count;
$id_of_post = array();
$name_of_post = array();


// avoir toutes les catégories
$terms = PRODUCT_TO_WIN::get_all_categories();

$date_array = array('2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026','2027','2028');
$mois_array = array('January', 'February', 'March','April','May','June','July','August','September','October','November','December');

while ( $crate_post->have_posts() ) {
	$crate_post->the_post();
	array_push($id_of_post, get_the_ID());
	$name_of_post[get_the_ID()] =  get_the_title();
}


if (isset($_POST['select_winner'])) {
	// Melange des items dans le tableau
	shuffle($id_of_post);
	// ENREGISTREMENT DES WINNERS GOLD
	if ($users['gold'] > 0) {
		PRODUCT_TO_WIN::save_winner_gold($id_of_post,$users['gold'],$users['gold_id']);
	}
	// ENREGISTREMENT DES WINNERS SILVER
	if ($users['silver'] > 0) {
		PRODUCT_TO_WIN::save_winner_silver($id_of_post,$users['gold'],$users['silver'],$users['silver_id']);
	}
	// ENREGISTREMENT DES WINNERS BRONZE
	if ($users['bronze'] > 0) {
		PRODUCT_TO_WIN::save_winner_bronze($id_of_post,$users['gold'],$users['silver'],$users['bronze'],$users['bronze_id']);
	}
}	

if (isset($_POST['create_excels'])) {
	PRODUCT_TO_WIN::create_excels_file();
}
?>

<style type="text/css" media="screen">
	.field_items{
		display: inline-block;
		min-width: 19%;
	}
	input#create_post{
		display: block;
	    margin: 30px auto;
	    width: 200px;
	    height: 50px;
	    background-color: black;
	    color: white;
	    border: 0px;
	}
	li#entete_fields_items, li#entete_winners{
		min-height: 60px;
	    line-height: 25px;
	    background-color: black;
	    color: white;
	    font-size: 60px;
	    padding: 0 20px;
        margin-bottom: 40px;
	}
	li.item_product{
		background: #b4b9be;
		min-height: 40px;
		padding: 0 10px;
		padding-top: 11px;
		margin-bottom: 5px;
	}
	.field_items > span{
		background-color: white;
	    display: inline-block;
	    width: 20px;
	    height: 20px;
	    text-align: center;
	    border-radius: 46%;
	    font-weight: bold;
	    margin-right: 10px;
	}
	div.content_users_infos{
		text-align: center;
	}
	input#select_winner,input#create_excels{
		display: block;
		margin: 30px auto;
		width: 200px;
		height: 50px;
		background-color: black;
		color: white;
		border: 0px;
		text-transform: uppercase;
	}
	input:disabled{
		background-color: rgba(0, 0, 0, 0.19) !important;
	}
	div#product_of_month{
		margin-top: 80px;
	}
	.winners_blk {
        display: inline-block;
        min-width: 45%;
        padding-left: 2%;
    }
    .winners_blk:last-child{
		border-left: 1px solid;
    }
    .winners_blk span{
    	display: inline-block;
    	margin-right: 20px;
    }
    .type_user_title{
    	text-align: center;
    	text-transform: uppercase;
    }
    li#entete_winners .winners_blk{
    	border-left: 0px;
    	padding-left: 1%;
    }
    li.winner_item{
    	border: 2px solid;
    }
    .type_user_title.gold_li{
    	background-color: gold;
	    padding: 10px;
    }
    
    .type_user_title.silver_li{
    	background-color: silver;
	    padding: 10px;
	    margin-top: 80px;
    }
    .type_user_title.bronze_li{
    	background-color: #cd7f32;
	    padding: 10px;
	    margin-top: 80px;
    }
</style>
<script type="text/javascript">

	jQuery(document).ready(function($){

		$('#confirm_select').click(function(event) {
			if ($(this).is(':checked')) {
				$('#select_winner').prop( "disabled", false );
			}else{
				$('#select_winner').prop( "disabled", true );
			}
		});
			
			if (typeof window.upload_img === 'undefined') {
			    window.upload_img = function($id) {
					var mediaUploader;
						// $this = $(this);
					  event.preventDefault();
					  // If the uploader object has already been created, reopen the dialog
					    if (mediaUploader) {
					    mediaUploader.open();
					    return;
					  }
					  // Extend the wp.media object
					  mediaUploader = wp.media.frames.file_frame = wp.media({
					    title: 'Choose Image',
					    button: {
					    text: 'Choose Image'
					  }, multiple: false });

					  // When a file is selected, grab the URL and set it as the text field's value
					  mediaUploader.on('select', function() {
					    attachment = mediaUploader.state().get('selection').first().toJSON();
					    // console.log(attachment);
					    $('.image-url_'+$id).val(attachment.id);
					  });
					  // Open the uploader dialog
					  mediaUploader.open();
				}
			}
	  		  var nbr_element = '<?php echo $total_item-$nbr_post; ?>';

	  		  $('#add_product_link').click(function(e) {
		  		    event.preventDefault();
		  		    nbr_element++;
		  		    $element = '<li class="item_product li_product_'+nbr_element+'"><div class="field_items"><span>'+nbr_element+'</span><input type="text" name="name_product[]" value="" placeholder=""></div><div class="field_items"><input type="text" name="categorie[]" value="" placeholder=""></div><div class="field_items"><input type="number" name="price[]" value="" placeholder=""></div><div class="field_items"><input type="number" name="position[]" value="" placeholder=""></div><div class="field_items"><input class="image-url_b_'+nbr_element+'" type="text" name="image_prod[]" /><input id="b_'+nbr_element+'"  type="button" class="button upload-button" value="Upload Image" onclick="upload_img(this.id)" /></div></li>';
		  		    $('ul#liste_de_produit').append($element);
		  		    $('#nbr_col').val(nbr_element);
		  		     
	  		  });

	  		  $('#remove_product_link').click(function(e) {
		  		    event.preventDefault();
		  		     $('li.li_product_'+nbr_element).remove();
		  		     $element = '<li class="item_product li_product_'+nbr_element+'"></li>';
		  		     if (nbr_element > <?php echo $total_item-$nbr_post; ?>) {
		  		     	nbr_element--;
		  		     }
		  		     $('#nbr_col').val(nbr_element);

	  		  });
	});
</script>

<div id='infos_user'>
	<div class="content_users_infos">
		<h1>For the month of <?php echo date('F Y') ?></h1>
		<h4>You have a total of <?php echo $users['user']; ?> users</h4>
		<p><?php echo $users['gold']; ?> GOLD users</p>
		<p><?php echo $users['silver']; ?> SILVER users</p>
		<p><?php echo $users['bronze']; ?> BRONZE users</p>
		<?php
			if ($nbr_post > 0 && $nbr_post < $total_item) { ?>
				<h2>You have already create <?php echo $nbr_post;  ?> crates to win, You must create <?php echo $total_item-$nbr_post;  ?>  more for this month</h2>
		<?php	}elseif($nbr_post == $total_item ){ ?>
				<h2> All crates to win this month were created</h2>
		<?php }else{ ?>
				<h2> You must create <?php echo $total_item;  ?> crates to win this month</h2>
		<?php } ?>
	</div>
</div>
	<?php 
		// if ($categories_crate) {
		// 	foreach ($categories_crate as $category_crate) {
		// 		echo "<span> * ".$category_crate."</span>";
		// 	}
		// }
	?>
<div>
	<h4 style='text-align:center;color: brown;'>YOUR CHOICE FOR THE TOP 3</h4>
	<?php 
	$crate_choosen = PRODUCT_TO_WIN::get_crate_have_position();
	?>
	<div style="min-height: 80px;">
	<form action="" method="POST" accept-charset="utf-8" style="text-align: center;">
			<?php 
				foreach ($crate_choosen as $id_pr => $name_prod) { ?>
					<label for="<?php echo $id_pr; ?>" style="min-width: 200px;display: inline-block;"> <input type="checkbox" name="delete_prod[]" value="<?php echo $id_pr; ?>"><?php echo $name_prod; ?></label>
			<?php } ?>
			<input type="submit" name="delete_posit" value="Delete position" style="width: 120px;height: 40px;background: black;color: white;border: 0px;" <?php echo $disabled = (!empty($crate_choosen)) ? '' : 'disabled' ; ?> >
	</form>
	</div>
</div>


<div id='container_red'>
	<div id='add_product'>
		<form action="" method="POST" accept-charset="utf-8">
		<input type="hidden" name="nbr_col" id='nbr_col' value="<?php echo $total_item-$nbr_post; ?>">
			<ul id='liste_de_produit'>
				<li id='entete_fields_items'>
					<div class='field_items'> <p> <?php echo __('Product Name', 'redbox'); ?>  </p> </div>
					<div class='field_items'> <p> <?php echo __('Name product catégorie', 'redbox'); ?>  </p> </div>
					<div class='field_items'> <p> <?php echo __('Product value', 'redbox'); ?>  </p> </div>
					<div class='field_items'> <p> <?php echo __('Product Position ( TOP 3 )', 'redbox'); ?>  </p> </div>
					<div class='field_items'> <p> <?php echo __('Product Image', 'redbox'); ?>  </p> </div>
				</li>
				<?php for ($i=1; $i <= ($total_item-$nbr_post) ; $i++) { ?>
				<li class='item_product'>
					<div class='field_items'>
						<span><?php echo $i; ?></span>
						<input type="text" name="name_product[]" value="" placeholder="">
					</div>
					<div class='field_items'>
						<select name="categorie[]">
							<?php foreach ($terms as $term ) { 
								if (!in_array($term,$date_array) && !in_array($term,$mois_array)) {?>
								<option value="<?php echo $term; ?>"> <?php echo $term; ?></option>
							<?php } } ?>
						</select>
						<!-- <input type="text" name="categorie[]" value="" placeholder=""> -->
					</div>
					<div class='field_items'>
						<input type="number" name="price[]" value="" placeholder="">
					</div>
					<div class='field_items'>
						<input type="number" name="position[]" value="" placeholder="">
					</div>
					<div class='field_items'>
						<input class="image-url_b_<?php echo $i; ?>" type="text" name="image_prod[]" />
					  	<input id="b_<?php echo $i; ?>"  type="button" class="button upload-button" value="Upload Image" onclick="upload_img(this.id)" />
					</div>
				</li>
				<?php } ?>
			</ul>
			<input  type="button" id='add_product_link' class="button" value="Add new product to win +" />
			<input  type="button" id='remove_product_link' class="button" value="Remove product to win -" />

			<input type="submit" name="create_post" value="create" id="create_post">
		</form>
	</div>
</div>

<div id="product_of_month">
	<div id='content_all_product'>
	<hr>
		<h3 style='text-align: center;'> For the month of November 2016 </h3>
		<ul>
			<li id='entete_winners'>
				<div class='winners_blk'>
					<p> Name of winner</p>
				</div>
				<div class='winners_blk'>
					<p>Crates gagnés</p>
				</div>
			</li>
			<li> <h3 class='type_user_title gold_li'>GOLD USERS</h3> </li>
			<?php  for ($i=0; $i < count($users['gold_id']); $i++) : 
				$post_win_gold = PRODUCT_TO_WIN::user_items_wins_month($users['gold_id'][$i]); 
				$user_win_gold = PRODUCT_TO_WIN::get_info_user($users['gold_id'][$i]); ?>
				<li class="winner_item">
					<div class='winners_blk'>
						<p> <?php echo $user_win_gold['first_name'].' '.$user_win_gold['last_name']; ?> </p>
					</div>
					<div class='winners_blk'>
						<?php
							if ($post_win_gold) {
								echo "<p>";
								foreach ($post_win_gold as $crate_win) {
									echo "<span> * ". $crate_win ."</span>";
								}
								echo "</p>";
							}else{
								echo "<p> no products won for this month</p>";
							}
						 ?>
					</div>
				</li>
			<?php endfor; ?>

			<li> <h3 class='type_user_title silver_li'>SILVER USERS</h3> </li>
			<?php  for ($i=0; $i < count($users['silver_id']); $i++) : 
				$post_win_silver = PRODUCT_TO_WIN::user_items_wins_month($users['silver_id'][$i]); 
				$user_win_silver = PRODUCT_TO_WIN::get_info_user($users['silver_id'][$i]); ?>
				<li class="winner_item">
					<div class='winners_blk'>
						<p> <?php echo $user_win_silver['first_name'].' '.$user_win_silver['last_name']; ?> </p>
					</div>
					<div class='winners_blk'>
						<?php
							if ($post_win_silver) {
								echo "<p>";
								foreach ($post_win_silver as $crate_win) {
									echo "<span> * ". $crate_win ."</span>";
								}
								echo "</p>";
							}else{
								echo "<p> no products won for this month</p>";
							}
						 ?>
					</div>
				</li>
			<?php endfor; ?>

			<li> <h3 class='type_user_title bronze_li'>BRONZE USERS</h3> </li>
			<?php  for ($i=0; $i < count($users['bronze_id']); $i++) : 
				$post_win_bronze = PRODUCT_TO_WIN::user_items_wins_month($users['bronze_id'][$i]); 
				$user_win_bronze = PRODUCT_TO_WIN::get_info_user($users['bronze_id'][$i]); ?>
				<li class="winner_item">
					<div class='winners_blk'>
						<p> <?php echo $user_win_bronze['first_name'].' '.$user_win_bronze['last_name']; ?> </p>
					</div>
					<div class='winners_blk'>
						<?php
							if ($post_win_bronze) {
								echo "<p>";
								foreach ($post_win_bronze as $crate_win) {
									echo "<span> * ". $crate_win ."</span>";
								}
								echo "</p>";
							}else{
								echo "<p> no products won for this month</p>";
							}
						 ?>
					</div>
				</li>
			<?php endfor; ?>
		</ul>
	</div>


	<form action="" method="POST" accept-charset="utf-8">
		<?php
			$post_win_silver = PRODUCT_TO_WIN::user_items_wins_month($users['silver_id'][0]);
			if ($post_win_silver) {
			 	$disable_inp = 'disabled'; ?>
		 	<p style="text-align:center;margin-top:40px;margin-bottom:0px;">
		 		<input type="checkbox" id="confirm_select" name="" value="">
		 		<label for="confirm_select"> Je suis conscient que les résultats seront réimitialisés </label>
			</p>

		<?php	 }else{
			 	$disable_inp = '';
			 }
		 ?>
		<input type="submit" name="select_winner" value="get the winners" id="select_winner"  <?php echo $disable_inp; ?>>
	</form>
	<hr>
	<form action="" method="POST" accept-charset="utf-8">
		<input type="submit" name="create_excels" value="Generate excels" id="create_excels">
	</form>
	<p style='text-align: center;'><a href="<?php echo '/wp-content/themes/redboxmedia/fichiers_excels/user_'.date('F').'_'.date('Y').'.xlsx' ?>" title="">
		Download excel file 
	</a> </br> (Make sure you are generate excel file)</p>
	
</div>

<?php }









?>