<?php 
class PRODUCT_TO_WIN
{
    // property declaration
    public $var = 'a default value';

    // method declaration
    private function add_to_categorie() {
        echo 'je veux voir ca';
    }
    private function set_featured_image( $post_id, $image_id){
       set_post_thumbnail( $post_id, $image_id );
    }

    public function create_item($name,$category,$position,$product_image,$price){
    		// création de post
	   	$new_post = array(
	   	            'post_title' => $name,
                    'post_content' => '',
	   	            'post_type' => 'crate',
	   	            'post_status' => 'publish',
	   	            'post_date' => date('Y-m-d H:i:s'),
	   	            // 'post_category' => array( $category),
	   	        );
            $date_mois = date('F');
            $date_annee = date('Y');
	        $post_id = wp_insert_post($new_post);
            $category = array($category,$date_mois,$date_annee );
            update_post_meta( $post_id, 'position', $position);
            update_post_meta( $post_id, 'crate_price', $price);
            wp_set_post_terms( $post_id, $category, 'category_crate');
             // Ajouter l'image à la une
            set_post_thumbnail($post_id,$product_image);
    }

    public static function get_number_user_by_groups(){
      $users = get_users();
      $Bronze = Groups_Group::read_by_name( 'Bronze' );
      $Gold = Groups_Group::read_by_name( 'Gold' );
      $Silver = Groups_Group::read_by_name( 'Silver' );
      $nb_users_bronze = 0;
      $nb_users_gold = 0;
      $nb_users_silver = 0;
      $gold_id = array();
      $silver_id = array();
      $bronze_id = array();
      $all_user = array();
      foreach ($users as $user) {
        if ( Groups_User_Group::read( $user->ID, $Bronze->group_id ) ) { 
            $nb_users_bronze++;
            array_push($bronze_id,$user->ID);
        }
        if ( Groups_User_Group::read( $user->ID, $Gold->group_id ) ) { 
            $nb_users_gold++;
            array_push($gold_id,$user->ID);
        }
        if ( Groups_User_Group::read( $user->ID, $Silver->group_id ) ) { 
            $nb_users_silver++;
            array_push($silver_id,$user->ID);
        }
      }

      $all_user['gold'] = $nb_users_gold;
      $all_user['bronze'] = $nb_users_bronze;
      $all_user['silver'] = $nb_users_silver;
      $all_user['gold_id'] = $gold_id;
      $all_user['bronze_id'] = $bronze_id;
      $all_user['silver_id'] = $silver_id;
      $all_user['user'] = $nb_users_silver + $nb_users_gold + $nb_users_bronze;

      return $all_user;

    }

    public static function delete_post_position($id_post){
        if (!empty($id_post)) {
            update_post_meta($id_post,'position', '');
        }
    }

    public static function get_all_crate(){
        $args_items = array(
                    'post_type' => 'crate',
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'category_crate',
                                'field'    => 'name',
                                'terms'    => date('F'),
                            ),
                            array(
                                'taxonomy' => 'category_crate',
                                'field'    => 'name',
                                'terms'    => date('Y'),
                            ),
                        ),
                    );
        $query_items = new WP_Query( $args_items );
        wp_reset_postdata();
        return $query_items;
    }

    public static function get_all_categories(){
        $names_categores = array();
        $categories = get_terms( array(
          'taxonomy' => 'category_crate',
          'hide_empty' => false,
         ) );
        foreach ($categories as $category) {
           array_push($names_categores, $category->name);
        }
        return $names_categores;
    }

    public static function get_crate_have_position(){
      $args_items = array(
                'post_type' => 'crate',
                'tax_query' => array(
                        array(
                            'taxonomy' => 'category_crate',
                            'field'    => 'name',
                            'terms'    => date('F'),
                        ),
                        array(
                            'taxonomy' => 'category_crate',
                            'field'    => 'name',
                            'terms'    => date('Y'),
                        ),
                    ),
                  'meta_query' => array(
                              array('key' => 'position',
                    'value'   => array(''),
                    'compare' => 'NOT IN'
                              )
                          ),
                  'orderby' => array(
                    'meta_value' => 'ASC',
                    'post_date'=> 'DESC',
                     //or 'meta_value_num'
                                ) 
                  );
      $crate_post = new WP_Query( $args_items );

      $crate_have_position = array();
      $array_position = array(); 
      while ( $crate_post->have_posts() ) {
        $crate_post->the_post();
        $laPosition = get_post_meta(get_the_ID(),'position', true);
        if ($laPosition && !in_array($laPosition ,$array_position)) {
          array_push($array_position, $laPosition);
          $crate_have_position[get_the_ID()] =  get_the_title();
        }
      }
        wp_reset_postdata();
        return $crate_have_position;
    }

    public static function get_info_user($id_user){
        $user = get_userdata($id_user);
        $user_spl = $user->data;
        $data_user['first_name'] = $user->first_name;
        $data_user['last_name'] = $user->last_name;
        $data_user['steam_id'] = get_user_meta($user->ID,'steam_id',true);
        $data_user['ID'] = $user->ID;
        return $data_user;
    }

    public function save_winner_gold($id_of_post, $user_nb, $users_id ){
        // division des tableaux par groups
        $gold_items = array_slice($id_of_post, 0, ($user_nb*3));
        // Melange des items Gold dans le tableau
        shuffle($gold_items);
        // divise les valeurs des tableau en part égale, par de division pour les bronzes car ils vont gagner un seul item
        $gold_items = array_chunk($gold_items,3);
        // Enregistrement des winners GOLD 
        for ($i=0; $i < count($users_id); $i++) {
            foreach ( $gold_items[$i] as $gold_item ) {
                update_post_meta($gold_item, 'winner_item', $users_id[$i]);
            }
        }
    }

    public function save_winner_silver($id_of_post, $user_nb_gold,$user_nb_silver, $users_id ){
        // division des tableaux par groups
        $silver_items = array_slice($id_of_post, $user_nb_gold*3, ($user_nb_silver*2) );
        // melange des valeurs dans les tableaux divisés
        shuffle($silver_items);
        // divise les valeurs des tableau en part égale, par de division pour les bronzes car ils vont gagner un seul item
        $silver_items = array_chunk($silver_items,2);
        // Enregistrement des winners SILVER 
        for ($i=0; $i < count($users_id); $i++) { 
            foreach ($silver_items[$i] as $silver_item) {
                update_post_meta($silver_item, 'winner_item', $users_id[$i]);
            }
        }
    }

    public function save_winner_bronze($id_of_post, $user_nb_gold,$user_nb_silver,$user_nb_bronze, $users_id ){
       // division des tableaux par groups
       $bronze_items = array_slice($id_of_post, ($user_nb_silver*2)+ ($user_nb_gold*3), $user_nb_bronze );
       // melange des valeurs dans les tableaux divisés
       shuffle($bronze_items);
       // Enregistrement des winners BRONZE 
       for ($i=0; $i < count($users_id); $i++) { 
            update_post_meta($bronze_items[$i], 'winner_item', $users_id[$i]);
       }
    }


    public static function user_items_wins_month($id_user){
        $name_of_post = array();
        $args_items = array(
                    'post_type' => 'crate',
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'category_crate',
                                'field'    => 'name',
                                'terms'    => date('F'),
                            ),
                            array(
                                'taxonomy' => 'category_crate',
                                'field'    => 'name',
                                'terms'    => date('Y'),
                            ),
                        ),
                    'meta_query' => array(
                        array(
                            'key' => 'winner_item',
                            'value' => $id_user,
                            'compare' => '=',
                          ),
                       )
                    );
        $items_wins_month = new WP_Query( $args_items );
        if ($items_wins_month->have_posts()) {
            while ( $items_wins_month->have_posts() ) {
                $items_wins_month->the_post();
                array_push($name_of_post, get_the_title());
            }
            wp_reset_postdata();
            return $name_of_post;
        }else{
            return false;
        }
    }

    public function create_excels_file(){
      error_reporting(E_ALL);
      ini_set('display_errors', TRUE);
      ini_set('display_startup_errors', TRUE);
      date_default_timezone_set('Europe/London');

      define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

      /** Include PHPExcel */
      require_once dirname(__FILE__) . '/PHPExcel.php';

      // Create new PHPExcel object
      echo date('H:i:s') , " Create new PHPExcel object" , EOL;
      $objPHPExcel = new PHPExcel();

      // Set document properties
      echo date('H:i:s') , " Set document properties" , EOL;
      $objPHPExcel->getProperties()->setCreator("CSCRATE")
                     ->setLastModifiedBy("cscrate")
                     ->setTitle("User winner crate")
                     ->setSubject("User winner crate")
                     ->setDescription("User winner crate")
                     ->setKeywords("office PHPExcel php")
                     ->setCategory("User winner crate");

     // Add some data
     $objPHPExcel->setActiveSheetIndex(0)
                 ->setCellValue('A1', 'FIRST NAME')
                 ->setCellValue('B1', 'LAST NAME')
                 ->setCellValue('C1', 'STEAM ID')
                 ->setCellValue('D1', 'PRODUCT WON')
                 ->setCellValue('E1', 'TYPE OF USER');



      $users = PRODUCT_TO_WIN::get_number_user_by_groups();


      $num_rws_gold = 2;
      for ($i=0; $i < count($users['gold_id']); $i++) : 
          // je take les infos des gols user
          $num_rws_gold = $num_rws_gold+1;
          $post_win_gold = PRODUCT_TO_WIN::user_items_wins_month($users['gold_id'][$i]); 
          $user_win_gold = PRODUCT_TO_WIN::get_info_user($users['gold_id'][$i]);

          $objPHPExcel->setActiveSheetIndex(0)
                      ->setCellValue('A'.$num_rws_gold, $user_win_gold['first_name'])
                      ->setCellValue('B'.$num_rws_gold, $user_win_gold['last_name'])
                      ->setCellValue('C'.$num_rws_gold, $user_win_gold['steam_id'])
                      ->setCellValue('D'.$num_rws_gold, $post_win_gold[0].', '.$post_win_gold[1].', '.$post_win_gold[2])
                      ->setCellValue('E'.$num_rws_gold, 'GOLD');
      endfor;

      $num_rws_silver = $num_rws_gold+1;
      for ($i=0; $i < count($users['silver_id']); $i++) : 
          // je take les infos des gols user
          $num_rws_silver = $num_rws_silver+1;
          $post_win_silver = PRODUCT_TO_WIN::user_items_wins_month($users['silver_id'][$i]); 
          $user_win_silver = PRODUCT_TO_WIN::get_info_user($users['silver_id'][$i]);

          $objPHPExcel->setActiveSheetIndex(0)
                      ->setCellValue('A'.$num_rws_silver, $user_win_silver['first_name'])
                      ->setCellValue('B'.$num_rws_silver, $user_win_silver['last_name'])
                      ->setCellValue('C'.$num_rws_silver, $user_win_silver['steam_id'])
                      ->setCellValue('D'.$num_rws_silver, $post_win_silver[0].', '.$post_win_silver[1])
                      ->setCellValue('E'.$num_rws_silver, 'SILVER');
      endfor;

      $num_rws_bronze = $num_rws_silver+1;
      for ($i=0; $i < count($users['bronze_id']); $i++) : 
          // je take les infos des gols user
          $num_rws_bronze = $num_rws_bronze+1;
          $post_win_bronze = PRODUCT_TO_WIN::user_items_wins_month($users['bronze_id'][$i]); 
          $user_win_bronze = PRODUCT_TO_WIN::get_info_user($users['bronze_id'][$i]);

          $objPHPExcel->setActiveSheetIndex(0)
                      ->setCellValue('A'.$num_rws_bronze, $user_win_bronze['first_name'])
                      ->setCellValue('B'.$num_rws_bronze, $user_win_bronze['last_name'])
                      ->setCellValue('C'.$num_rws_bronze, $user_win_bronze['steam_id'])
                      ->setCellValue('D'.$num_rws_bronze, $post_win_bronze[0])
                      ->setCellValue('E'.$num_rws_bronze, 'BRONZE');
      endfor;
              

      // Rename worksheet
      $objPHPExcel->getActiveSheet()->setTitle('Simple');

      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $objPHPExcel->setActiveSheetIndex(0);

      // Save Excel 2007 file
      // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
      // $callStartTime = microtime(true);

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      // $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
      $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/redboxmedia/fichiers_excels/user_'.date('F').'_'.date('Y').'.xlsx');




    }


}

 ?>