<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/* Vérifier s'il y a une configuration locale */

if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
	define( 'WP_LOCAL_DEV', true );

	include( dirname( __FILE__ ) . '/local-config.php' );
} else {
	//define('DB_NAME', 'cscrate_wp1');

	//define('DB_USER', 'cscrate_wp1');

	//define('DB_PASSWORD', 'S(MV7slgX4cELSt0Fn(88*^2');
	//define('DB_HOST', 'localhost');
	define('DB_NAME', 'cjmrocor_cscrate');

	define('DB_USER', 'cjmrocor_cscrate');

	define('DB_PASSWORD', 'O14#)7;cW0Ty');
	define('DB_HOST', 'localhost');
}

// define( 'WP_CACHE', true );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/* SÉCURITÉ + TWEAKS */
define( 'WP_MEMORY_LIMIT', '256M' );

// define( 'FORCE_SSL_LOGIN', true );
#define( 'FORCE_SSL_ADMIN', true );

define( 'WP_AUTO_UPDATE_CORE', false ); # No auto-update!

define( 'DISALLOW_FILE_EDIT', true); # No Dashboard file editing!

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!m0;yu`#/p>*LV5XyR#9FpP+-%DRu<Y+Q+KwM);;SVg.-|xo$l&+(RgH3v%=;.KR');
define('SECURE_AUTH_KEY',  's9QYw*aG-c< z%/F^D~LL;=SAr$moB1@ gE&1r$ame<!Au;ah;$XT#-U$!WwnxeG');
define('LOGGED_IN_KEY',    '~lYD)iuT;;Ah6J@=aq&[OJp7^&Ylv$kJ=9H#fO+`gO KsqOi3!Aw`e}HH?&!vzZ(');
define('NONCE_KEY',        'fadPO5KuYc~%#>N+Hm0RXYq`r]of]_G`PJv{k>G`OjG1LfHOw=Cz:@80S;Y,#>eA');
define('AUTH_SALT',        'a0oVNWaR7`qvxik=f~ {Sxm=>r)$aO1YhhMr<sY_.i%x<oLPhCkQE<Tc(HbL+q,L');
define('SECURE_AUTH_SALT', '.2;c6!Hn3?-5BppZ&Nc.N$OQ=av=B`.A!NfViaHiK=(iAkr*n?75F2Zw(;TzC|US');
define('LOGGED_IN_SALT',   'PNZ_4hinX<!!W| HK:T*rtrT?[mc4q&@_,inYK?VzBd4>B5Q:,;P8D :wvQ#sNX7');
define('NONCE_SALT',       'yyHViC;jI-BJ7;px,2u;rE;0&o`1.sr4E;?T9f-zq|2Z*O^4pXd:*DtJ K0e+yMj');


/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');